=== Fairy ===

Contributors: candidthemes
Tags: grid-layout, one-column, two-columns, left-sidebar, right-sidebar, custom-background, custom-logo, custom-menu, featured-images, translation-ready, footer-widgets, custom-colors, custom-header, editor-style, rtl-language-support, theme-options, threaded-comments, blog, news
Requires at least: 4.5
Requires PHP: 5.2
Tested up to: 5.9
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Premium Blog theme.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== License ===

License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Fairy Pro WordPress Theme, Copyright 2021 Candid Themes
Fairy Pro is distributed under the terms of the GNU General Public License v2

== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
* Font-Awesome https://github.com/FortAwesome/Font-Awesome FontAwesome 4.7.0 Copyright 2012 Dave Gandy Font License: SIL OFL 1.1 Code License: MIT License http://fontawesome.io/license/
* theia-sticky-sidebar https://github.com/WeCodePixels/theia-sticky-sidebar [MIT](http://opensource.org/licenses/MIT)
* BreadcrumbTrail - http://themehybrid.com/plugins/breadcrumb-trail http://www.gnu.org/licenses/old-licenses/gpl-2.0.html Copyright (c) 2008 - 2015, Justin Tadlock
* Slick Slider - MIT License, https://github.com/kenwheeler/slick/blob/master/LICENSE


== Images License ==
License: Creative Commons CC0 license.
All the images used in the screenshots are CCO License.


== Changelog ==
= 1.1.4 - January 31 2022 =
* Tested with WP 5.9
* Changed Copyright date

= 1.1.3 - August 20 2021 =
* Tested with WP 5.8
* Freemius SDK update

= 1.1.2 - April 01 2021 =
* Added new icons for social menu

= 1.1.0 - January 22 2021 =
* Fixed notice in editor page

= 1.0.9 - January 21 2021 =
* Added Poppins font family

= 1.0.8 - December 18 2020 =
* Added option for the author signature in author widget

= 1.0.7 - December 17 2020 =
* Checked with latest WP version 5.6
* Fixed customizer issues on php 7.4 

= 1.0.6 - November 19 2020 =
* Fixed Menu Issues
* Fixed Previous and Next Post IE Issues

= 1.0.5 - November 18 2020 =
* Option Added to change the blog and sidbear section overlay
* Fixed Breadcrumb Issue
* Fixed Background Color Issue

= 1.0.4 - October 31 2020 =
* Added WooCommerce Support
* Telegram Icon Added
* Fixed some minor CSS issues

= 1.0.3 - October 29 2020 =
* Added Menu Font Option
* Added Logo section background color
* Freemius SDK updated to 2.4.1

= 1.0.2 - October 28 2020 =
* Added Option for Post Tags
* Fixed site title active color

= 1.0.1 - October 17 2020 =
* Fixed RTL Issues
* Fixed some CSS Issues

= 1.0.0 - October 13 2020 =
* Initial Submission
