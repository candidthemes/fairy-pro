<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package fairy
 */
global $fairy_theme_options;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    $image_alignment_class = '';
    $image_alignment = esc_attr($fairy_theme_options['fairy-blog-page-image-position']);
    if($image_alignment == 'right-image'){
        $image_alignment_class = 'reverse-row';
    }elseif($image_alignment == 'full-image'){
        $image_alignment_class = 'card-full-width';
    }
    ?>
    <div class="card card-blog-post <?php echo esc_attr($image_alignment_class); ?>">
        <?php
        if(has_post_thumbnail() && ($image_alignment != 'hide-image')) {
            ?>
            <figure class="card_media">
                <a href="<?php the_permalink(); ?>">
                    <?php
                    $cropped_image = esc_attr($fairy_theme_options['fairy-image-size-blog-page']);
                    if($cropped_image == 'cropped-image'){
                        the_post_thumbnail('fairy-medium');
                    }else{
                        the_post_thumbnail();
                    }

                    ?>
                </a>
            </figure>
            <?php
        }
        ?>
        <div class="card_body">
            <!-- To have a background category link add [.bg-label] in category-label-group class -->
            <div <?php if($fairy_theme_options['fairy-title-position-blog-page'] == 'center-title'){ ?> class="text-center" <?php } ?>>
                <?php
                if(($fairy_theme_options['fairy-enable-blog-category'] == 1) && ( 'post' === get_post_type()) ){
                    fairy_list_category();
                }

                if (is_singular()) :
                    the_title('<h1 class="card_title">', '</h1>');
                else :
                    the_title('<h2 class="card_title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
                endif;
                ?>
                <?php

                if ('post' === get_post_type()) :
                    ?>
                    <div class="entry-meta">
                        <?php
                        if($fairy_theme_options['fairy-enable-blog-date'] == 1){
                            fairy_posted_on();
                        }
                        if($fairy_theme_options['fairy-enable-blog-author'] == 1){
                            fairy_posted_by();
                        }

                        ?>
                    </div><!-- .entry-meta -->
                <?php endif; ?>
            </div>
            <div <?php if($fairy_theme_options['fairy-content-position-blog-page'] == 'center-content'){ ?> class="text-center" <?php } ?>>
                <div class="entry-content">
                    <?php
                    if (is_singular()) {
                        the_content(
                            sprintf(
                                wp_kses(
                                /* translators: %s: Name of current post. Only visible to screen readers */
                                    __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'fairy'),
                                    array(
                                        'span' => array(
                                            'class' => array(),
                                        ),
                                    )
                                ),
                                wp_kses_post(get_the_title())
                            )
                        );

                        wp_link_pages(
                            array(
                                'before' => '<div class="page-links">' . esc_html__('Pages:', 'fairy'),
                                'after' => '</div>',
                            )
                        );
                    } else {
                        if($fairy_theme_options['fairy-content-show-from'] == 'excerpt'){
                            the_excerpt();
                        }elseif($fairy_theme_options['fairy-content-show-from'] == 'content'){
                            the_content();
                        }

                    }
                    ?>
                </div>
                <?php
                if(($fairy_theme_options['fairy-content-show-from'] == 'excerpt') && (!is_singular())){
                    $read_more_text = $fairy_theme_options['fairy-read-more-text'];
                    ?>
                    <a href="<?php the_permalink(); ?>" class="btn btn-primary">
                        <?php
                        if(!empty($read_more_text)){
                            echo esc_html($read_more_text);
                        }else{
                            esc_html_e('Read More', 'fairy');
                        }
                        ?>
                    </a>
                    <?php
                }
                ?>
            </div>


        </div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
