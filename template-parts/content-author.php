<?php
/**
 * The template for displaying Author Bio
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Refined Magazine
 */
?>
<div class="author-wrapper">
    <?php if (get_option('show_avatars') == 1): ?>
        <figure class="author">
            <?php echo get_avatar(get_the_author_meta('user_email'), '150', ''); ?>
        </figure>
    <?php endif; ?>

    <div class="author-description">
        <h4 class="author-title"><?php the_author_posts_link(); ?></h4>
        <p><?php the_author_meta('description'); ?></p>

        <?php do_action('fairy_author_links'); ?>
    </div>


</div>
