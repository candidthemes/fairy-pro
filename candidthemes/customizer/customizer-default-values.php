<?php
/**
 * Fairy Theme Customizer default values
 *
 * @package Fairy
 */
if ( !function_exists('fairy_default_theme_options_values') ) :
    function fairy_default_theme_options_values() {
        $default_theme_options = array(
            /*Top Header*/
            'fairy-enable-top-header'=> true,
            'fairy-enable-top-header-social'=> true,
            'fairy-enable-top-header-menu'=> true,
            'fairy-enable-top-header-search'=> true,

            /*Header Ads Option*/
            'fairy-enable-ads-header'=> false,
            'fairy-header-ads-image'=> '',
            'fairy-header-ads-image-link'=> '',

            /*Header Types*/
            'fairy-header-types'=> 'default-header',
            'fairy-header-menu-position'=> 'default-menu',
            'fairy-header-logo-position'=> 'default-logo',

            /*Primary Menu Section Option*/
            'fairy-enable-sticky-primary-menu' => true,

            /*Slider Settings Option*/
            'fairy-enable-slider'=> false,
            'fairy-select-category'=> 0,
            'fairy-slider-post-category'=> true,
            'fairy-slider-post-date'=> true,
            'fairy-slider-post-author'=> true,
            'fairy-slider-types'=> 'default-slider',
            'fairy-image-size-slider'=> 'cropped-image',

            /*Category Boxes*/
            'fairy-enable-category-boxes'=> false,
            'fairy-boxes-selection-method'=> 'multiple-cat',
            'fairy-multiple-cat-select-1'=> 0,
            'fairy-multiple-image-select-1'=> '',
            'fairy-multiple-cat-select-2'=> 0,
            'fairy-multiple-image-select-2'=> '',
            'fairy-multiple-cat-select-3'=> 0,
            'fairy-multiple-image-select-3'=> '',            
            'fairy-multiple-cat-posts-select-1'=> 0,
            'fairy-multiple-cat-posts-select-2'=> 0,
            'fairy-multiple-cat-posts-select-3'=> 0,
            'fairy-single-cat-posts-select-1'=> 0,


            /*Site Layout*/
            'fairy-site-layout-blog-overlay'=> 1,
            'fairy-site-layout-options'=> 'full-width',
            'fairy-boxed-width-options'=> 1200,
            'fairy-boxed-layout-background-color'=> '#fff',
            'fairy-site-dark-light-layout-options'=> 'default-light',

            /*Sidebar Options*/
            'fairy-sidebar-blog-page'=>'right-sidebar',
            'fairy-sidebar-archive-page'=>'right-sidebar',
            'fairy-sidebar-single-page'=>'right-sidebar',
            'fairy-enable-sticky-sidebar'=> true,
            'fairy-sidebar-title-designs'=> 'default-title',

            /*Blog Page Default Value*/
            'fairy-column-blog-page'=> 'one-column',
            'fairy-content-show-from'=>'excerpt',
            'fairy-excerpt-length'=>25,
            'fairy-pagination-options'=>'numeric',
            'fairy-default-pagination-text-one' => esc_html__('Next','fairy'),
            'fairy-default-pagination-text-two' => esc_html__('Previous','fairy'),
            'fairy-read-more-text'=> esc_html__('Read More','fairy'),
            'fairy-enable-blog-author'=> false,
            'fairy-enable-blog-category'=> true,
            'fairy-enable-blog-date'=> true,
            'fairy-title-position-blog-page'=> 'left-title',
            'fairy-blog-page-masonry-normal'=> 'normal',
            'fairy-blog-page-image-position'=> 'left-image',
            'fairy-content-position-blog-page'=>'left-content',
            'fairy-image-size-blog-page'=> 'cropped-image',

            /*Single Page Default Value*/
            'fairy-enable-single-title-position' => 'left-title',
            'fairy-single-page-featured-image'=> true,
            'fairy-single-page-related-posts'=> true,
            'fairy-single-page-related-posts-title'=> esc_html__('Related Posts','fairy'),
            'fairy-enable-single-category' => true,
            'fairy-enable-single-date' => true,
            'fairy-enable-single-author' => true,
            'fairy-single-page-tags'=> false,
            'fairy-enable-single-comments-form'=> true,
            'fairy-enable-single-comments-form-move'=>'default',
            'fairy-enable-underline-link' => false,
            'fairy-single-page-related-posts-number' => 2,
            'fairy-single-page-related-posts-image' => true,
            'fairy-single-page-related-posts-date' => true,
            'fairy-single-page-related-posts-author'=> true,
            'fairy-show-hide-author-box'  => false,
            'fairy-single-page-related-selection-types'=> 'category',

            /*Breadcrumb Settings*/
            'fairy-blog-site-breadcrumb'=> true,
            'fairy-breadcrumb-display-from-option'=> 'theme-default',
            'fairy-breadcrumb-text'=> '',

             /*General Colors*/
            'fairy-primary-color' => '#ff7e00',
            'fairy-header-description-color'=> '#404040',
            'fairy-overlay-color'=> 'rgba(255, 126, 0, 0.5)',
            'fairy-overlay-second-color'=> 'rgba(0, 0, 0, 0.5)',
            'fairy-boxes-overlay-first-color'=> 'rgba(255, 126, 0, 0.5)',
            'fairy-boxes-overlay-second-color'=> 'rgba(0, 0, 0, 0.5)',

            /*Logo Section Color*/
            'fairy-logo-section-background-color'=>'',

            /*Top Header Colors*/
            'fairy-top-header-background-color'=>'',
            'fairy-top-header-text-color'=> '',
            'fairy-top-header-hover-color'=> '',

            /*Menu Colors*/
            'fairy-menu-background-color'=>'',
            'fairy-border-color'=>'',
            'fairy-top-border-color'=>'',
            'fairy-text-color'=> '',
            'fairy-text-hover-color'=> '',
            'fairy-dropdown-text-color'=> '',
            'fairy-dropdown-text-hover-color'=> '',
            'fairy-dropdown-background-color'=> '',
            'fairy-dropdown-single-background-color'=> '',

            /*Blog Page*/
            'fairy-blog-background-color'=>'',
            'fairy-blog-heading-color'=>'',
            'fairy-blog-heading-hover-color'=> '',
            'fairy-blog-paragraph-color'=> '',
            'fairy-blog-read-more-background-color'=>'',
            'fairy-blog-read-more-hover-color'=>'',
            'fairy-blog-read-more-text-color'=>'',
            'fairy-blog-pagination-color'=>'',
            'fairy-blog-pagination-active-color'=>'',
            'fairy-blog-pagination-hover-color'=>'',

            /*Sidebar Color*/
            'fairy-sidebar-background-color'=>'',
            'fairy-sidebar-title-color'=>'',
            'fairy-sidebar-content-color'=>'',
            'fairy-sidebar-link-color'=>'',
            'fairy-sidebar-link-hover-color'=>'',

            /*Footer Color*/
            'fairy-footer-background-color'=> '',
            'fairy-footer-text-color'=>'',
            'fairy-footer-link-color'=>'',
            'fairy-footer-link-hover-color'=>'',
            'fairy-footer-to-top-back-color'=>'',

            /*Lower Footer Color*/
            'fairy-lower-footer-background-color'=> '',
            'fairy-lower-footer-text-color'=>'',
            'fairy-lower-footer-link-color'=>'',
            'fairy-lower-footer-link-hover-color'=>'',

            /*Footer Options*/
            'fairy-footer-instagram'=> '[instagram-feed]',
            'fairy-footer-copyright'=> esc_html__('All Rights Reserved 2022.','fairy'),
            'fairy-footer-powered-text'=> sprintf( esc_html__('Proudly powered by %1$s %2$s Theme: %3$s by %4$s.','fairy') , '<a href="https://wordpress.org/" target="_blank">WordPress</a>', ' <span class="sep"> | </span>', 'Fairy Pro', '<a href="https://www.candidthemes.com/" target="_blank">Candid Themes</a>' ),
            'fairy-go-to-top'=> true,
            'fairy-go-to-top-icon'=> esc_html__('fa-long-arrow-up','fairy'),
            'fairy-go-to-top-icon-new'=> esc_html__('fa-long-arrow-alt-up','fairy'),
            'fairy-footer-social-icons'=> false,
            'fairy-footer-mailchimp-subscribe'=> false,
            'fairy-footer-mailchimp-form-id'=> '',
            'fairy-footer-mailchimp-form-title'=>  esc_html__('Subscribe to my Newsletter','fairy'),
            'fairy-footer-mailchimp-form-subtitle'=> esc_html__('Be the first to receive the latest buzz on upcoming contests & more!','fairy'),

            /*Extra Options*/
            'fairy-category-title-design'=> 'theme-default',
            'fairy-font-awesome-version-loading'=> 'version-4',
            'fairy-enable-category-color'=> 0,
            'fairy-post-published-updated-date'=> 'post-published',

            /*Site Title Font Options*/
            'fairy-font-site-title-family-url'=> 'Oswald:400,300,700',
            'fairy-site-title-font-size'=> 54,
            'fairy-site-title-font-line-height'=> 1,
            'fairy-site-title-letter-spacing'=> 4,

            /*Paragraph Font Options*/
            'fairy-font-family-url'=> 'Muli:400,300italic,300',
            'fairy-font-paragraph-font-size'=> 16,
            'fairy-font-line-height'=> 1.5,
            'fairy-letter-spacing'=> 0,
    
            /*Menu Font Options*/
            'fairy-menu-font-family-url'=> 'Muli:400,300italic,300',
            'fairy-menu-font-size'=> 16,
            'fairy-menu-font-line-height'=> 1.7,
            'fairy-menu-letter-spacing'=> 0,
    
            /*Widget Font Options*/
            'fairy-widget-font-family-url'=> 'Roboto:400,500,300,700,400italic',
            'fairy-widget-font-size'=> 18,
            'fairy-widget-font-line-height'=> 1,
            'fairy-widget-letter-spacing'=> 0,

            /*Post Title Font Options*/
            'fairy-post-font-family-url'=> 'Roboto:400,500,300,700,400italic',
            'fairy-post-font-size'=> 26,
            'fairy-post-font-line-height'=> 1.25,
            'fairy-post-letter-spacing'=> 0,
    
            /*H1 Font Options*/
            'fairy-font-heading-family-url'=> 'Roboto:400,500,300,700,400italic',
            'fairy-h1-font-size'=> 36,
            'fairy-h1-font-line-height'=> 1.5,
            'fairy-h1-letter-spacing'=> 0,
    
            /*H2 Font Options*/
            'fairy-font-h2-family-url'=> 'Roboto:400,500,300,700,400italic',
            'fairy-h2-font-size'=> 32,
            'fairy-h2-font-line-height'=> 1.5,
            'fairy-h2-letter-spacing'=> 0,
    
            /*H3 Font Options*/
            'fairy-font-h3-family-url'=> 'Roboto:400,500,300,700,400italic',
            'fairy-h3-font-size'=> 26,
            'fairy-h3-font-line-height'=> 1.5,
            'fairy-h3-letter-spacing'=> 0,
    
            /*H4 Font Options*/
            'fairy-font-h4-family-url'=> 'Roboto:400,500,300,700,400italic',
            'fairy-h4-font-size'=> 22,
            'fairy-h4-font-line-height'=> 1.5,
            'fairy-h4-letter-spacing'=> 0,
    
            /*H5 Font Options*/
            'fairy-font-h5-family-url'=> 'Roboto:400,500,300,700,400italic',
            'fairy-h5-font-size'=> 18,
            'fairy-h5-font-line-height'=> 1.5,
            'fairy-h5-letter-spacing'=> 0,
    
            /*H6 Font Options*/
            'fairy-font-h6-family-url'=> 'Roboto:400,500,300,700,400italic',
            'fairy-h6-font-size'=> 14,
            'fairy-h6-font-line-height'=> 1.5,
            'fairy-h6-letter-spacing'=> 0,
        );
        return apply_filters( 'fairy_default_theme_options_values', $default_theme_options );
    }
endif;

/**
 *  Fairy Theme Options and Settings
 *
 * @since Fairy 1.0.0
 *
 * @param null
 * @return array fairy_get_options_value
 *
 */
if ( !function_exists('fairy_get_options_value') ) :
    function fairy_get_options_value() {
        $fairy_default_theme_options_values = fairy_default_theme_options_values();
        $fairy_get_options_value = get_theme_mod( 'fairy_options');
        if( is_array( $fairy_get_options_value )){
            return array_merge( $fairy_default_theme_options_values, $fairy_get_options_value );
        }
        else{
            return $fairy_default_theme_options_values;
        }
    }
endif;