<?php
/**
 *  Fairy Category Color Option
 *
 * @since Fairy 1.0.0
 *
 */
/*Category Color Options*/
$wp_customize->add_section('fairy_category_color_setting', array(
    'priority'      => 72,
    'title'         => __('Category Color', 'fairy'),
    'description'   => __('You can select the different color for each category.', 'fairy'),
    'panel'          => 'fairy_panel'
));

/*Enable Top Header Section*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-category-color]', array(
   'capability'        => 'edit_theme_options',
   'transport' => 'refresh',
   'default'           => $default['fairy-enable-category-color'],
   'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-category-color]', array(
   'label'     => __( 'Enable Category Color', 'fairy' ),
   'description' => __('Checked to enable the category color and select the required color for each category.', 'fairy'),
   'section'   => 'fairy_category_color_setting',
   'settings'  => 'fairy_options[fairy-enable-category-color]',
   'type'      => 'checkbox',
   'priority'  => 1,
) );

/*callback functions header section*/
if ( !function_exists('fairy_colors_active_callback') ) :
  function fairy_colors_active_callback(){
      global $fairy_theme_options;
      $fairy_theme_options = fairy_get_options_value();
      $enable_color = absint($fairy_theme_options['fairy-enable-category-color']);
      if( 1 == $enable_color ){
          return true;
      }
      else{
          return false;
      }
  }
endif;

$i = 1;
$args = array(
    'orderby' => 'id',
    'hide_empty' => 0
);
$categories = get_categories( $args );
$wp_category_list = array();
foreach ($categories as $category_list ) {
    $wp_category_list[$category_list->cat_ID] = $category_list->cat_name;

    $wp_customize->add_setting('fairy_options[cat-'.get_cat_id($wp_category_list[$category_list->cat_ID]).']', array(
        'default'           => $default['fairy-primary-color'],
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_hex_color'
    ));
    $wp_customize->add_control(
    	new WP_Customize_Color_Control(
    		$wp_customize,
		    'fairy_options[cat-'.get_cat_id($wp_category_list[$category_list->cat_ID]).']',
		    array(
		    	'label'     => sprintf(__('"%s" Color', 'fairy'), $wp_category_list[$category_list->cat_ID] ),
			    'section'   => 'fairy_category_color_setting',
			    'settings'  => 'fairy_options[cat-'.get_cat_id($wp_category_list[$category_list->cat_ID]).']',
			    'priority'  => $i,
                'active_callback'   => 'fairy_colors_active_callback'
		    )
	    )
    );
    $i++;
}