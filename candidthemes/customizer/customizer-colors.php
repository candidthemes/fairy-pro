<?php
/**
 *  Fairy Color Option
 *
 * @since Fairy 1.0.0
 *
 */

$wp_customize->add_panel(
    'colors',
    array(
        'title'    => __( 'Color Options', 'fairy' ),
        'priority' => 30, // Before Additional CSS.
    )
);
$wp_customize->add_section(
    'colors',
    array(
        'title' => __( 'General Colors', 'fairy' ),
        'panel' => 'colors',
    )
);

/* Site Title hover color */
$wp_customize->add_setting( 'fairy_options[fairy-primary-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-primary-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-primary-color]',
        array(
            'label'       => esc_html__( 'Site Primary Color', 'fairy' ),
            'description' => esc_html__( 'It will change the color of site whole site.', 'fairy' ),
            'section'     => 'colors',
             'settings'  => 'fairy_options[fairy-primary-color]',
        )
    )
);


/* Header Description Color */
$wp_customize->add_setting( 'fairy_options[fairy-header-description-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-header-description-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-header-description-color]',
        array(
            'label'       => esc_html__( 'Header Description Color', 'fairy' ),
            'description' => esc_html__( 'It will change the color of site header description.', 'fairy' ),
            'section'     => 'colors',
             'settings'  => 'fairy_options[fairy-header-description-color]',
        )
    )
);

/*top header*/
$wp_customize->add_section(
    'top-header-colors',
    array(
        'title' => __( 'Top Header Colors', 'fairy' ),
        'panel' => 'colors',
    )
);

/* Top Header Background */
$wp_customize->add_setting( 'fairy_options[fairy-top-header-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-top-header-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-top-header-background-color]',
        array(
            'label'       => esc_html__( 'Top Header Background Color', 'fairy' ),
            'description' => esc_html__( 'This is for the top header color where menu and social icons are there.', 'fairy' ),
            'section'     => 'top-header-colors',
             'settings'  => 'fairy_options[fairy-top-header-background-color]',
        )
    )
);


/* Top Header Color */
$wp_customize->add_setting( 'fairy_options[fairy-top-header-text-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-top-header-text-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-top-header-text-color]',
        array(
            'label'       => esc_html__( 'Top Header Text Color', 'fairy' ),
            'description' => esc_html__( 'This is for the top header color where menu and social icons are there.', 'fairy' ),
            'section'     => 'top-header-colors',
             'settings'  => 'fairy_options[fairy-top-header-text-color]',
        )
    )
);


/* Top Header Hover Color */
$wp_customize->add_setting( 'fairy_options[fairy-top-header-hover-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-top-header-hover-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-top-header-hover-color]',
        array(
            'label'       => esc_html__( 'Top Header Hover Color', 'fairy' ),
            'description' => esc_html__( 'This is for the top header hover color where menu and social icons are there.', 'fairy' ),
            'section'     => 'top-header-colors',
             'settings'  => 'fairy_options[fairy-top-header-hover-color]',
        )
    )
);

/*Logo Section*/
$wp_customize->add_section(
    'logo-section-colors',
    array(
        'title' => __( 'Logo Section Colors', 'fairy' ),
        'panel' => 'colors',
    )
);

/* Top Header Background */
$wp_customize->add_setting( 'fairy_options[fairy-logo-section-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-logo-section-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-logo-section-background-color]',
        array(
            'label'       => esc_html__( 'Logo Section Background Color', 'fairy' ),
            'description' => esc_html__( 'This is for the logo section color for background.', 'fairy' ),
            'section'     => 'logo-section-colors',
             'settings'  => 'fairy_options[fairy-logo-section-background-color]',
        )
    )
);


/*Menu Colors*/
$wp_customize->add_section(
    'menu-colors',
    array(
        'title' => __( 'Menu Colors', 'fairy' ),
        'panel' => 'colors',
    )
);

/* Menu Background */
$wp_customize->add_setting( 'fairy_options[fairy-menu-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-menu-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-menu-background-color]',
        array(
            'label'       => esc_html__( 'Menu Background Color', 'fairy' ),
            'description' => esc_html__( 'Whole Background color for the menu bar.', 'fairy' ),
            'section'     => 'menu-colors',
             'settings'  => 'fairy_options[fairy-menu-background-color]',
        )
    )
);

/* Menu Border Background */
$wp_customize->add_setting( 'fairy_options[fairy-border-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-border-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-border-color]',
        array(
            'label'       => esc_html__( 'Buttom Border Color', 'fairy' ),
            'description' => esc_html__( 'It will change the color of the menu bar border.', 'fairy' ),
            'section'     => 'menu-colors',
             'settings'  => 'fairy_options[fairy-border-color]',
        )
    )
);

/* Menu Border Background */
$wp_customize->add_setting( 'fairy_options[fairy-top-border-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-top-border-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-top-border-color]',
        array(
            'label'       => esc_html__( 'Top Border Color', 'fairy' ),
            'description' => esc_html__( 'It will change the color of the menu bar top border.', 'fairy' ),
            'section'     => 'menu-colors',
             'settings'  => 'fairy_options[fairy-top-border-color]',
        )
    )
);

/* Menu Text Color */
$wp_customize->add_setting( 'fairy_options[fairy-text-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-text-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-text-color]',
        array(
            'label'       => esc_html__( 'Text Color', 'fairy' ),
            'description' => esc_html__( 'It will change the color of the menu text.', 'fairy' ),
            'section'     => 'menu-colors',
             'settings'  => 'fairy_options[fairy-text-color]',
        )
    )
);

/* Menu Text hover Color */
$wp_customize->add_setting( 'fairy_options[fairy-text-hover-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-text-hover-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-text-hover-color]',
        array(
            'label'       => esc_html__( 'Text Hover Color', 'fairy' ),
            'description' => esc_html__( 'It will change the color of the menu text hover.', 'fairy' ),
            'section'     => 'menu-colors',
             'settings'  => 'fairy_options[fairy-text-hover-color]',
        )
    )
);

/* Menu Dropdown Text Color */
$wp_customize->add_setting( 'fairy_options[fairy-dropdown-text-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-dropdown-text-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-dropdown-text-color]',
        array(
            'label'       => esc_html__( 'Dropdown Menu Text Color', 'fairy' ),
            'description' => esc_html__( 'It will change the color of the dropdown menu text.', 'fairy' ),
            'section'     => 'menu-colors',
             'settings'  => 'fairy_options[fairy-dropdown-text-color]',
        )
    )
);

/* Menu Dropdown Text Color hover */
$wp_customize->add_setting( 'fairy_options[fairy-dropdown-text-hover-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-dropdown-text-hover-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-dropdown-text-hover-color]',
        array(
            'label'       => esc_html__( 'Dropdown Menu Text Hover Color', 'fairy' ),
            'description' => esc_html__( 'It will change the color of the dropdown menu text hover.', 'fairy' ),
            'section'     => 'menu-colors',
             'settings'  => 'fairy_options[fairy-dropdown-text-hover-color]',
        )
    )
);


/* Menu Dropdown whole background */
$wp_customize->add_setting( 'fairy_options[fairy-dropdown-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-dropdown-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-dropdown-background-color]',
        array(
            'label'       => esc_html__( 'Dropdown Menu Text Background Color', 'fairy' ),
            'description' => esc_html__( 'Dropdown menu whole background color can be set from here.', 'fairy' ),
            'section'     => 'menu-colors',
             'settings'  => 'fairy_options[fairy-dropdown-background-color]',
        )
    )
);

/* Menu Dropdown individual background */
$wp_customize->add_setting( 'fairy_options[fairy-dropdown-single-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-dropdown-single-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-dropdown-single-background-color]',
        array(
            'label'       => esc_html__( 'Dropdown Menu Text Individual Background Color', 'fairy' ),
            'description' => esc_html__( 'Dropdown menu individual background color can be set from here.', 'fairy' ),
            'section'     => 'menu-colors',
             'settings'  => 'fairy_options[fairy-dropdown-single-background-color]',
        )
    )
);

/*Blog Page Colors*/
$wp_customize->add_section(
    'blog-colors',
    array(
        'title' => __( 'Blog Section Colors', 'fairy' ),
        'panel' => 'colors',
    )
);

/* Blog Background */
$wp_customize->add_setting( 'fairy_options[fairy-blog-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-blog-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-blog-background-color]',
        array(
            'label'       => esc_html__( 'Blog Background Color', 'fairy' ),
            'description' => esc_html__( 'Whole Background color for the blog  sections.', 'fairy' ),
            'section'     => 'blog-colors',
             'settings'  => 'fairy_options[fairy-blog-background-color]',
        )
    )
);

/* Blog page heading color */
$wp_customize->add_setting( 'fairy_options[fairy-blog-heading-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-blog-heading-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-blog-heading-color]',
        array(
            'label'       => esc_html__( 'Blog Heading Color', 'fairy' ),
            'description' => esc_html__( 'Heading Color of Blog Page.', 'fairy' ),
            'section'     => 'blog-colors',
             'settings'  => 'fairy_options[fairy-blog-heading-color]',
        )
    )
);
/* Blog page heading color hover */
$wp_customize->add_setting( 'fairy_options[fairy-blog-heading-hover-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-blog-heading-hover-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-blog-heading-hover-color]',
        array(
            'label'       => esc_html__( 'Blog Heading Hover Color', 'fairy' ),
            'description' => esc_html__( 'Heading Hover Color of Blog Page.', 'fairy' ),
            'section'     => 'blog-colors',
             'settings'  => 'fairy_options[fairy-blog-heading-hover-color]',
        )
    )
);

/* Blog page paragraph color hover */
$wp_customize->add_setting( 'fairy_options[fairy-blog-paragraph-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-blog-paragraph-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-blog-paragraph-color]',
        array(
            'label'       => esc_html__( 'Blog Paragraph Color', 'fairy' ),
            'description' => esc_html__( 'Paragraph Color of Blog Page.', 'fairy' ),
            'section'     => 'blog-colors',
             'settings'  => 'fairy_options[fairy-blog-paragraph-color]',
        )
    )
);

/* Blog page Read more background */
$wp_customize->add_setting( 'fairy_options[fairy-blog-read-more-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-blog-read-more-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-blog-read-more-background-color]',
        array(
            'label'       => esc_html__( 'Read More Background Color', 'fairy' ),
            'description' => esc_html__( 'Read More Background Color of Blog Page.', 'fairy' ),
            'section'     => 'blog-colors',
             'settings'  => 'fairy_options[fairy-blog-read-more-background-color]',
        )
    )
);

/* Blog page Read more hover  */
$wp_customize->add_setting( 'fairy_options[fairy-blog-read-more-hover-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-blog-read-more-hover-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-blog-read-more-hover-color]',
        array(
            'label'       => esc_html__( 'Read More Hover Color', 'fairy' ),
            'description' => esc_html__( 'Read More Hover Color of Blog Page.', 'fairy' ),
            'section'     => 'blog-colors',
             'settings'  => 'fairy_options[fairy-blog-read-more-hover-color]',
        )
    )
);

/* Blog page Read More Text Color */
$wp_customize->add_setting( 'fairy_options[fairy-blog-read-more-text-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-blog-read-more-text-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-blog-read-more-text-color]',
        array(
            'label'       => esc_html__( 'Read More Text Color', 'fairy' ),
            'description' => esc_html__( 'Read More Text Color of Blog Page.', 'fairy' ),
            'section'     => 'blog-colors',
             'settings'  => 'fairy_options[fairy-blog-read-more-text-color]',
        )
    )
);

/* Pagination color */
$wp_customize->add_setting( 'fairy_options[fairy-blog-pagination-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-blog-pagination-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-blog-pagination-color]',
        array(
            'label'       => esc_html__( 'Pagination Background', 'fairy' ),
            'description' => esc_html__( 'Select the color for the pagination.', 'fairy' ),
            'section'     => 'blog-colors',
             'settings'  => 'fairy_options[fairy-blog-pagination-color]',
        )
    )
);

/* Pagination active and hover color */
$wp_customize->add_setting( 'fairy_options[fairy-blog-pagination-active-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-blog-pagination-active-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-blog-pagination-active-color]',
        array(
            'label'       => esc_html__( 'Pagination Active Color', 'fairy' ),
            'description' => esc_html__( 'Select the color for the active pagination.', 'fairy' ),
            'section'     => 'blog-colors',
             'settings'  => 'fairy_options[fairy-blog-pagination-active-color]',
        )
    )
);

/* Pagination hover color */
$wp_customize->add_setting( 'fairy_options[fairy-blog-pagination-hover-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-blog-pagination-hover-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-blog-pagination-hover-color]',
        array(
            'label'       => esc_html__( 'Pagination Hover Color', 'fairy' ),
            'description' => esc_html__( 'Select the color for the active pagination.', 'fairy' ),
            'section'     => 'blog-colors',
             'settings'  => 'fairy_options[fairy-blog-pagination-hover-color]',
        )
    )
);


/*Sidebar Colors*/
$wp_customize->add_section(
    'sidebar-colors',
    array(
        'title' => __( 'Sidebar Section Colors', 'fairy' ),
        'panel' => 'colors',
    )
);

/* Sidebar Background */
$wp_customize->add_setting( 'fairy_options[fairy-sidebar-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-sidebar-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-sidebar-background-color]',
        array(
            'label'       => esc_html__( 'Sidebar Color', 'fairy' ),
            'description' => esc_html__( 'Sidebar Background color for the Sidebar  sections.', 'fairy' ),
            'section'     => 'sidebar-colors',
             'settings'  => 'fairy_options[fairy-sidebar-background-color]',
        )
    )
);

/* Sidebar title  color */
$wp_customize->add_setting( 'fairy_options[fairy-sidebar-title-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-sidebar-title-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-sidebar-title-color]',
        array(
            'label'       => esc_html__( 'Widget Sidebar Title Color', 'fairy' ),
            'description' => esc_html__( 'Sidebar title color for the Sidebar  sections.', 'fairy' ),
            'section'     => 'sidebar-colors',
             'settings'  => 'fairy_options[fairy-sidebar-title-color]',
        )
    )
);

/* Sidebar content  color */
$wp_customize->add_setting( 'fairy_options[fairy-sidebar-content-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-sidebar-content-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-sidebar-content-color]',
        array(
            'label'       => esc_html__( 'Widget Sidebar Content Color', 'fairy' ),
            'description' => esc_html__( 'Sidebar content color for the Sidebar  sections.', 'fairy' ),
            'section'     => 'sidebar-colors',
             'settings'  => 'fairy_options[fairy-sidebar-content-color]',
        )
    )
);

/* Sidebar link  color */
$wp_customize->add_setting( 'fairy_options[fairy-sidebar-link-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-sidebar-link-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-sidebar-link-color]',
        array(
            'label'       => esc_html__( 'Sidebar Link Color', 'fairy' ),
            'description' => esc_html__( 'Link color of the Sidebar.', 'fairy' ),
            'section'     => 'sidebar-colors',
             'settings'  => 'fairy_options[fairy-sidebar-link-color]',
        )
    )
);

/* Sidebar link hover color */
$wp_customize->add_setting( 'fairy_options[fairy-sidebar-link-hover-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-sidebar-link-hover-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-sidebar-link-hover-color]',
        array(
            'label'       => esc_html__( 'Sidebar Link Hover Color', 'fairy' ),
            'description' => esc_html__( 'Link Hover color of the Sidebar.', 'fairy' ),
            'section'     => 'sidebar-colors',
             'settings'  => 'fairy_options[fairy-sidebar-link-hover-color]',
        )
    )
);

/*Footer Colors*/
$wp_customize->add_section(
    'footer-colors',
    array(
        'title' => __( 'Footer Section Colors', 'fairy' ),
        'panel' => 'colors',
    )
);

/* Footer Background */
$wp_customize->add_setting( 'fairy_options[fairy-footer-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-footer-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-footer-background-color]',
        array(
            'label'       => esc_html__( 'Footer Background Color', 'fairy' ),
            'description' => esc_html__( 'Footer Background color for the sections.', 'fairy' ),
            'section'     => 'footer-colors',
             'settings'  => 'fairy_options[fairy-footer-background-color]',
        )
    )
);

/* Footer Text Color */
$wp_customize->add_setting( 'fairy_options[fairy-footer-text-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-footer-text-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-footer-text-color]',
        array(
            'label'       => esc_html__( 'Footer Text Color', 'fairy' ),
            'description' => esc_html__( 'Footer text color for the sections.', 'fairy' ),
            'section'     => 'footer-colors',
             'settings'  => 'fairy_options[fairy-footer-text-color]',
        )
    )
);

/* Footer Text link Color */
$wp_customize->add_setting( 'fairy_options[fairy-footer-link-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-footer-link-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-footer-link-color]',
        array(
            'label'       => esc_html__( 'Footer Link Color', 'fairy' ),
            'description' => esc_html__( 'Footer link color for the sections.', 'fairy' ),
            'section'     => 'footer-colors',
             'settings'  => 'fairy_options[fairy-footer-link-color]',
        )
    )
);


/* Footer Text link hover Color */
$wp_customize->add_setting( 'fairy_options[fairy-footer-link-hover-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-footer-link-hover-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-footer-link-hover-color]',
        array(
            'label'       => esc_html__( 'Footer Link Hover Color', 'fairy' ),
            'description' => esc_html__( 'Footer link hover color for the sections.', 'fairy' ),
            'section'     => 'footer-colors',
             'settings'  => 'fairy_options[fairy-footer-link-hover-color]',
        )
    )
);

/* Go to top color */
$wp_customize->add_setting( 'fairy_options[fairy-footer-to-top-back-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-footer-to-top-back-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-footer-to-top-back-color]',
        array(
            'label'       => esc_html__( 'Footer Go to Top Background Color', 'fairy' ),
            'description' => esc_html__( 'Background color for go to the top.', 'fairy' ),
            'section'     => 'footer-colors',
             'settings'  => 'fairy_options[fairy-footer-to-top-back-color]',
        )
    )
);


/* Lower Footer Colors*/
$wp_customize->add_section(
    'lower-footer-colors',
    array(
        'title' => __( 'Lower Footer Section Colors', 'fairy' ),
        'panel' => 'colors',
    )
);

/* Footer Background */
$wp_customize->add_setting( 'fairy_options[fairy-lower-footer-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-lower-footer-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-lower-footer-background-color]',
        array(
            'label'       => esc_html__( 'Lower Footer Background Color', 'fairy' ),
            'description' => esc_html__( 'Lower Footer Background color for the sections.', 'fairy' ),
            'section'     => 'lower-footer-colors',
             'settings'  => 'fairy_options[fairy-lower-footer-background-color]',
        )
    )
);

/* Footer Text Color */
$wp_customize->add_setting( 'fairy_options[fairy-lower-footer-text-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-lower-footer-text-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-lower-footer-text-color]',
        array(
            'label'       => esc_html__( 'Lower Footer Text Color', 'fairy' ),
            'description' => esc_html__( 'Lower Footer text color for the sections.', 'fairy' ),
            'section'     => 'lower-footer-colors',
             'settings'  => 'fairy_options[fairy-lower-footer-text-color]',
        )
    )
);

/* Footer Text link Color */
$wp_customize->add_setting( 'fairy_options[fairy-lower-footer-link-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-lower-footer-link-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-lower-footer-link-color]',
        array(
            'label'       => esc_html__( 'Lower Footer Link Color', 'fairy' ),
            'description' => esc_html__( 'Lower Footer link color for the sections.', 'fairy' ),
            'section'     => 'lower-footer-colors',
             'settings'  => 'fairy_options[fairy-lower-footer-link-color]',
        )
    )
);


/* Footer Text link hover Color */
$wp_customize->add_setting( 'fairy_options[fairy-lower-footer-link-hover-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-lower-footer-link-hover-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-lower-footer-link-hover-color]',
        array(
            'label'       => esc_html__( 'Lower Footer Link Hover Color', 'fairy' ),
            'description' => esc_html__( 'Lower Footer link hover color for the sections.', 'fairy' ),
            'section'     => 'lower-footer-colors',
             'settings'  => 'fairy_options[fairy-lower-footer-link-hover-color]',
        )
    )
);

//Color option for slider hex color
$wp_customize->add_setting( 'fairy_options[fairy-overlay-color]' , array(
    'default'           => $default['fairy-overlay-color'], // Use any HEX or RGBA value.
    'transport'         => 'refresh',
    'sanitize_callback' => 'fairy_alpha_color_sanitization_callback'
) );
include_once get_theme_file_path( 'candidthemes/alpha-color/src/ColorAlpha.php' );

$wp_customize->add_control( new ColorAlpha( $wp_customize, 'fairy_options[fairy-overlay-color]', [
    'label'      => __( 'Overlay First Color', 'fairy' ),
    'section'    => 'fairy_slider_section',
    'settings'   => 'fairy_options[fairy-overlay-color]',
    'priority'  => 15,
    'active_callback'=>'fairy_slider_active_callback',
] ) );


$wp_customize->add_setting( 'fairy_options[fairy-overlay-second-color]' , array(
    'default'           => $default['fairy-overlay-second-color'], // Use any HEX or RGBA value.
    'transport'         => 'refresh',
    'sanitize_callback' => 'fairy_alpha_color_sanitization_callback'
) );

$wp_customize->add_control( new ColorAlpha( $wp_customize, 'fairy_options[fairy-overlay-second-color]', [
    'label'      => __( 'Overlay Second Color', 'fairy' ),
    'section'    => 'fairy_slider_section',
    'settings'   => 'fairy_options[fairy-overlay-second-color]',
    'priority'  => 15,
    'active_callback'=>'fairy_slider_active_callback',
] ) );


//hex color for promo boxes
$wp_customize->add_setting( 'fairy_options[fairy-boxes-overlay-first-color]' , array(
    'default'           => $default['fairy-boxes-overlay-first-color'], // Use any HEX or RGBA value.
    'transport'         => 'refresh',
    'sanitize_callback' => 'fairy_alpha_color_sanitization_callback'
) );

$wp_customize->add_control( new ColorAlpha( $wp_customize, 'fairy_options[fairy-boxes-overlay-first-color]', [
    'label'      => __( 'Overlay First Color', 'fairy' ),
    'section'    => 'fairy_category_boxes_section',
    'settings'   => 'fairy_options[fairy-boxes-overlay-first-color]',
    'priority'  => 15,
    'active_callback'=>'fairy_category_enable_boxes_callback',
] ) );

$wp_customize->add_setting( 'fairy_options[fairy-boxes-overlay-second-color]' , array(
    'default'           => $default['fairy-boxes-overlay-second-color'], // Use any HEX or RGBA value.
    'transport'         => 'refresh',
    'sanitize_callback' => 'fairy_alpha_color_sanitization_callback'
) );

$wp_customize->add_control( new ColorAlpha( $wp_customize, 'fairy_options[fairy-boxes-overlay-second-color]', [
    'label'      => __( 'Overlay Second Color', 'fairy' ),
    'section'    => 'fairy_category_boxes_section',
    'settings'   => 'fairy_options[fairy-boxes-overlay-second-color]',
    'priority'  => 15,
    'active_callback'=>'fairy_category_enable_boxes_callback',
] ) );
