<?php
/**
 *  Fairy Single Page Option
 *
 * @since Fairy 1.0.0
 *
 */
/*Single Page Options*/
$wp_customize->add_section( 'fairy_single_page_section', array(
   'priority'       => 40,
   'capability'     => 'edit_theme_options',
   'theme_supports' => '',
   'title'          => __( 'Single Post Options', 'fairy' ),
   'panel' 		 => 'fairy_panel',
) );

/*Title position of Single post*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-single-title-position]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-single-title-position'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-single-title-position]', array(
    'label'     => __( 'Title and Meta Position', 'fairy' ),
    'description' => __('Change the position of title and meta on single post.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-enable-single-title-position]',
    'type'      => 'select',
    'choices' => array(
        'left-title'    => __('Title and Meta Left','fairy'),
        'center-title'   => __('Title and Meta Center','fairy'),
    ),
    'priority'  => 15,
) );


/*Featured Image Option*/
$wp_customize->add_setting( 'fairy_options[fairy-single-page-featured-image]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-single-page-featured-image'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-single-page-featured-image]', array(
    'label'     => __( 'Enable Featured Image', 'fairy' ),
    'description' => __('You can hide or show featured image on single page.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-single-page-featured-image]',
    'type'      => 'checkbox',
    'priority'  => 15,
) );
/*Enable Category*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-single-category]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-single-category'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-single-category]', array(
    'label'     => __( 'Enable Category', 'fairy' ),
    'description' => __('Checked to Enable Category In Single post and page.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-enable-single-category]',
    'type'      => 'checkbox',
    'priority'  => 20,
) );
/*Enable Date*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-single-date]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-single-date'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-single-date]', array(
    'label'     => __( 'Enable Date', 'fairy' ),
    'description' => __('Checked to Enable Date In Single post and page.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-enable-single-date]',
    'type'      => 'checkbox',
    'priority'  => 20,
) );
/*Enable Author*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-single-author]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-single-author'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-single-author]', array(
    'label'     => __( 'Enable Author', 'fairy' ),
    'description' => __('Checked to Enable Author In Single post and page.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-enable-single-author]',
    'type'      => 'checkbox',
    'priority'  => 20,
) );
/*Hide Tags in Single Page*/
$wp_customize->add_setting( 'fairy_options[fairy-single-page-tags]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-single-page-tags'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-single-page-tags]', array(
    'label'     => __( 'Enable Posts Tags', 'fairy' ),
    'description' => __('You can enable the post tags in single page.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-single-page-tags]',
    'type'      => 'checkbox',
    'priority'  => 15,
) );
/*Comment Area*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-single-comments-form]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-single-comments-form'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-single-comments-form]', array(
    'label'     => __( 'Enable Comment Form', 'fairy' ),
    'description' => __('Do you want to hide the comment form on the single page? You can manage form here.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-enable-single-comments-form]',
    'type'      => 'checkbox',
    'priority'  => 20,
) );

/*Move Comment Area*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-single-comments-form-move]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-single-comments-form-move'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-single-comments-form-move]', array(
    'label'     => __( 'Move Comment Form Above Comments', 'fairy' ),
    'description' => __('Please select the option from the dropdown to move the comment form above the comments.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-enable-single-comments-form-move]',
    'type'      => 'select',
    'choices' => array(
        'default'    => __('Default Location Below Comments','fairy'),
        'move-top'   => __('Move Top Before Comments','fairy'),
    ),
    'priority'  => 20,
) );

/*Enable Underline in single post link place */
$wp_customize->add_setting( 'fairy_options[fairy-enable-underline-link]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-underline-link'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-underline-link]', array(
    'label'     => __( 'Enable Underline on Link', 'fairy' ),
    'description' => __('If you enabled this, you will see the underline in the links. You can change it color from the general section of colors.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-enable-underline-link]',
    'type'      => 'checkbox',
    'priority'  => 20,
) );

/*Show hide author information box*/
$wp_customize->add_setting( 'fairy_options[fairy-show-hide-author-box]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-show-hide-author-box'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-show-hide-author-box]', array(
    'label'     => __( 'Show/Hide Author Box', 'fairy' ),
    'description' => __('Author Information can be managed from this section.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-show-hide-author-box]',
    'type'      => 'checkbox',
    'priority'  => 20,
) );

/*Related Post Options*/
$wp_customize->add_setting( 'fairy_options[fairy-single-page-related-posts]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-single-page-related-posts'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-single-page-related-posts]', array(
    'label'     => __( 'Enable Related Posts', 'fairy' ),
    'description' => __('3 Post from similar category will display at the end of the page.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-single-page-related-posts]',
    'type'      => 'checkbox',
    'priority'  => 20,
) );
/*callback functions related posts*/
if ( !function_exists('fairy_related_post_callback') ) :
    function fairy_related_post_callback(){
        global $fairy_theme_options;
        $fairy_theme_options = fairy_get_options_value();
        $related_posts = absint($fairy_theme_options['fairy-single-page-related-posts']);
        if( 1 == $related_posts ){
            return true;
        }
        else{
            return false;
        }
    }
endif;
/*Related Post Title*/
$wp_customize->add_setting( 'fairy_options[fairy-single-page-related-posts-title]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-single-page-related-posts-title'],
    'sanitize_callback' => 'sanitize_text_field'
) );
$wp_customize->add_control( 'fairy_options[fairy-single-page-related-posts-title]', array(
    'label'     => __( 'Related Posts Title', 'fairy' ),
    'description' => __('Give the appropriate title for related posts', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-single-page-related-posts-title]',
    'type'      => 'text',
    'priority'  => 20,
    'active_callback'=>'fairy_related_post_callback'
) );

/*Related Post Selection types*/
$wp_customize->add_setting( 'fairy_options[fairy-single-page-related-selection-types]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-single-page-related-selection-types'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-single-page-related-selection-types]', array(
    'label'     => __( 'Related Posts Selection', 'fairy' ),
    'description' => __('You can show the related post from Category or Tags', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-single-page-related-selection-types]',
    'type'      => 'select',
    'choices' => array(
        'category'    => __('Related Post from Category','fairy'),
        'tags'   => __('Related Post from Tags','fairy'),
    ),
    'priority'  => 20,
    'active_callback'=>'fairy_related_post_callback'
) );

/*Related Post Number*/
$wp_customize->add_setting( 'fairy_options[fairy-single-page-related-posts-number]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-single-page-related-posts-number'],
    'sanitize_callback' => 'absint'
) );
$wp_customize->add_control( 'fairy_options[fairy-single-page-related-posts-number]', array(
    'label'     => __( 'Related Posts Number', 'fairy' ),
    'description' => __('Enter the number for the required related posts', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-single-page-related-posts-number]',
    'type'      => 'number',
    'priority'  => 20,
    'active_callback'=>'fairy_related_post_callback'
) );
/*Related Post Image*/
$wp_customize->add_setting( 'fairy_options[fairy-single-page-related-posts-image]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-single-page-related-posts-image'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-single-page-related-posts-image]', array(
    'label'     => __( 'Show Image on Related Posts', 'fairy' ),
    'description' => __('You can show or hide the related post featured image.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-single-page-related-posts-image]',
    'type'      => 'checkbox',
    'priority'  => 20,
    'active_callback'=>'fairy_related_post_callback'
) );

/*Related Post Date*/
$wp_customize->add_setting( 'fairy_options[fairy-single-page-related-posts-date]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-single-page-related-posts-date'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-single-page-related-posts-date]', array(
    'label'     => __( 'Show Date on Related Posts', 'fairy' ),
    'description' => __('You can show or hide the related post date.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-single-page-related-posts-date]',
    'type'      => 'checkbox',
    'priority'  => 20,
    'active_callback'=>'fairy_related_post_callback'
) );

/*Related Post Author*/
$wp_customize->add_setting( 'fairy_options[fairy-single-page-related-posts-author]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-single-page-related-posts-author'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-single-page-related-posts-author]', array(
    'label'     => __( 'Show Author on Related Posts', 'fairy' ),
    'description' => __('You can show or hide the related post author name.', 'fairy'),
    'section'   => 'fairy_single_page_section',
    'settings'  => 'fairy_options[fairy-single-page-related-posts-author]',
    'type'      => 'checkbox',
    'priority'  => 20,
    'active_callback'=>'fairy_related_post_callback'
) );