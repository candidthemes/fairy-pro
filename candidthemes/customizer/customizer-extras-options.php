<?php 
/**
 *  Fairy Extra Settings Option
 *
 * @since Fairy 1.0.0
 *
 */
/*Extra Options*/
$wp_customize->add_section( 'fairy_extras_options', array(
    'priority'       => 60,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Extra Options', 'fairy' ),
    'panel'          => 'fairy_panel',
) );

/*Category Design*/
$wp_customize->add_setting( 'fairy_options[fairy-category-title-design]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-category-title-design'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-category-title-design]', array(
    'choices' => array(
        'theme-default'    => __('Line Category Design','fairy'),
        'new-category'    => __('Background Category Design','fairy'),
    ),
    'label'     => __( 'Select the required design of Category', 'fairy' ),
    'description' => __('You can choose the category design from this option.', 'fairy'),
    'section'   => 'fairy_extras_options',
    'settings'  => 'fairy_options[fairy-category-title-design]',
    'type'      => 'select',
    'priority'  => 15,
) );

/*post published or updated date*/
$wp_customize->add_setting( 'fairy_options[fairy-post-published-updated-date]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-post-published-updated-date'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-post-published-updated-date]', array(
   'choices' => array(
    'post-published'    => __('Show Post Published Date','fairy'),
    'post-updated'   => __('Show Post Updated Date','fairy'),
),
   'label'     => __( 'Show Post Publish or Updated Date', 'fairy' ),
   'description' => __('Show either post published or updated date.', 'fairy'),
   'section'   => 'fairy_extras_options',
   'settings'  => 'fairy_options[fairy-post-published-updated-date]',
   'type'      => 'select',
   'priority'  => 15,
) );
/*Font awesome version loading*/
$wp_customize->add_setting( 'fairy_options[fairy-font-awesome-version-loading]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-font-awesome-version-loading'],
    'sanitize_callback' => 'fairy_sanitize_select'
 ) );
 $wp_customize->add_control( 'fairy_options[fairy-font-awesome-version-loading]', array(
   'choices' => array(
    'version-4'    => __('Current Theme Used Version 4','fairy'),
    'version-5'   => __('New Fontaweome Version 5','fairy'),
 ),
   'label'     => __( 'Select the preferred fontawesome version', 'fairy' ),
   'description' => __('You can select the latest fontawesome version to get more options for icons', 'fairy'),
   'section'   => 'fairy_extras_options',
   'settings'  => 'fairy_options[fairy-font-awesome-version-loading]',
   'type'      => 'select',
   'priority'  => 15,
 ) );