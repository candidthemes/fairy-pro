<?php
/**
 *  Fairy Sidebar Option
 *
 * @since Fairy 1.0.0
 *
 */
/*Blog Page Options*/
$wp_customize->add_section( 'fairy_sidebar_section', array(
   'priority'       => 45,
   'capability'     => 'edit_theme_options',
   'theme_supports' => '',
   'title'          => __( 'Sidebar Options', 'fairy' ),
   'panel' 		 => 'fairy_panel',
) );
/*Front Page Sidebar Layout*/
$wp_customize->add_setting( 'fairy_options[fairy-sidebar-blog-page]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-sidebar-blog-page'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-sidebar-blog-page]', array(
   'choices' => array(
    'right-sidebar'   => __('Right Sidebar','fairy'),
    'left-sidebar'    => __('Left Sidebar','fairy'),
    'no-sidebar'      => __('No Sidebar','fairy'),
    'middle-column'   => __('Middle Column','fairy')
),
   'label'     => __( 'Front Page Sidebar', 'fairy' ),
   'description' => __('This sidebar will work for home page or blog page.', 'fairy'),
   'section'   => 'fairy_sidebar_section',
   'settings'  => 'fairy_options[fairy-sidebar-blog-page]',
   'type'      => 'select',
   'priority'  => 10,
) );
/*Archive Page Sidebar Layout*/
$wp_customize->add_setting( 'fairy_options[fairy-sidebar-archive-page]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-sidebar-archive-page'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-sidebar-archive-page]', array(
   'choices' => array(
    'right-sidebar'   => __('Right Sidebar','fairy'),
    'left-sidebar'    => __('Left Sidebar','fairy'),
    'no-sidebar'      => __('No Sidebar','fairy'),
    'middle-column'   => __('Middle Column','fairy')
),
   'label'     => __( 'Archive Page Sidebar', 'fairy' ),
   'description' => __('This sidebar will work for all category and archive pages.', 'fairy'),
   'section'   => 'fairy_sidebar_section',
   'settings'  => 'fairy_options[fairy-sidebar-archive-page]',
   'type'      => 'select',
   'priority'  => 10,
) );

/*Single Page Sidebar Layout*/
$wp_customize->add_setting( 'fairy_options[fairy-sidebar-single-page]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-sidebar-single-page'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-sidebar-single-page]', array(
   'choices' => array(
    'right-sidebar'   => __('Right Sidebar','fairy'),
    'left-sidebar'    => __('Left Sidebar','fairy'),
    'no-sidebar'      => __('No Sidebar','fairy'),
    'middle-column'   => __('Middle Column','fairy')
),
   'label'     => __( 'Single Page Sidebar', 'fairy' ),
   'description' => __('This sidebar will work for all single pages.', 'fairy'),
   'section'   => 'fairy_sidebar_section',
   'settings'  => 'fairy_options[fairy-sidebar-single-page]',
   'type'      => 'select',
   'priority'  => 10,
) );

/*Sticky Sidebar Setting*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-sticky-sidebar]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-sticky-sidebar'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-sticky-sidebar]', array(
    'label'     => __( 'Sticky Sidebar Option', 'fairy' ),
    'description' => __('Enable and Disable sticky sidebar from this section.', 'fairy'),
    'section'   => 'fairy_sidebar_section',
    'settings'  => 'fairy_options[fairy-enable-sticky-sidebar]',
    'type'      => 'checkbox',
    'priority'  => 15,
) );

/*Design of Sidebar Widgets Title*/
$wp_customize->add_setting( 'fairy_options[fairy-sidebar-title-designs]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-sidebar-title-designs'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-sidebar-title-designs]', array(
   'choices' => array(
    'default-title'   => __('Default Title Design','fairy'),
    'title-one'    => __('Title Design One','fairy'),
    'title-two'      => __('Title Design Two','fairy'),
    'title-three'   => __('Title Design Three','fairy')
),
   'label'     => __( 'Design of Sidebar Titles', 'fairy' ),
   'description' => __('You can change the design of Sidebar.', 'fairy'),
   'section'   => 'fairy_sidebar_section',
   'settings'  => 'fairy_options[fairy-sidebar-title-designs]',
   'type'      => 'select',
   'priority'  => 10,
) );