<?php
/**
 *  Fairy Blog Page Option
 *
 * @since Fairy 1.0.0
 *
 */
/*Blog Page Options*/
$wp_customize->add_section( 'fairy_blog_page_section', array(
   'priority'       => 35,
   'capability'     => 'edit_theme_options',
   'theme_supports' => '',
   'title'          => __( 'Blog Section Options', 'fairy' ),
   'panel' 		 => 'fairy_panel',
) );
/*Blog Page column number*/
$wp_customize->add_setting( 'fairy_options[fairy-column-blog-page]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-column-blog-page'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-column-blog-page]', array(
   'choices' => array(
    'one-column'    => __('Single Column','fairy'),
    'two-columns'   => __('Two Column','fairy'),
    'three-columns' => __('Three Column','fairy'),
    'alternate' => __('One and Two Column','fairy'),
),
   'label'     => __( 'Blog Layout Column', 'fairy' ),
   'description' => __('You can change the blog page and archive page layouts', 'fairy'),
   'section'   => 'fairy_blog_page_section',
   'settings'  => 'fairy_options[fairy-column-blog-page]',
   'type'      => 'select',
   'priority'  => 10,
) );
/*Blog Page Layout Masorny*/
$wp_customize->add_setting( 'fairy_options[fairy-blog-page-masonry-normal]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-blog-page-masonry-normal'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-blog-page-masonry-normal]', array(
   'choices' => array(
    'normal'    => __('Normal Layout','fairy'),
    'masonry'   => __('Masonry Layout','fairy'),
),
   'label'     => __( 'Masonry or Normal Layout', 'fairy' ),
   'description' => __('Some image layout option will not work in masonry.', 'fairy'),
   'section'   => 'fairy_blog_page_section',
   'settings'  => 'fairy_options[fairy-blog-page-masonry-normal]',
   'type'      => 'select',
   'priority'  => 10,
) );

/*Blog Page Layout*/
$wp_customize->add_setting( 'fairy_options[fairy-blog-page-image-position]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-blog-page-image-position'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-blog-page-image-position]', array(
   'choices' => array(
    'left-image'    => __('Left Image','fairy'),
    'right-image'   => __('Right Image','fairy'),
    'full-image'   => __('Full Image','fairy'),
    'hide-image'   => __('Hide Image','fairy'),
),
   'label'     => __( 'Image alignment in blog page', 'fairy' ),
   'description' => __('Select the required blog page option.', 'fairy'),
   'section'   => 'fairy_blog_page_section',
   'settings'  => 'fairy_options[fairy-blog-page-image-position]',
   'type'      => 'select',
   'priority'  => 10,
) );

/*Blog Page Title*/
$wp_customize->add_setting( 'fairy_options[fairy-title-position-blog-page]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-title-position-blog-page'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-title-position-blog-page]', array(
   'choices' => array(
    'left-title'    => __('Blog Title Meta Left','fairy'),
    'center-title'   => __('Blog Title Meta Center','fairy'),
),
   'label'     => __( 'Title of Blog and Archive Page', 'fairy' ),
   'description' => __('The title of the blog and archive page can be center and left.', 'fairy'),
   'section'   => 'fairy_blog_page_section',
   'settings'  => 'fairy_options[fairy-title-position-blog-page]',
   'type'      => 'select',
   'priority'  => 10,
) );

/*Blog content alignment*/
$wp_customize->add_setting( 'fairy_options[fairy-content-position-blog-page]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-content-position-blog-page'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-content-position-blog-page]', array(
   'choices' => array(
    'left-content'    => __('Blog Content Left','fairy'),
    'center-content'   => __('Blog Content Center','fairy'),
),
   'label'     => __( 'Content of Blog and Archive Page', 'fairy' ),
   'description' => __('The content of the blog and archive page can be center and left.', 'fairy'),
   'section'   => 'fairy_blog_page_section',
   'settings'  => 'fairy_options[fairy-content-position-blog-page]',
   'type'      => 'select',
   'priority'  => 10,
) );

/*Blog Page Show content from*/
$wp_customize->add_setting( 'fairy_options[fairy-content-show-from]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-content-show-from'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-content-show-from]', array(
   'choices' => array(
    'excerpt'    => __('Excerpt','fairy'),
    'content'    => __('Content','fairy'),
    'hide'    => __('Hide','fairy')
),
   'label'     => __( 'Select Content Display Option', 'fairy' ),
   'description' => __('You can enable excerpt from Screen Options inside post section of dashboard', 'fairy'),
   'section'   => 'fairy_blog_page_section',
   'settings'  => 'fairy_options[fairy-content-show-from]',
   'type'      => 'select',
   'priority'  => 10,
) );

/*Blog image size*/
$wp_customize->add_setting( 'fairy_options[fairy-image-size-blog-page]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-image-size-blog-page'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-image-size-blog-page]', array(
   'choices' => array(
    'cropped-image'    => __('Cropped Image','fairy'),
    'original-image'   => __('Original Image','fairy'),
),
   'label'     => __( 'Size of the image, either cropped or original', 'fairy' ),
   'description' => __('The image will be either cropped or original size based on the image. Recommended image size is.', 'fairy'),
   'section'   => 'fairy_blog_page_section',
   'settings'  => 'fairy_options[fairy-image-size-blog-page]',
   'type'      => 'select',
   'priority'  => 10,
) );

/*Blog Page excerpt length*/
$wp_customize->add_setting( 'fairy_options[fairy-excerpt-length]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-excerpt-length'],
    'sanitize_callback' => 'absint'
) );
$wp_customize->add_control( 'fairy_options[fairy-excerpt-length]', array(
    'label'     => __( 'Size of Excerpt Content', 'fairy' ),
    'description' => __('Enter the number per Words to show the content in blog page.', 'fairy'),
    'section'   => 'fairy_blog_page_section',
    'settings'  => 'fairy_options[fairy-excerpt-length]',
    'type'      => 'number',
    'priority'  => 10,
) );
/*Blog Page Pagination Options*/
$wp_customize->add_setting( 'fairy_options[fairy-pagination-options]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-pagination-options'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-pagination-options]', array(
   'choices' => array(
    'default'    => __('Default','fairy'),
    'numeric'    => __('Numeric','fairy'),
    'ajax'    => __('Loading in Same Page','fairy'),
    'infinite'    => __('Auto Loading','fairy'),
    'hide'    => __('Hide Pagination','fairy'),
),
   'label'     => __( 'Pagination Types', 'fairy' ),
   'description' => __('Select the Required Pagination Type', 'fairy'),
   'section'   => 'fairy_blog_page_section',
   'settings'  => 'fairy_options[fairy-pagination-options]',
   'type'      => 'select',
   'priority'  => 10,
) );

/*Pagination Text for Defaut & Numeric Next*/
$wp_customize->add_setting( 'fairy_options[fairy-default-pagination-text-one]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-default-pagination-text-one'],
    'sanitize_callback' => 'sanitize_text_field'
) );
$wp_customize->add_control( 'fairy_options[fairy-default-pagination-text-one]', array(
    'label'     => __( 'Enter Next Pagination Text', 'fairy' ),
    'description' => __('Next or Previous Text.', 'fairy'),
    'section'   => 'fairy_blog_page_section',
    'settings'  => 'fairy_options[fairy-default-pagination-text-one]',
    'type'      => 'text',
    'priority'  => 10,
) );
/*Pagination Text for Default & Numeric Previous*/
$wp_customize->add_setting( 'fairy_options[fairy-default-pagination-text-two]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-default-pagination-text-two'],
    'sanitize_callback' => 'sanitize_text_field'
) );
$wp_customize->add_control( 'fairy_options[fairy-default-pagination-text-two]', array(
    'label'     => __( 'Enter Previous Pagination Text', 'fairy' ),
    'description' => __('Next or Previous Text.', 'fairy'),
    'section'   => 'fairy_blog_page_section',
    'settings'  => 'fairy_options[fairy-default-pagination-text-two]',
    'type'      => 'text',
    'priority'  => 10,
) );

/*Blog Page read more text*/
$wp_customize->add_setting( 'fairy_options[fairy-read-more-text]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-read-more-text'],
    'sanitize_callback' => 'sanitize_text_field'
) );
$wp_customize->add_control( 'fairy_options[fairy-read-more-text]', array(
    'label'     => __( 'Enter Read More Text', 'fairy' ),
    'description' => __('Read more text for blog and archive page.', 'fairy'),
    'section'   => 'fairy_blog_page_section',
    'settings'  => 'fairy_options[fairy-read-more-text]',
    'type'      => 'text',
    'priority'  => 10,
) );

/*Blog Page author*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-blog-author]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-blog-author'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-blog-author]', array(
    'label'     => __( 'Show Author', 'fairy' ),
    'description' => __('Checked to show the author.', 'fairy'),
    'section'   => 'fairy_blog_page_section',
    'settings'  => 'fairy_options[fairy-enable-blog-author]',
    'type'      => 'checkbox',
    'priority'  => 10,
) );
/*Blog Page category*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-blog-category]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-blog-category'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-blog-category]', array(
    'label'     => __( 'Show Category', 'fairy' ),
    'description' => __('Checked to show the category.', 'fairy'),
    'section'   => 'fairy_blog_page_section',
    'settings'  => 'fairy_options[fairy-enable-blog-category]',
    'type'      => 'checkbox',
    'priority'  => 10,
) );
/*Blog Page date*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-blog-date]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-enable-blog-date'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-blog-date]', array(
    'label'     => __( 'Show Date', 'fairy' ),
    'description' => __('Checked to show the Date.', 'fairy'),
    'section'   => 'fairy_blog_page_section',
    'settings'  => 'fairy_options[fairy-enable-blog-date]',
    'type'      => 'checkbox',
    'priority'  => 10,
) );