<?php
/**
 *  Fairy Site Layout Option
 *
 * @since Fairy 1.0.0
 *
 */
/*Site Layout Section*/
$wp_customize->add_section( 'fairy_site_layout_section', array(
   'priority'       => 30,
   'capability'     => 'edit_theme_options',
   'theme_supports' => '',
   'title'          => __( 'Site Layout Options', 'fairy' ),
   'panel'     => 'fairy_panel',
) );

/*Blog Section Box Shadow*/
$wp_customize->add_setting( 'fairy_options[fairy-site-layout-blog-overlay]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-site-layout-blog-overlay'],
    'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-site-layout-blog-overlay]', array(
    'label'     => __( 'Change the Blog and sidebar Box Shadow', 'fairy' ),
    'description' => __('Remove the blog and sidebar box shadow completely.', 'fairy'),
    'section'   => 'fairy_site_layout_section',
    'settings'  => 'fairy_options[fairy-site-layout-blog-overlay]',
    'type'      => 'checkbox',
    'priority'  => 15,
) );

/*Site Layout settings*/
$wp_customize->add_setting( 'fairy_options[fairy-site-layout-options]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-site-layout-options'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-site-layout-options]', array(
   'choices' => array(
    'boxed'    => __('Boxed Layout','fairy'),
    'full-width'    => __('Full Width','fairy')
),
   'label'     => __( 'Site Layout Option', 'fairy' ),
   'description' => __('You can make the overall site full width or boxed in nature.', 'fairy'),
   'section'   => 'fairy_site_layout_section',
   'settings'  => 'fairy_options[fairy-site-layout-options]',
   'type'      => 'select',
   'priority'  => 30,
) );

/*callback functions header section*/
if ( !function_exists('fairy_boxed_layout_width') ) :
  function fairy_boxed_layout_width(){
      global $fairy_theme_options;
      $fairy_theme_options = fairy_get_options_value();
      $boxed_width = esc_attr($fairy_theme_options['fairy-site-layout-options']);
      if( 'boxed' == $boxed_width ){
          return true;
      }
      else{
          return false;
      }
  }
endif;

/*Site Layout width*/
$wp_customize->add_setting( 'fairy_options[fairy-boxed-width-options]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-boxed-width-options'],
    'sanitize_callback' => 'absint'
) );
$wp_customize->add_control( 'fairy_options[fairy-boxed-width-options]', array(
   'label'     => __( 'Set Boxed Width Range', 'fairy' ),
   'description' => __('Make the required width of your boxed layout. Select a custom width for the boxed layout. Minimim is 1200px and maximum is 1500px.', 'fairy'),
   'section'   => 'fairy_site_layout_section',
   'settings'  => 'fairy_options[fairy-boxed-width-options]',
   'type'      => 'range',
   'priority'  => 30,
   'input_attrs' => array(
          'min' => 1200,
          'max' => 1500,
        ),
   'active_callback' => 'fairy_boxed_layout_width',
) );

/* Boxed background */
$wp_customize->add_setting( 'fairy_options[fairy-boxed-layout-background-color]',
    array(
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'transport' => 'refresh',
        'default'           => $default['fairy-boxed-layout-background-color'],
    )
);
$wp_customize->add_control(
    new WP_Customize_Color_Control(
        $wp_customize,
        'fairy_options[fairy-boxed-layout-background-color]',
        array(
            'label'       => esc_html__( 'Box Layout Background', 'fairy' ),
            'description' => esc_html__( 'It will change the color of boxed background.', 'fairy' ),
            'section'     => 'fairy_site_layout_section',
             'settings'  => 'fairy_options[fairy-boxed-layout-background-color]',
             'active_callback' => 'fairy_boxed_layout_width',
             'priority'  => 30,
        )
    )
);

/*Site dark and light Layout settings*/
$wp_customize->add_setting( 'fairy_options[fairy-site-dark-light-layout-options]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-site-dark-light-layout-options'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-site-dark-light-layout-options]', array(
    'choices' => array(
        'default-light'    => __('Default Light Layout','fairy'),
        'dark-layout'    => __('Dark Layout','fairy')
    ),
    'label'     => __( 'Dark and Light Layout Option', 'fairy' ),
    'description' => __('Make the overall layout of site dark ad light.', 'fairy'),
    'section'   => 'fairy_site_layout_section',
    'settings'  => 'fairy_options[fairy-site-dark-light-layout-options]',
    'type'      => 'select',
    'priority'  => 30,
) );