<?php
    /*
     * Customizer default value loaded here.
     */
    $default = fairy_default_theme_options_values();

    /*
    * Theme Options Panel
    */
    $wp_customize->add_panel( 'fairy_panel', array(
     'priority' => 25,
     'capability' => 'edit_theme_options',
     'title' => __( 'Fairy Theme Options', 'fairy' ),
    ) );

    /**
     * Load Customizer Colors
     *
     * Colors
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-colors.php';

    /**
     * Load Customizer Header Top setting
     *
     * Header section need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-top-header.php';

    /**
     * Load Customizer Hheader Types setting
     *
     * Header Types section need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-header-types.php';

    /**
     * Load Customizer Menu setting
     *
     * Menu section need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-menu-section.php';

    /**
     * Load Customizer Slider setting
     *
     * Slider section need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-slider.php';

    /**
     * Load Customizer category setting
     *
     * Category section need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-category-boxes-options.php';


    /**
     * Load Customizer Site Layout setting
     *
     * Site Layout need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-site-layout.php';
    /**
     * Load Customizer Sidebar setting
     *
     * Sidebar need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-sidebar-options.php';

    /**
     * Load Customizer Blog setting
     *
     * Blog need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-blog-page.php';

    /**
     * Load Customizer Single page setting
     *
     * Single page need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-single-page.php';

    /**
     * Load Customizer Breadcrumb setting
     *
     * Breadcrumb need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-breadcrumb.php';

    /**
     * Load Customizer footer setting
     *
     * footer need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-footer-settings.php';

    /**
     * Load Customizer extra setting
     *
     * extra need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-extras-options.php';

    /**
     * Load Customizer extra setting
     *
     * extra need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-typography.php';

     /**
     * Load Customizer category colir
     *
     * category color need to manage from here
    */
    require get_template_directory() . '/candidthemes/customizer/customizer-category-color.php';

