<?php
/**
 *  Fairy Typography Option
 *
 * @since Fairy 1.0.0
 *
 */
$wp_customize->add_panel( 'fairy_typography', array(
    'priority' => 30,
    'capability' => 'edit_theme_options',
    'title' => __( 'Fonts Options', 'fairy' ),
) );

/*
* Site Title Fonts Options
* Site Title Font Option Section
* Fairy 1.0.0
*/
/*Site Title*/
$wp_customize->add_section( 'fairy_site_title_font_options', array(
    'priority'       => 20,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Site Title Font', 'fairy' ),
    'panel'         => 'fairy_typography',
) );
/*Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-font-site-title-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-font-site-title-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-font-site-title-family-url]', array(
    'label'     => __( 'Select Title Font Family', 'fairy' ),
    'description' => __( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'   => $choices,
    'section'   => 'fairy_site_title_font_options',
    'settings'  => 'fairy_options[fairy-font-site-title-family-url]',
    'type'      => 'select',
    'priority'  => 10,
) );
/*Heading Font Size*/
$wp_customize->add_setting('fairy_options[fairy-site-title-font-size]', array(
    'default'     => $default['fairy-site-title-font-size'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range',
    'settings'=>'fairy_options[fairy-site-title-font-size]'
));

$wp_customize->add_control('fairy_options[fairy-site-title-font-size]', array(
    'label' => __('Site Title Font Size', 'fairy'),
    'section' => 'fairy_site_title_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Font Size.', 'fairy' ),
    'input_attrs' => array(
        'min' => '24',
        'max' => '54',
        'step' => '1',
    ),
    'priority'  => 10,
));
/*Heading Line height*/
$wp_customize->add_setting('fairy_options[fairy-site-title-font-line-height]', array(
    'default'     => $default['fairy-site-title-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-site-title-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-site-title-font-line-height]', array(
    'label' => __('Site Title Font Line Height', 'fairy'),
    'section' => 'fairy_site_title_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 10,
));
/*Heading letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-site-title-letter-spacing]', array(
    'default' => $default['fairy-site-title-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-site-title-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-site-title-letter-spacing]', array(
    'label'   => __('Site Title Font Letter Space', 'fairy'),
    'section' => 'fairy_site_title_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-2',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 10,
));

/*
* Font and Typography Options
* Paragraph Option Section
* Fairy 1.0.0
*/
$wp_customize->add_section( 'fairy_font_options', array(
   'priority'       => 20,
   'capability'     => 'edit_theme_options',
   'theme_supports' => '',
   'title'          => __( 'Paragraph Font', 'fairy' ),
   'panel' 		 => 'fairy_typography',
) );
/*Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-font-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-font-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-font-family-url]', array(
    'label'     => __( 'Body Paragraph Font Family', 'fairy' ),
    'description' =>__( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'  	=> $choices,
    'section'   => 'fairy_font_options',
    'settings'  => 'fairy_options[fairy-font-family-url]',
    'type'      => 'select',
    'priority'  => 15,
) );
/*Paragraph font Size*/
$wp_customize->add_setting( 'fairy_options[fairy-font-paragraph-font-size]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-font-paragraph-font-size'],
    'sanitize_callback' => 'fairy_sanitize_number_range'
) );
$wp_customize->add_control( 'fairy_options[fairy-font-paragraph-font-size]', array(
    'label'     => __( 'Paragraph Font Size', 'fairy' ),
    'description' => __('Size apply only for paragraphs, not headings. Font size between 12px to 20px.', 'fairy'),
    'section'   => 'fairy_font_options',
    'settings'  => 'fairy_options[fairy-font-paragraph-font-size]',
    'type'      => 'number',
    'priority'  => 15,
    'input_attrs' => array(
     'min' => 12,
     'max' => 20,
     'step' => 1,
 ),
) );
/*Paragraph Line height*/
$wp_customize->add_setting('fairy_options[fairy-font-line-height]', array(
    'default'     => $default['fairy-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-font-line-height]', array(
    'label' => __('Line Height', 'fairy'),
    'section' => 'fairy_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 15,
));
/*Paragraph letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-letter-spacing]', array(
    'default' => $default['fairy-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-letter-spacing]', array(
    'label'   => __('Letter Space', 'fairy'),
    'section' => 'fairy_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-20',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 15,
));

/*
* Menu Font Options
* Menu Font Option Section
* Fairy 1.0.0
*/

/*Menu Font Options*/
$wp_customize->add_section( 'fairy_menu_font_options', array(
    'priority'       => 20,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Menu Font', 'fairy' ),
    'panel' 		 => 'fairy_typography',
) );
/* Menu Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-menu-font-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-menu-font-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-menu-font-family-url]', array(
    'label'     => __( 'Select Font Family', 'fairy' ),
    'description' => __( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'  	=> $choices,
    'section'   => 'fairy_menu_font_options',
    'settings'  => 'fairy_options[fairy-menu-font-family-url]',
    'type'      => 'select',
    'priority'  => 15,
) );
/* Menu Paragraph font Size*/
$wp_customize->add_setting( 'fairy_options[fairy-menu-font-size]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-menu-font-size'],
    'sanitize_callback' => 'fairy_sanitize_number_range'
) );
$wp_customize->add_control( 'fairy_options[fairy-menu-font-size]', array(
    'label'     => __( 'Menu Font Size', 'fairy' ),
    'description' => __('Size apply only for paragraphs, not headings. Font size between 12px to 20px.', 'fairy'),
    'section'   => 'fairy_menu_font_options',
    'settings'  => 'fairy_options[fairy-menu-font-size]',
    'type'      => 'number',
    'priority'  => 15,
    'input_attrs' => array(
        'min' => 12,
        'max' => 20,
        'step' => 1,
    ),
) );
/*Menu Paragraph Line height*/
$wp_customize->add_setting('fairy_options[fairy-menu-font-line-height]', array(
    'default'     => $default['fairy-menu-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-menu-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-menu-font-line-height]', array(
    'label' => __('Line Height', 'fairy'),
    'section' => 'fairy_menu_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 15,
));
/*Menu Paragraph letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-menu-letter-spacing]', array(
    'default' => $default['fairy-menu-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-menu-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-menu-letter-spacing]', array(
    'label'   => __('Letter Spacing', 'fairy'),
    'section' => 'fairy_menu_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-20',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 15,
));

/*
* Widget Title Font Options
* Widget Title Font Option Section
* Fairy 1.0.0
*/
/*Widget Title Font Options*/
$wp_customize->add_section( 'fairy_widget_font_options', array(
    'priority'       => 20,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Widget Title Font', 'fairy' ),
    'panel'          => 'fairy_typography',
) );
/* Widget title Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-widget-font-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-widget-font-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-widget-font-family-url]', array(
    'label'     => __( 'Select Font Family', 'fairy' ),
    'description' => __( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'  	=> $choices,
    'section'   => 'fairy_widget_font_options',
    'settings'  => 'fairy_options[fairy-widget-font-family-url]',
    'type'      => 'select',
    'priority'  => 15,
) );
/* Widget title Paragraph font Size*/
$wp_customize->add_setting( 'fairy_options[fairy-widget-font-size]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-widget-font-size'],
    'sanitize_callback' => 'fairy_sanitize_number_range'
) );
$wp_customize->add_control( 'fairy_options[fairy-widget-font-size]', array(
    'label'     => __( 'Widget Font Size', 'fairy' ),
    'description' => __('Size apply only for paragraphs, not headings. Font size between 12px to 20px.', 'fairy'),
    'section'   => 'fairy_widget_font_options',
    'settings'  => 'fairy_options[fairy-widget-font-size]',
    'type'      => 'number',
    'priority'  => 15,
    'input_attrs' => array(
        'min' => 12,
        'max' => 20,
        'step' => 1,
    ),
) );
/*Widget title Paragraph Line height*/
$wp_customize->add_setting('fairy_options[fairy-widget-font-line-height]', array(
    'default'     => $default['fairy-widget-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-widget-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-widget-font-line-height]', array(
    'label' => __('Line Height', 'fairy'),
    'section' => 'fairy_widget_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 15,
));
/*Widget title Paragraph letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-widget-letter-spacing]', array(
    'default' => $default['fairy-widget-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-widget-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-widget-letter-spacing]', array(
    'label'   => __('Letter Space', 'fairy'),
    'section' => 'fairy_widget_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-20',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 15,
));



/*
* Post Title Font Options
* Post Title Font Option Section
* Fairy 1.0.0
*/
/*Widget Title Font Options*/
$wp_customize->add_section( 'fairy_post_font_options', array(
    'priority'       => 20,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Post Title Font', 'fairy' ),
    'panel'          => 'fairy_typography',
) );
/* Widget title Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-post-font-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-post-font-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-post-font-family-url]', array(
    'label'     => __( 'Select Font Family', 'fairy' ),
    'description' => __( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'  	=> $choices,
    'section'   => 'fairy_post_font_options',
    'settings'  => 'fairy_options[fairy-post-font-family-url]',
    'type'      => 'select',
    'priority'  => 15,
) );
/* Widget title Paragraph font Size*/
$wp_customize->add_setting( 'fairy_options[fairy-post-font-size]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-post-font-size'],
    'sanitize_callback' => 'fairy_sanitize_number_range'
) );
$wp_customize->add_control( 'fairy_options[fairy-post-font-size]', array(
    'label'     => __( 'Post Title Font Size', 'fairy' ),
    'description' => __('Size apply only for post titles. Font size between 20px to 40px.', 'fairy'),
    'section'   => 'fairy_post_font_options',
    'settings'  => 'fairy_options[fairy-post-font-size]',
    'type'      => 'number',
    'priority'  => 15,
    'input_attrs' => array(
        'min' => 20,
        'max' => 40,
        'step' => 1,
    ),
) );
/*Widget title Paragraph Line height*/
$wp_customize->add_setting('fairy_options[fairy-post-font-line-height]', array(
    'default'     => $default['fairy-post-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-post-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-post-font-line-height]', array(
    'label' => __('Line Height', 'fairy'),
    'section' => 'fairy_post_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 15,
));
/*Widget title Paragraph letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-post-letter-spacing]', array(
    'default' => $default['fairy-post-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-post-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-post-letter-spacing]', array(
    'label'   => __('Letter Space', 'fairy'),
    'section' => 'fairy_post_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-20',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 15,
));

/*
* Heading H1 Fonts Options
* Heading H1 Font Option Section
* Fairy 1.0.0
*/

/*Heading H1 Fonts*/
$wp_customize->add_section( 'fairy_h1_font_options', array(
    'priority'       => 30,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Heading H1 Font', 'fairy' ),
    'panel'         => 'fairy_typography',
) );
/*Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-font-heading-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-font-heading-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-font-heading-family-url]', array(
    'label'     => __( 'Select H1 Font Family', 'fairy' ),
    'description' => __( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'  	=> $choices,
    'section'   => 'fairy_h1_font_options',
    'settings'  => 'fairy_options[fairy-font-heading-family-url]',
    'type'      => 'select',
    'priority'  => 10,
) );
/*Heading Font Size height*/
$wp_customize->add_setting('fairy_options[fairy-h1-font-size]', array(
    'default'     => $default['fairy-h1-font-size'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range',
    'settings'=>'fairy_options[fairy-h1-font-size]'
));

$wp_customize->add_control('fairy_options[fairy-h1-font-size]', array(
    'label' => __('H1 Font Size', 'fairy'),
    'section' => 'fairy_h1_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Font Size.', 'fairy' ),
    'input_attrs' => array(
        'min' => '20',
        'max' => '50',
        'step' => '1',
    ),
    'priority'  => 10,
));
/*Heading Line height*/
$wp_customize->add_setting('fairy_options[fairy-h1-font-line-height]', array(
    'default'     => $default['fairy-h1-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-h1-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-h1-font-line-height]', array(
    'label' => __('H1 Line Height', 'fairy'),
    'section' => 'fairy_h1_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 10,
));
/*Heading letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-h1-letter-spacing]', array(
    'default' => $default['fairy-h1-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-h1-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-h1-letter-spacing]', array(
    'label'   => __('H1 Letter Space', 'fairy'),
    'section' => 'fairy_h1_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-20',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 10,
));

/*
* Heading H2 Fonts Options
* Heading H2 Font Option Section
* Fairy 1.0.0
*/

/*Heading H2 Fonts*/
$wp_customize->add_section( 'fairy_h2_font_options', array(
    'priority'       => 30,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Heading H2 Font', 'fairy' ),
    'panel'         => 'fairy_typography',
) );
/*Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-font-h2-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-font-h2-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-font-h2-family-url]', array(
    'label'     => __( 'Select H2 Font Family', 'fairy' ),
    'description' => __( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'  	=> $choices,
    'section'   => 'fairy_h2_font_options',
    'settings'  => 'fairy_options[fairy-font-h2-family-url]',
    'type'      => 'select',
    'priority'  => 10,
) );

/*Heading Font Size */
$wp_customize->add_setting('fairy_options[fairy-h2-font-size]', array(
    'default'     => $default['fairy-h2-font-size'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range',
    'settings'=>'fairy_options[fairy-h2-font-size]'
));

$wp_customize->add_control('fairy_options[fairy-h2-font-size]', array(
    'label' => __('H2 Font Size', 'fairy'),
    'section' => 'fairy_h2_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Font Size.', 'fairy' ),
    'input_attrs' => array(
        'min' => '20',
        'max' => '45',
        'step' => '1',
    ),
    'priority'  => 10,
));
/*Heading Line height*/
$wp_customize->add_setting('fairy_options[fairy-h2-font-line-height]', array(
    'default'     => $default['fairy-h2-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-h2-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-h2-font-line-height]', array(
    'label' => __('H2 Line Height', 'fairy'),
    'section' => 'fairy_h2_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 10,
));
/*Heading letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-h2-letter-spacing]', array(
    'default' => $default['fairy-h2-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-h2-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-h2-letter-spacing]', array(
    'label'   => __('H2 Letter Space', 'fairy'),
    'section' => 'fairy_h2_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-20',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 10,
));


/*
* Heading H3 Fonts Options
* Heading H3 Font Option Section
* Fairy 1.0.0
*/
/*Heading H3 Fonts*/
$wp_customize->add_section( 'fairy_h3_font_options', array(
    'priority'       => 30,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Heading H3 Font', 'fairy' ),
    'panel'         => 'fairy_typography',
) );
/*Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-font-h3-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-font-h3-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-font-h3-family-url]', array(
    'label'     => __( 'Select H3 Font Family', 'fairy' ),
    'description' => __( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'  	=> $choices,
    'section'   => 'fairy_h3_font_options',
    'settings'  => 'fairy_options[fairy-font-h3-family-url]',
    'type'      => 'select',
    'priority'  => 10,
) );
/*Heading Font Size*/
$wp_customize->add_setting('fairy_options[fairy-h3-font-size]', array(
    'default'     => $default['fairy-h3-font-size'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range',
    'settings'=>'fairy_options[fairy-h3-font-size]'
));

$wp_customize->add_control('fairy_options[fairy-h3-font-size]', array(
    'label' => __('H3 Font Size', 'fairy'),
    'section' => 'fairy_h3_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Font Size.', 'fairy' ),
    'input_attrs' => array(
        'min' => '16',
        'max' => '40',
        'step' => '1',
    ),
    'priority'  => 10,
));
/*Heading Line height*/
$wp_customize->add_setting('fairy_options[fairy-h3-font-line-height]', array(
    'default'     => $default['fairy-h3-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-h3-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-h3-font-line-height]', array(
    'label' => __('H3 Line Height', 'fairy'),
    'section' => 'fairy_h3_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 10,
));
/*Heading letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-h3-letter-spacing]', array(
    'default' => $default['fairy-h3-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-h3-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-h3-letter-spacing]', array(
    'label'   => __('H3 Letter Space', 'fairy'),
    'section' => 'fairy_h3_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-20',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 10,
));


/*
* Heading H4 Fonts Options
* Heading H4 Font Option Section
* Fairy 1.0.0
*/
/*Heading H4 Fonts*/
$wp_customize->add_section( 'fairy_h4_font_options', array(
    'priority'       => 30,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Heading H4 Font', 'fairy' ),
    'panel'         => 'fairy_typography',
) );
/*Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-font-h4-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-font-h4-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-font-h4-family-url]', array(
    'label'     => __( 'Select H4 Font Family', 'fairy' ),
    'description' => __( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'  	=> $choices,
    'section'   => 'fairy_h4_font_options',
    'settings'  => 'fairy_options[fairy-font-h4-family-url]',
    'type'      => 'select',
    'priority'  => 10,
) );
/*Heading Font Size */
$wp_customize->add_setting('fairy_options[fairy-h4-font-size]', array(
    'default'     => $default['fairy-h4-font-size'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range',
    'settings'=>'fairy_options[fairy-h4-font-size]'
));

$wp_customize->add_control('fairy_options[fairy-h4-font-size]', array(
    'label' => __('H4 Font Size', 'fairy'),
    'section' => 'fairy_h4_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Font Size.', 'fairy' ),
    'input_attrs' => array(
        'min' => '16',
        'max' => '35',
        'step' => '1',
    ),
    'priority'  => 10,
));
/*Heading Line height*/
$wp_customize->add_setting('fairy_options[fairy-h4-font-line-height]', array(
    'default'     => $default['fairy-h4-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-h4-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-h4-font-line-height]', array(
    'label' => __('H4 Line Height', 'fairy'),
    'section' => 'fairy_h4_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 10,
));
/*Heading letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-h4-letter-spacing]', array(
    'default' => $default['fairy-h4-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-h4-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-h4-letter-spacing]', array(
    'label'   => __('H4 Letter Space', 'fairy'),
    'section' => 'fairy_h4_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-20',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 10,
));

/*
* Heading H5 Fonts Options
* Heading H5 Font Option Section
* Fairy 1.0.0
*/
/*Heading H5 Fonts*/
$wp_customize->add_section( 'fairy_h5_font_options', array(
    'priority'       => 30,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Heading H5 Font', 'fairy' ),
    'panel'         => 'fairy_typography',
) );
/*Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-font-h5-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-font-h5-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-font-h5-family-url]', array(
    'label'     => __( 'Select H5 Font Family', 'fairy' ),
    'description' => __( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'  	=> $choices,
    'section'   => 'fairy_h5_font_options',
    'settings'  => 'fairy_options[fairy-font-h5-family-url]',
    'type'      => 'select',
    'priority'  => 10,
) );
/*Heading Font Size */
$wp_customize->add_setting('fairy_options[fairy-h5-font-size]', array(
    'default'     => $default['fairy-h5-font-size'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range',
    'settings'=>'fairy_options[fairy-h5-font-size]'
));

$wp_customize->add_control('fairy_options[fairy-h5-font-size]', array(
    'label' => __('H5 Font Size', 'fairy'),
    'section' => 'fairy_h5_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Font Size.', 'fairy' ),
    'input_attrs' => array(
        'min' => '14',
        'max' => '30',
        'step' => '1',
    ),
    'priority'  => 10,
));
/*Heading Line height*/
$wp_customize->add_setting('fairy_options[fairy-h5-font-line-height]', array(
    'default'     => $default['fairy-h5-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-h5-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-h5-font-line-height]', array(
    'label' => __('H5 Line Height', 'fairy'),
    'section' => 'fairy_h5_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 10,
));
/*Heading letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-h5-letter-spacing]', array(
    'default' => $default['fairy-h5-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-h5-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-h5-letter-spacing]', array(
    'label'   => __('H5 Letter Space', 'fairy'),
    'section' => 'fairy_h5_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-20',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 10,
));

/*
* Heading H6 Fonts Options
* Heading H6 Font Option Section
* Fairy 1.0.0
*/
/*Heading H6 Fonts*/
$wp_customize->add_section( 'fairy_h6_font_options', array(
    'priority'       => 30,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Heading H6 Font', 'fairy' ),
    'panel'         => 'fairy_typography',
) );
/*Font Family URL*/
$wp_customize->add_setting( 'fairy_options[fairy-font-h6-family-url]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-font-h6-family-url'],
    'sanitize_callback' => 'wp_kses_post'
) );
$choices = fairy_google_fonts();
$wp_customize->add_control( 'fairy_options[fairy-font-h6-family-url]', array(
    'label'     => __( 'Select Font Family', 'fairy' ),
    'description' => __( 'Select the required font from the dropdown.', 'fairy' ),
    'choices'  	=> $choices,
    'section'   => 'fairy_h6_font_options',
    'settings'  => 'fairy_options[fairy-font-h6-family-url]',
    'type'      => 'select',
    'priority'  => 10,
) );
/*Heading Font Size*/
$wp_customize->add_setting('fairy_options[fairy-h6-font-size]', array(
    'default'     => $default['fairy-h6-font-size'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range',
    'settings'=>'fairy_options[fairy-h6-font-size]'
));

$wp_customize->add_control('fairy_options[fairy-h6-font-size]', array(
    'label' => __('H6 Font Size', 'fairy'),
    'section' => 'fairy_h6_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Font Size.', 'fairy' ),
    'input_attrs' => array(
        'min' => '10',
        'max' => '24',
        'step' => '1',
    ),
    'priority'  => 10,
));
/*Heading Line height*/
$wp_customize->add_setting('fairy_options[fairy-h6-font-line-height]', array(
    'default'     => $default['fairy-h6-font-line-height'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number_range_decimal',
    'settings'=>'fairy_options[fairy-h6-font-line-height]'
));

$wp_customize->add_control('fairy_options[fairy-h6-font-line-height]', array(
    'label' => __('H6 Line Height', 'fairy'),
    'section' => 'fairy_h6_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Line Height.', 'fairy' ),
    'input_attrs' => array(
        'min' => '0',
        'max' => '4',
        'step' => '0.1',
    ),
    'priority'  => 10,
));
/*Heading letter spacing*/
$wp_customize->add_setting('fairy_options[fairy-h6-letter-spacing]', array(
    'default' => $default['fairy-h6-letter-spacing'],
    'transport'   => 'refresh',
    'sanitize_callback' => 'fairy_sanitize_number',
    'settings'=>'fairy_options[fairy-h6-letter-spacing]',
));

$wp_customize->add_control('fairy_options[fairy-h6-letter-spacing]', array(
    'label'   => __('H6 Letter Space', 'fairy'),
    'section' => 'fairy_h6_font_options',
    'type'    => 'number',
    'description' => __( 'Increase/Decrease Letter Space.', 'fairy' ),
    'input_attrs' => array(
        'min' => '-20',
        'max' => '4',
        'step' => '1',
    ),
    'priority'  => 10,
));