<?php
/**
 *  Fairy Boxed Option
 *
 * @since Fairy 1.0.0
 *
 */
/* Header Types Options*/
$wp_customize->add_section( 'fairy_category_boxes_section', array(
    'priority'       => 25,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Category Boxes Options', 'fairy' ),
    'panel' 		 => 'fairy_panel',
) );
/*Enable Boxes*/
$wp_customize->add_setting( 'fairy_options[fairy-enable-category-boxes]', array(
 'capability'        => 'edit_theme_options',
 'transport' => 'refresh',
 'default'           => $default['fairy-enable-category-boxes'],
 'sanitize_callback' => 'fairy_sanitize_checkbox'
) );
$wp_customize->add_control( 'fairy_options[fairy-enable-category-boxes]', array(
 'label'     => __( 'Enable Category Boxes Section', 'fairy' ),
 'description' => __('Checked to show category boxes section in Home Page.', 'fairy'),
 'section'   => 'fairy_category_boxes_section',
 'settings'  => 'fairy_options[fairy-enable-category-boxes]',
 'type'      => 'checkbox',
 'priority'  => 10,
) );

/*callback functions header section*/
if ( !function_exists('fairy_category_enable_boxes_callback') ) :
  function fairy_category_enable_boxes_callback(){
      global $fairy_theme_options;
      $fairy_theme_options = fairy_get_options_value();
      $enable_box = absint($fairy_theme_options['fairy-enable-category-boxes']);
      if( true == $enable_box ){
          return true;
      }
      else{
          return false;
      }
  }
endif;


/*Enable Boxes*/
$wp_customize->add_setting( 'fairy_options[fairy-boxes-selection-method]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-boxes-selection-method'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-boxes-selection-method]', array(
    'choices' => array(
        'multiple-cat'    => __('Three Category With Custom Image','fairy'),
        'multiple-cat-posts'       => __('Three Category With Latest Posts','fairy'),
        'single-cat-post'       => __('One Category and three posts','fairy')
    ),
    'label'     => __( 'Select Category Boxes from here', 'fairy' ),
    'description' => __('You can select the boxes from two methods. ', 'fairy'),
    'section'   => 'fairy_category_boxes_section',
    'settings'  => 'fairy_options[fairy-boxes-selection-method]',
    'type'      => 'select',
    'priority'  => 10,
    'active_callback'=> 'fairy_category_enable_boxes_callback', 
) );


/*callback functions header section*/
if ( !function_exists('fairy_category_boxes_callback') ) :
  function fairy_category_boxes_callback(){
      global $fairy_theme_options;
      $fairy_theme_options = fairy_get_options_value();
      $cat_box = absint($fairy_theme_options['fairy-enable-category-boxes']);
      $cat_method = esc_attr($fairy_theme_options['fairy-boxes-selection-method']);
      if( true == $cat_box && 'multiple-cat' == $cat_method){
          return true;
      }
      else{
          return false;
      }
  }
endif;


/*Boxes Category 1 Selection*/
$wp_customize->add_setting( 'fairy_options[fairy-multiple-cat-select-1]', array(
  'capability'        => 'edit_theme_options',
  'transport' => 'refresh',
  'default'           => $default['fairy-multiple-cat-select-1'],
  'sanitize_callback' => 'absint'
) );
$wp_customize->add_control(
  new Fairy_Customize_Category_Dropdown_Control(
    $wp_customize,
    'fairy_options[fairy-multiple-cat-select-1]',
    array(
      'label'     => __( 'Select Category For First Item', 'fairy' ),
      'description' => __('This will be the first item to be displayed.', 'fairy'),
      'section'   => 'fairy_category_boxes_section',
      'settings'  => 'fairy_options[fairy-multiple-cat-select-1]',
      'type'      => 'category_dropdown',
      'priority'  => 10,
      'active_callback'=>'fairy_category_boxes_callback'
    )
  )
);

/*Image for category one*/
$wp_customize->add_setting( 'fairy_options[fairy-multiple-image-select-1]', array(
    'capability'    => 'edit_theme_options',
    'default'     => $default['fairy-multiple-image-select-1'],
    'sanitize_callback' => 'fairy_sanitize_image'
) );
$wp_customize->add_control(
    new WP_Customize_Image_Control(
        $wp_customize,
        'fairy_options[fairy-multiple-image-select-1]',
        array(
            'label'   => __( 'Select the image for category first.', 'fairy' ),
            'section'   => 'fairy_category_boxes_section',
            'settings'  => 'fairy_options[fairy-multiple-image-select-1]',
            'type'      => 'image',
            'priority'  => 10,
            'active_callback' => 'fairy_category_boxes_callback',
            'description' => __( 'Recommended image size is 600*400', 'fairy' )
        )
    )
);

/*Boxes Category 2 Selection*/
$wp_customize->add_setting( 'fairy_options[fairy-multiple-cat-select-2]', array(
  'capability'        => 'edit_theme_options',
  'transport' => 'refresh',
  'default'           => $default['fairy-multiple-cat-select-2'],
  'sanitize_callback' => 'absint'
) );
$wp_customize->add_control(
  new Fairy_Customize_Category_Dropdown_Control(
    $wp_customize,
    'fairy_options[fairy-multiple-cat-select-2]',
    array(
      'label'     => __( 'Select Category For Second Item', 'fairy' ),
      'description' => __('This will be the second item to be displayed.', 'fairy'),
      'section'   => 'fairy_category_boxes_section',
      'settings'  => 'fairy_options[fairy-multiple-cat-select-2]',
      'type'      => 'category_dropdown',
      'priority'  => 10,
      'active_callback'=>'fairy_category_boxes_callback'
    )
  )
);

/*Image for category two*/
$wp_customize->add_setting( 'fairy_options[fairy-multiple-image-select-2]', array(
    'capability'    => 'edit_theme_options',
    'default'     => $default['fairy-multiple-image-select-2'],
    'sanitize_callback' => 'fairy_sanitize_image'
) );
$wp_customize->add_control(
    new WP_Customize_Image_Control(
        $wp_customize,
        'fairy_options[fairy-multiple-image-select-2]',
        array(
            'label'   => __( 'Select the image for second category.', 'fairy' ),
            'section'   => 'fairy_category_boxes_section',
            'settings'  => 'fairy_options[fairy-multiple-image-select-2]',
            'type'      => 'image',
            'priority'  => 10,
            'active_callback' => 'fairy_category_boxes_callback',
            'description' => __( 'Recommended image size is 600*400', 'fairy' )
        )
    )
);

/*Boxes Category 3 Selection*/
$wp_customize->add_setting( 'fairy_options[fairy-multiple-cat-select-3]', array(
  'capability'        => 'edit_theme_options',
  'transport' => 'refresh',
  'default'           => $default['fairy-multiple-cat-select-3'],
  'sanitize_callback' => 'absint'
) );
$wp_customize->add_control(
  new Fairy_Customize_Category_Dropdown_Control(
    $wp_customize,
    'fairy_options[fairy-multiple-cat-select-3]',
    array(
      'label'     => __( 'Select Category For Third Item', 'fairy' ),
      'description' => __('This will be the third item to be displayed.', 'fairy'),
      'section'   => 'fairy_category_boxes_section',
      'settings'  => 'fairy_options[fairy-multiple-cat-select-3]',
      'type'      => 'category_dropdown',
      'priority'  => 10,
      'active_callback'=>'fairy_category_boxes_callback'
    )
  )
);

/*Image for category three*/
$wp_customize->add_setting( 'fairy_options[fairy-multiple-image-select-3]', array(
    'capability'    => 'edit_theme_options',
    'default'     => $default['fairy-multiple-image-select-3'],
    'sanitize_callback' => 'fairy_sanitize_image'
) );
$wp_customize->add_control(
    new WP_Customize_Image_Control(
        $wp_customize,
        'fairy_options[fairy-multiple-image-select-3]',
        array(
            'label'   => __( 'Select the image for category third.', 'fairy' ),
            'section'   => 'fairy_category_boxes_section',
            'settings'  => 'fairy_options[fairy-multiple-image-select-3]',
            'type'      => 'image',
            'priority'  => 10,
            'active_callback' => 'fairy_category_boxes_callback',
            'description' => __( 'Recommended image size is 600*400', 'fairy' )
        )
    )
);

/*Three Category With Latest Posts*/

/*callback functions header section*/
if ( !function_exists('fairy_multiple_category_boxes_callback') ) :
  function fairy_multiple_category_boxes_callback(){
      global $fairy_theme_options;
      $fairy_theme_options = fairy_get_options_value();
      $cat_box = absint($fairy_theme_options['fairy-enable-category-boxes']);
      $cat_method = esc_attr($fairy_theme_options['fairy-boxes-selection-method']);
      if( true == $cat_box && 'multiple-cat-posts' == $cat_method){
          return true;
      }
      else{
          return false;
      }
  }
endif;

/*Boxes Category 1 Selection*/
$wp_customize->add_setting( 'fairy_options[fairy-multiple-cat-posts-select-1]', array(
  'capability'        => 'edit_theme_options',
  'transport' => 'refresh',
  'default'           => $default['fairy-multiple-cat-posts-select-1'],
  'sanitize_callback' => 'absint'
) );
$wp_customize->add_control(
  new Fairy_Customize_Category_Dropdown_Control(
    $wp_customize,
    'fairy_options[fairy-multiple-cat-posts-select-1]',
    array(
      'label'     => __( 'Select Category For First Item', 'fairy' ),
      'description' => __('This will be the first item to be displayed.', 'fairy'),
      'section'   => 'fairy_category_boxes_section',
      'settings'  => 'fairy_options[fairy-multiple-cat-posts-select-1]',
      'type'      => 'category_dropdown',
      'priority'  => 10,
      'active_callback'=>'fairy_multiple_category_boxes_callback'
    )
  )
);

/*Boxes Category 2 Selection*/
$wp_customize->add_setting( 'fairy_options[fairy-multiple-cat-posts-select-2]', array(
  'capability'        => 'edit_theme_options',
  'transport' => 'refresh',
  'default'           => $default['fairy-multiple-cat-posts-select-2'],
  'sanitize_callback' => 'absint'
) );
$wp_customize->add_control(
  new Fairy_Customize_Category_Dropdown_Control(
    $wp_customize,
    'fairy_options[fairy-multiple-cat-posts-select-2]',
    array(
      'label'     => __( 'Select Category For Second Item', 'fairy' ),
      'description' => __('This will be the second item to be displayed.', 'fairy'),
      'section'   => 'fairy_category_boxes_section',
      'settings'  => 'fairy_options[fairy-multiple-cat-posts-select-2]',
      'type'      => 'category_dropdown',
      'priority'  => 10,
      'active_callback'=>'fairy_multiple_category_boxes_callback'
    )
  )
);

/*Boxes Category 3 Selection*/
$wp_customize->add_setting( 'fairy_options[fairy-multiple-cat-posts-select-3]', array(
  'capability'        => 'edit_theme_options',
  'transport' => 'refresh',
  'default'           => $default['fairy-multiple-cat-posts-select-3'],
  'sanitize_callback' => 'absint'
) );
$wp_customize->add_control(
  new Fairy_Customize_Category_Dropdown_Control(
    $wp_customize,
    'fairy_options[fairy-multiple-cat-posts-select-3]',
    array(
      'label'     => __( 'Select Category For Thid Item', 'fairy' ),
      'description' => __('This will be the third item to be displayed.', 'fairy'),
      'section'   => 'fairy_category_boxes_section',
      'settings'  => 'fairy_options[fairy-multiple-cat-posts-select-3]',
      'type'      => 'category_dropdown',
      'priority'  => 10,
      'active_callback'=>'fairy_multiple_category_boxes_callback'
    )
  )
);

/*One Category With 3 Latest Posts*/
/*callback functions header section*/
if ( !function_exists('fairy_single_category_boxes_callback') ) :
  function fairy_single_category_boxes_callback(){
      global $fairy_theme_options;
      $fairy_theme_options = fairy_get_options_value();
      $cat_box = absint($fairy_theme_options['fairy-enable-category-boxes']);
      $cat_method = esc_attr($fairy_theme_options['fairy-boxes-selection-method']);
      if( true == $cat_box && 'single-cat-post' == $cat_method){
          return true;
      }
      else{
          return false;
      }
  }
endif;

/*Boxes Category*/
$wp_customize->add_setting( 'fairy_options[fairy-single-cat-posts-select-1]', array(
  'capability'        => 'edit_theme_options',
  'transport' => 'refresh',
  'default'           => $default['fairy-single-cat-posts-select-1'],
  'sanitize_callback' => 'absint'
) );
$wp_customize->add_control(
  new Fairy_Customize_Category_Dropdown_Control(
    $wp_customize,
    'fairy_options[fairy-single-cat-posts-select-1]',
    array(
      'label'     => __( 'Select Category', 'fairy' ),
      'description' => __('Three Posts from the same category will appear.', 'fairy'),
      'section'   => 'fairy_category_boxes_section',
      'settings'  => 'fairy_options[fairy-single-cat-posts-select-1]',
      'type'      => 'category_dropdown',
      'priority'  => 10,
      'active_callback'=>'fairy_single_category_boxes_callback'
    )
  )
);