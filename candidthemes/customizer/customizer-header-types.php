<?php
/**
 *  Fairy Header Types Option
 *
 * @since Fairy 1.0.0
 *
 */
/* Header Types Options*/
$wp_customize->add_section( 'fairy_header_types_section', array(
    'priority'       => 10,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => __( 'Header Types Options', 'fairy' ),
    'panel' 		 => 'fairy_panel',
) );
/*Types of Header*/
$wp_customize->add_setting( 'fairy_options[fairy-header-types]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-header-types'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-header-types]', array(
    'choices' => array(
        'default-header'    => __('Logo Above Menu','fairy'),
        'header-two'       => __('Menu Above Logo','fairy'),
        'header-one'       => __('Left Logo and Right Menu','fairy')
    ),
    'label'     => __( 'Select Preferred Header Layout', 'fairy' ),
    'description' => __('Select the required header design. Left logo and Right menu does not support ads. ', 'fairy'),
    'section'   => 'fairy_header_types_section',
    'settings'  => 'fairy_options[fairy-header-types]',
    'type'      => 'select',
    'priority'  => 10,
) );

/*callback functions header section*/
if ( !function_exists('fairy_ads_header_active_callback') ) :
  function fairy_ads_header_active_callback(){
      global $fairy_theme_options;
      $fairy_theme_options = fairy_get_options_value();
      $enable_ads_header = esc_attr($fairy_theme_options['fairy-header-types']);
      if( 'header-one' == $enable_ads_header ){
          return false;
      }
      else{
          return true;
      }
  }
endif;

/*Logo Position of Header*/
$wp_customize->add_setting( 'fairy_options[fairy-header-logo-position]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-header-logo-position'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-header-logo-position]', array(
    'choices' => array(
        'default-logo'    => __('Default(Center) Logo','fairy'),
        'left-logo'       => __('Left Logo','fairy')
    ),
    'label'     => __( 'Position of Logo in Header Layout', 'fairy' ),
    'description' => __('Select the position of logo either left or in center. ', 'fairy'),
    'section'   => 'fairy_header_types_section',
    'settings'  => 'fairy_options[fairy-header-logo-position]',
    'type'      => 'select',
    'active_callback' => 'fairy_ads_header_active_callback',
    'priority'  => 10,
) );

/*Menu Position of Header*/
$wp_customize->add_setting( 'fairy_options[fairy-header-menu-position]', array(
    'capability'        => 'edit_theme_options',
    'transport' => 'refresh',
    'default'           => $default['fairy-header-menu-position'],
    'sanitize_callback' => 'fairy_sanitize_select'
) );
$wp_customize->add_control( 'fairy_options[fairy-header-menu-position]', array(
    'choices' => array(
        'default-menu'    => __('Default(Center) Menu','fairy'),
        'left-menu'       => __('Left Menu','fairy')
    ),
    'label'     => __( 'Position of Menu in Header Layout', 'fairy' ),
    'description' => __('Select the position of logo either left or in center. ', 'fairy'),
    'section'   => 'fairy_header_types_section',
    'settings'  => 'fairy_options[fairy-header-menu-position]',
    'type'      => 'select',
    'active_callback' => 'fairy_ads_header_active_callback',
    'priority'  => 10,
) );

/*Header Ads Image*/
$wp_customize->add_setting( 'fairy_options[fairy-header-ads-image]', array(
    'capability'    => 'edit_theme_options',
    'default'     => $default['fairy-header-ads-image'],
    'sanitize_callback' => 'fairy_sanitize_image'
) );
$wp_customize->add_control(
    new WP_Customize_Image_Control(
        $wp_customize,
        'fairy_options[fairy-header-ads-image]',
        array(
            'label'   => __( 'Header Ad Image', 'fairy' ),
            'section'   => 'fairy_header_types_section',
            'settings'  => 'fairy_options[fairy-header-ads-image]',
            'type'      => 'image',
            'priority'  => 10,
            'active_callback' => 'fairy_ads_header_active_callback',
            'description' => __( 'Recommended image size of 728*90', 'fairy' )
        )
    )
);

/*Ads Image Link*/
$wp_customize->add_setting( 'fairy_options[fairy-header-ads-image-link]', array(
    'capability'    => 'edit_theme_options',
    'default'     => $default['fairy-header-ads-image-link'],
    'sanitize_callback' => 'esc_url_raw',
) );
$wp_customize->add_control( 'fairy_options[fairy-header-ads-image-link]', array(
    'label'   => __( 'Header Ads Image Link', 'fairy' ),
    'section'   => 'fairy_header_types_section',
    'settings'  => 'fairy_options[fairy-header-ads-image-link]',
    'type'      => 'url',
    'active_callback' => 'fairy_ads_header_active_callback',
    'priority'  => 10
) );