<?php
/**
 * Fairy Pro 
*/

/**
 * Add a new page under Appearance
 */
function fairy_menu() {
	add_theme_page( __( 'About Fairy Pro', 'fairy' ), __( 'About Fairy Pro', 'fairy' ), 'edit_theme_options', 'welcome-page', 'fairy_page' );
}
add_action( 'admin_menu', 'fairy_menu' );

/**
 * Enqueue styles for the help page.
 */
function fairy_admin_scripts( $hook ) {
	if ( 'appearance_page_fairy' !== $hook ) {
		return;
	}
	wp_enqueue_style( 'fairy-admin-style', get_template_directory_uri() . '/candidthemes/welcome/welcome.css', array(), '' );
}
add_action( 'admin_enqueue_scripts', 'fairy_admin_scripts' );

/**
 * Add the theme page
 */
function fairy_page() {
	?>
	<div class="candid-about-wrap">
		<div class="fairy-setting">
			<p>
			<?php esc_html_e( 'A clean and minimal WordPress premium magazine theme.', 'fairy' ); ?></p>
			<a class="button button-primary" href="<?php echo esc_url (admin_url( '/customize.php?' ));
				?>"><?php esc_html_e( 'Theme Options and Settings', 'fairy' ); ?></a>
		</div>

		<div class="fairy-panel">
			<div class="fairy-docs">
				<div class="theme-title">
					<h3><?php esc_html_e( 'Theme Documentation with Video', 'fairy' ); ?></h3>
				</div>
				<a href="https://docs.candidthemes.com/fairy-pro/" target="_blank" class="button button-secondary"><?php esc_html_e( 'Video Documentation', 'fairy' ); ?></a>
			</div>
		</div>
	</div>
	<?php
}
