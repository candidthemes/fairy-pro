<?php

if (!function_exists('fairy_construct_cat_section')) {
    /**
     * Display category section on homepage
     *
     * @since 1.0.0
     *
     */
    function fairy_construct_cat_section()
    {
        global $fairy_theme_options;
        if ((is_front_page()) && ($fairy_theme_options['fairy-enable-category-boxes'] == 1)) {
            $category_box_type = $fairy_theme_options['fairy-boxes-selection-method'];
            if ($category_box_type == 'multiple-cat-posts') {
                /**
                 * fairy_multiple_cat_posts hook.
                 *
                 * @since 1.0.0
                 *
                 * @hooked fairy_construct_multiple_cat_posts - 10
                 */
                do_action('fairy_multiple_cat_posts');
            } elseif ($category_box_type == 'single-cat-post') {
                /**
                 * fairy_single_cat_posts hook.
                 *
                 * @since 1.0.0
                 *
                 * @hooked fairy_constuct_single_cat_posts - 10
                 */
                do_action('fairy_single_cat_posts');
            } else {
                /**
                 * fairy_multiple_cats_box hook.
                 *
                 * @since 1.0.0
                 *
                 * @hooked fairy_construct_multiple_cats_box - 10
                 */
                do_action('fairy_multiple_cats_box');
            }

        }


    }
}
add_action('fairy_cat_section', 'fairy_construct_cat_section', 10);


if (!function_exists('fairy_construct_multiple_cat_posts')) {
    /**
     * Display latest posts boxes of 3 different categories
     *
     * @since 1.0.0
     *
     */
    function fairy_construct_multiple_cat_posts()
    {
        global $fairy_theme_options;
        $cat1 = absint($fairy_theme_options['fairy-multiple-cat-posts-select-1']);
        $cat2 = absint($fairy_theme_options['fairy-multiple-cat-posts-select-2']);
        $cat3 = absint($fairy_theme_options['fairy-multiple-cat-posts-select-3']);
        if ((!empty($cat1)) || (!empty($cat2)) || (!empty($cat3))) {
            ?>
            <section class="promo-section sec-spacing">
                <div class="container">
                    <div class="row">
                        <?php
                        if (!empty($cat1)) {
                            $fairy_cat_post_args = array(
                                'category__in' => $cat1,
                                'post_type' => 'post',
                                'posts_per_page' => 1,
                                'post_status' => 'publish',
                                'ignore_sticky_posts' => true
                            );
                            $fairy_featured_query = new WP_Query($fairy_cat_post_args);
                            if ($fairy_featured_query->have_posts()) :

                                while ($fairy_featured_query->have_posts()) : $fairy_featured_query->the_post();
                                    ?>
                                    <div class="col-1-1 col-sm-1-2 col-md-1-3">
                                        <div class="card card-bg-image card-promo">
                                            <figure class="card_media">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php
                                                    if (has_post_thumbnail()) {
                                                        the_post_thumbnail('fairy-medium');
                                                    } else {
                                                        ?>
                                                        <img src="<?php echo esc_url(get_template_directory_uri()) . '/candidthemes/assets/custom/img/fairy-medium.jpg' ?>" alt="<?php the_title(); ?>">
                                                        <?php
                                                    }
                                                    ?>
                                                </a>
                                            </figure>

                                            <article class="card_body">
                                                <h3 class="card_title">
                                                    <a href="<?php the_permalink(); ?>"> <?php echo esc_html(get_cat_name($cat1)); ?> </a>
                                                </h3>
                                            </article>
                                        </div>
                                    </div>
                                <?php
                                endwhile;
                            endif;
                            wp_reset_postdata();
                        }

                        if (!empty($cat2)) {
                            $fairy_cat_post_args = array(
                                'category__in' => $cat2,
                                'post_type' => 'post',
                                'posts_per_page' => 1,
                                'post_status' => 'publish',
                                'ignore_sticky_posts' => true
                            );
                            $fairy_featured_query = new WP_Query($fairy_cat_post_args);
                            if ($fairy_featured_query->have_posts()) :

                                while ($fairy_featured_query->have_posts()) : $fairy_featured_query->the_post();
                                    ?>
                                    <div class="col-1-1 col-sm-1-2 col-md-1-3">
                                        <div class="card card-bg-image card-promo">
                                            <figure class="card_media">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php
                                                    if (has_post_thumbnail()) {
                                                        the_post_thumbnail('fairy-medium');
                                                    } else {
                                                        ?>
                                                        <img src="<?php echo esc_url(get_template_directory_uri()) . '/candidthemes/assets/custom/img/fairy-medium.jpg' ?>" alt="<?php the_title(); ?>">
                                                        <?php
                                                    }
                                                    ?>
                                                </a>
                                            </figure>

                                            <article class="card_body">
                                                <h3 class="card_title">
                                                    <a href="<?php the_permalink(); ?>"> <?php echo esc_html(get_cat_name($cat2)); ?> </a>
                                                </h3>
                                            </article>
                                        </div>
                                    </div>
                                <?php
                                endwhile;
                            endif;
                            wp_reset_postdata();
                        }

                        if (!empty($cat3)) {
                            $fairy_cat_post_args = array(
                                'category__in' => $cat3,
                                'post_type' => 'post',
                                'posts_per_page' => 1,
                                'post_status' => 'publish',
                                'ignore_sticky_posts' => true
                            );
                            $fairy_featured_query = new WP_Query($fairy_cat_post_args);
                            if ($fairy_featured_query->have_posts()) :

                                while ($fairy_featured_query->have_posts()) : $fairy_featured_query->the_post();
                                    ?>
                                    <div class="col-1-1 col-sm-1-2 col-md-1-3">
                                        <div class="card card-bg-image card-promo">
                                            <figure class="card_media">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php
                                                    if (has_post_thumbnail()) {
                                                        the_post_thumbnail('fairy-medium');
                                                    } else {
                                                        ?>
                                                        <img src="<?php echo esc_url(get_template_directory_uri()) . '/candidthemes/assets/custom/img/fairy-medium.jpg' ?>" alt="<?php the_title(); ?>">
                                                        <?php
                                                    }
                                                    ?>
                                                </a>
                                            </figure>

                                            <article class="card_body">
                                                <h3 class="card_title">
                                                    <a href="<?php the_permalink(); ?>"> <?php echo esc_html(get_cat_name($cat3)); ?> </a>
                                                </h3>
                                            </article>
                                        </div>
                                    </div>
                                <?php
                                endwhile;
                            endif;
                            wp_reset_postdata();
                        }
                        ?>
                    </div>
                </div>
            </section>
            <?php
        }
    }
}
add_action('fairy_multiple_cat_posts', 'fairy_construct_multiple_cat_posts', 10);


if (!function_exists('fairy_constuct_single_cat_posts')) {
    /**
     * Display latest posts boxes of 3 different categories
     *
     * @since 1.0.0
     *
     */
    function fairy_constuct_single_cat_posts()
    {
        global $fairy_theme_options;
        $cat1 = absint($fairy_theme_options['fairy-single-cat-posts-select-1']);
        if (!empty($cat1)) {
            ?>
            <section class="promo-section sec-spacing">
                <div class="container">
                    <div class="row">
                        <?php
                        $fairy_cat_post_args = array(
                            'category__in' => $cat1,
                            'post_type' => 'post',
                            'posts_per_page' => 3,
                            'post_status' => 'publish',
                            'ignore_sticky_posts' => true
                        );
                        $fairy_featured_query = new WP_Query($fairy_cat_post_args);
                        if ($fairy_featured_query->have_posts()) :

                            while ($fairy_featured_query->have_posts()) : $fairy_featured_query->the_post();
                                ?>
                                <div class="col-1-1 col-sm-1-2 col-md-1-3">
                                    <div class="card card-bg-image card-promo">
                                        <figure class="card_media">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php
                                                if (has_post_thumbnail()) {
                                                    the_post_thumbnail('fairy-medium');
                                                } else {
                                                    ?>
                                                    <img src="<?php echo esc_url(get_template_directory_uri()) . '/candidthemes/assets/custom/img/fairy-medium.jpg' ?>" alt="<?php the_title(); ?>">
                                                    <?php
                                                }
                                                ?>
                                            </a>
                                        </figure>

                                        <article class="card_body">
                                            <h3 class="card_title">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php
                                                    $tags = get_the_tags();
                                                    if (!empty($tags)) {
                                                        $tag_name = $tags[0]->name;
                                                        echo esc_html($tag_name);
                                                    } else {
                                                        echo esc_html(get_cat_name($cat1));
                                                    }

                                                    ?>
                                                </a>
                                            </h3>
                                        </article>
                                    </div>
                                </div>
                            <?php
                            endwhile;
                        endif;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </section>
            <?php
        }
    }
}
add_action('fairy_single_cat_posts', 'fairy_constuct_single_cat_posts', 10);


if (!function_exists('fairy_construct_multiple_cats_box')) {
    /**
     * Display three categories boxes
     *
     * @since 1.0.0
     *
     */
    function fairy_construct_multiple_cats_box()
    {
        global $fairy_theme_options;
        $cat1 = absint($fairy_theme_options['fairy-multiple-cat-select-1']);
        $cat2 = absint($fairy_theme_options['fairy-multiple-cat-select-2']);
        $cat3 = absint($fairy_theme_options['fairy-multiple-cat-select-3']);
        if ((!empty($cat1)) || (!empty($cat2)) || (!empty($cat3))) {
            ?>
            <section class="promo-section sec-spacing">
                <div class="container">
                    <div class="row">
                        <?php
                        if (!empty($cat1)) {
                            $cat_name = get_cat_name($cat1);
                            $cat_url = get_category_link($cat1);
                            $cat_image = esc_url($fairy_theme_options['fairy-multiple-image-select-1']);
                            ?>
                            <div class="col-1-1 col-sm-1-2 col-md-1-3">
                                <div class="card card-bg-image card-promo">
                                    <figure class="card_media">
                                        <a href="<?php echo esc_url($cat_url); ?>">
                                            <?php
                                            if (!empty($cat_image)) {
                                                ?>
                                                <img src="<?php echo $cat_image; ?>"
                                                     alt="<?php esc_attr($cat_name); ?>">
                                                <?php
                                            } else {
                                                ?>
                                                <img src="<?php echo esc_url(get_template_directory_uri()) . '/candidthemes/assets/custom/img/fairy-medium.jpg' ?>" alt="<?php echo esc_html($cat_name); ?>">
                                                <?php
                                            }
                                            ?>
                                        </a>
                                    </figure>

                                    <article class="card_body">
                                        <h3 class="card_title">
                                            <a href="<?php echo esc_url($cat_url); ?>"> <?php echo esc_html($cat_name); ?> </a>
                                        </h3>
                                    </article>
                                </div>
                            </div>
                            <?php
                        }

                        if (!empty($cat2)) {
                            $cat_name = get_cat_name($cat2);
                            $cat_url = get_category_link($cat2);
                            $cat_image = esc_url($fairy_theme_options['fairy-multiple-image-select-2']);
                            ?>
                            <div class="col-1-1 col-sm-1-2 col-md-1-3">
                                <div class="card card-bg-image card-promo">
                                    <figure class="card_media">
                                        <a href="<?php echo esc_url($cat_url); ?>">
                                            <?php
                                            if (!empty($cat_image)) {
                                                ?>
                                                <img src="<?php echo $cat_image; ?>"
                                                     alt="<?php esc_attr($cat_name); ?>">
                                                <?php
                                            } else {
                                                ?>
                                                <img src="<?php echo esc_url(get_template_directory_uri()) . '/candidthemes/assets/custom/img/fairy-medium.jpg' ?>" alt="<?php echo esc_html($cat_name); ?>">
                                                <?php
                                            }
                                            ?>
                                        </a>
                                    </figure>

                                    <article class="card_body">
                                        <h3 class="card_title">
                                            <a href="<?php echo esc_url($cat_url); ?>"> <?php echo esc_html($cat_name); ?> </a>
                                        </h3>
                                    </article>
                                </div>
                            </div>
                            <?php
                        }

                        if (!empty($cat3)) {
                            $cat_name = get_cat_name($cat3);
                            $cat_url = get_category_link($cat3);
                            $cat_image = esc_url($fairy_theme_options['fairy-multiple-image-select-3']);
                            ?>
                            <div class="col-1-1 col-sm-1-2 col-md-1-3">
                                <div class="card card-bg-image card-promo">
                                    <figure class="card_media">
                                        <a href="<?php echo esc_url($cat_url); ?>">
                                            <?php
                                            if (!empty($cat_image)) {
                                                ?>
                                                <img src="<?php echo $cat_image; ?>"
                                                     alt="<?php esc_attr($cat_name); ?>">
                                                <?php
                                            } else {
                                                ?>
                                                <img src="<?php echo esc_url(get_template_directory_uri()) . '/candidthemes/assets/custom/img/fairy-medium.jpg' ?>" alt="<?php echo esc_html($cat_name); ?>">
                                                <?php
                                            }
                                            ?>
                                        </a>
                                    </figure>

                                    <article class="card_body">
                                        <h3 class="card_title">
                                            <a href="<?php echo esc_url($cat_url); ?>"> <?php echo esc_html($cat_name); ?> </a>
                                        </h3>
                                    </article>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </section>
            <?php
        }
    }
}
add_action('fairy_multiple_cats_box', 'fairy_construct_multiple_cats_box', 10);


if (!function_exists('fairy_posts_navigation')) {
    /**
     * Display pagination based on type seclected
     *
     * @since 1.0.0
     *
     */
    function fairy_posts_navigation()
    {
        global $fairy_theme_options;
        if ($fairy_theme_options['fairy-pagination-options'] == 'numeric') {
            the_posts_pagination();
        } elseif ($fairy_theme_options['fairy-pagination-options'] == 'hide') {
            return '';
        } elseif (($fairy_theme_options['fairy-pagination-options'] == 'ajax') || ($fairy_theme_options['fairy-pagination-options'] == 'infinite')) {
            $page_number = get_query_var('paged');
            if ($page_number == 0) {
                $output_page = 2;
            } else {
                $output_page = $page_number + 1;
            }
            if (paginate_links()) {
                echo "<div class='ajax-pagination text-center'><div class='show-more' data-number='$output_page'><i class='fa fa-refresh'></i>" . __('Load More', 'fairy') . "</div><div id='free-temp-post'></div></div>";
            }
        } else {
            the_posts_navigation();
        }

    }
}
add_action('fairy_action_navigation', 'fairy_posts_navigation', 10);


if (!function_exists('fairy_related_post')) :
    /**
     * Display related posts from same category
     *
     * @param int $post_id
     * @return void
     *
     * @since 1.0.0
     *
     */
    function fairy_related_post($post_id)
    {

        global $fairy_theme_options;
        if ($fairy_theme_options['fairy-single-page-related-posts'] == 0) {
            return;
        }
        $post_number = absint($fairy_theme_options['fairy-single-page-related-posts-number']);
        $related_by = esc_html($fairy_theme_options['fairy-single-page-related-selection-types']);
        if (empty($post_number)) {
            $post_number = 2;
        }
        $count = 0;
        if ($related_by == 'category') {
            $categories = get_the_category($post_id);
            if ($categories) {
                $category_ids = array();
                $category = get_category($category_ids);
                $categories = get_the_category($post_id);
                foreach ($categories as $category) {
                    $category_ids[] = $category->term_id;
                }
                $count = count($category_ids);

                $fairy_cat_post_args = array(
                    'category__in' => $category_ids,
                    'post__not_in' => array($post_id),
                    'post_type' => 'post',
                    'posts_per_page' => $post_number,
                    'post_status' => 'publish',
                    'ignore_sticky_posts' => true
                );

            }
        } else {
            $tags = get_the_tags($post_id);
            if ($tags) {
                $tags_ids = array();
                foreach ($tags as $tag) {
                    $tags_ids[] = $tag->term_id;
                }

                $count = count($tags_ids);
                $fairy_cat_post_args = array(
                    'tag__in' => $tags_ids,
                    'post__not_in' => array($post_id),
                    'post_type' => 'post',
                    'posts_per_page' => $post_number,
                    'post_status' => 'publish',
                    'ignore_sticky_posts' => true
                );

            }
        }


        if ($count >= 1) { ?>
            <div class="related-post">
                <?php
                $fairy_related_post_title = $fairy_theme_options['fairy-single-page-related-posts-title'];
                if (!empty($fairy_related_post_title)):
                    ?>
                    <h2 class="post-title"><?php echo $fairy_related_post_title; ?></h2>
                <?php
                endif;


                $fairy_featured_query = new WP_Query($fairy_cat_post_args);
                ?>
                <div class="row">
                    <?php
                    if ($fairy_featured_query->have_posts()) :

                    while ($fairy_featured_query->have_posts()) : $fairy_featured_query->the_post();
                        ?>
                        <div class="col-1-1 col-sm-1-2 col-md-1-2">
                            <div class="card card-blog-post card-full-width">
                                <?php
                                if (has_post_thumbnail() && ($fairy_theme_options['fairy-single-page-related-posts-image'] == 1)):
                                    ?>
                                    <figure class="card_media">
                                        <a href="<?php the_permalink() ?>">
                                            <?php the_post_thumbnail(); ?>
                                        </a>
                                    </figure>
                                <?php
                                endif;
                                ?>
                                <div class="card_body">
                                    <?php fairy_list_category(); ?>
                                    <h4 class="card_title">
                                        <a href="<?php the_permalink() ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h4>
                                    <div class="entry-meta">
                                        <?php
                                        if ($fairy_theme_options['fairy-single-page-related-posts-date'] == 1) {
                                            fairy_posted_on();
                                        }
                                        if ($fairy_theme_options['fairy-single-page-related-posts-author'] == 1) {
                                            fairy_posted_by();
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                    ?>
                </div>

            <?php
            endif;
            wp_reset_postdata();
            ?>
            </div> <!-- .related-post -->
            <?php
        }

    }
endif;
add_action('fairy_related_posts', 'fairy_related_post', 10, 1);


if (!function_exists('fairy_constuct_carousel')) {
    /**
     * Add carousel on header
     *
     * @since 1.0.0
     */
    function fairy_constuct_carousel()
    {

        if (is_front_page()) {
            global $fairy_theme_options;
            if ($fairy_theme_options['fairy-enable-slider'] != 1)
                return false;
            $featured_cat = $fairy_theme_options['fairy-select-category'];
            $fairy_enable_category = $fairy_theme_options['fairy-slider-post-category'];
            $fairy_enable_date = $fairy_theme_options['fairy-slider-post-date'];
            $fairy_enable_author = $fairy_theme_options['fairy-slider-post-author'];

            $fairy_slider_args = array();
            if(is_rtl()){
                $fairy_slider_args['rtl'] = true;
            }
            $fairy_slider_args_encoded = wp_json_encode( $fairy_slider_args );
            $query_args = array(
                'post_type' => 'post',
                'ignore_sticky_posts' => true,
                'posts_per_page' => 6,
                'cat' => $featured_cat
            );

            $query = new WP_Query($query_args);
            if ($query->have_posts()) :
                ?>
                <section class="hero hero-slider-section">
                    <div class="container">
                        <!-- slick slider component start -->
                        <?php
                        if ($fairy_theme_options['fairy-slider-types'] == 'carousel-slider') {
                            $carousel_class = 'hero-style-carousel';
                        } else {
                            $carousel_class = 'hero_slick-slider';
                        }
                        ?>
                        <!-- .hero-style-carousel for carousel instead of hero_slick-slider -->
                        <div class="<?php echo $carousel_class; ?>" data-slick='<?php echo $fairy_slider_args_encoded; ?>'>
                            <?php
                            $i = 1;
                            while ($query->have_posts()) :
                                $query->the_post();

                                ?>
                                <div class="card card-bg-image">
                                    <?php
                                    if (has_post_thumbnail()) {
                                        ?>
                                        <div class="post-thumb">
                                            <figure class="card_media">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php
                                                    $cropped_image = $fairy_theme_options['fairy-image-size-slider'];
                                                    if($cropped_image == 'cropped-image'){
                                                        the_post_thumbnail('fairy-large');
                                                    }else{
                                                        the_post_thumbnail();
                                                    }
                                                    ?>
                                                </a>
                                            </figure>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="post-thumb">
                                            <a href="<?php the_permalink(); ?>">

                                                <img src="<?php echo esc_url(get_template_directory_uri()) . '/candidthemes/assets/custom/img/fairy-default.jpg' ?>"
                                                     alt="<?php the_title(); ?>">

                                            </a>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <article class="card_body">
                                        <?php
                                        if ($fairy_enable_category) {
                                            fairy_list_category();
                                        }
                                        ?>

                                        <h3 class="card_title">
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </h3>

                                        <div class="entry-meta">
                                            <?php
                                            if ($fairy_enable_date) {
                                                fairy_posted_on();
                                            }
                                            if ($fairy_enable_author) {
                                                fairy_posted_by();
                                            }
                                            ?>
                                        </div>
                                    </article>

                                </div>
                                <?php
                                $i++;

                            endwhile;
                            ?>
                    </div>
                    </div>
                </section><!-- .hero -->
            <?php
            endif;
            wp_reset_postdata();


        }//is_front_page
    }
}
add_action('fairy_carousel', 'fairy_constuct_carousel', 10);


if (!function_exists('fairy_breadcrumb_options')) :
    /**
     * Functions to manage breadcrumbs
     */
    function fairy_breadcrumb_options()
    {
        global $fairy_theme_options;
        if (($fairy_theme_options['fairy-blog-site-breadcrumb'] == 1) && !is_front_page()) {
            $breadcrumb_from = $fairy_theme_options['fairy-breadcrumb-display-from-option'];

            if ((function_exists('yoast_breadcrumb')) && ($breadcrumb_from == 'yoast-breadcrumb')) {
                ?>
                <div class="fairy-breadcrumb-wrapper">
                    <?php
                    yoast_breadcrumb();
                    ?>
                </div>
                <?php
            } elseif ((function_exists('rankmath-breadcrumb')) && ($breadcrumb_from == 'rank-math')) {
                ?>
                <div class="fairy-breadcrumb-wrapper">
                    <?php
                    rank_math_the_breadcrumbs();
                    ?>
                </div>
                <?php
            } elseif ((function_exists('bcn_display')) && ($breadcrumb_from == 'breadcrumb-navxt')) {
                ?>
                <div class="fairy-breadcrumb-wrapper">
                    <?php
                    bcn_display();
                    ?>
                </div>
                <?php
            } else {
                ?>
                <div class="fairy-breadcrumb-wrapper">
                    <?php
                    fairy_breadcrumbs();
                    ?>
                </div>
                <?php
            }
        }
    }
endif;
add_action('fairy_breadcrumb', 'fairy_breadcrumb_options', 10);


/**
 * BreadCrumb Settings
 */
if (!function_exists('fairy_breadcrumbs')):
    function fairy_breadcrumbs()
    {
        $breadcrumb_args = array(
            'container' => 'div',
            'show_browse' => false
        );
        global $fairy_theme_options;

        $fairy_you_are_here_text = esc_html($fairy_theme_options['fairy-breadcrumb-text']);


        if (!empty($fairy_you_are_here_text)) {
            $fairy_you_are_here_text = "<span class='breadcrumb'>" . $fairy_you_are_here_text . "</span>";
        }
        echo "<div class='breadcrumbs init-animate clearfix'>" . $fairy_you_are_here_text . "<div id='fairy-breadcrumbs' class='clearfix'>";
        breadcrumb_trail($breadcrumb_args);
        echo "</div></div>";

    }
endif;


if (!function_exists('fairy_show_author_links')) :

    function fairy_show_author_links()
    {
        $user_url = get_the_author_meta('user_url');
        $facebook = get_the_author_meta('facebook');
        $twitter = get_the_author_meta('twitter');
        $linkedin = get_the_author_meta('linkedin');
        $youtube = get_the_author_meta('youtube');
        $instagram = get_the_author_meta('instagram');
        $pinterest = get_the_author_meta('pinterest');
        $flickr = get_the_author_meta('flickr');
        $tumblr = get_the_author_meta('tumblr');
        $vk = get_the_author_meta('vk');
        $wordpress = get_the_author_meta('wordpress');
        ?>
        <ul class="author-social-profiles fairy-menu-social">
            <?php
            if (!empty($user_url)) {
                ?>
                <li>
                    <a href="<?php echo esc_url($user_url); ?>" class="website" data-title="Website" target="_blank">
                        <span class="font-icon-social-website"><i class="fa fa-external-link"></i></span>
                    </a>
                </li>
                <?php
            }
            if (!empty($facebook)) {
                ?>
                <li>
                    <a href="<?php echo esc_url($facebook); ?>" class="facebook" data-title="Facebook" target="_blank">
                        <span class="font-icon-social-facebook"><i class="fa fa-facebook"></i></span>
                    </a>
                </li>
                <?php
            }
            if (!empty($twitter)) {
                ?>
                <li>
                    <a href="<?php echo esc_url($twitter); ?>" class="twitter" data-title="Twitter" target="_blank">
                        <span class="font-icon-social-twitter"><i class="fa fa-twitter"></i></span>
                    </a></li>
                <?php
            }
            if (!empty($linkedin)) {
                ?>
                <li><a href="<?php echo esc_url($linkedin); ?>" class="linkedin" data-title="Linkedin" target="_blank">
                        <span class="font-icon-social-linkedin"><i class="fa fa-linkedin"></i></span>
                    </a></li>
                <?php
            }
            if (!empty($instagram)) {
                ?>
                <li><a href="<?php echo esc_url($instagram); ?>" class="instagram" data-title="Instagram"
                       target="_blank">
                        <span class="font-icon-social-instagram"><i class="fa fa-instagram"></i></span>
                    </a></li>
                <?php
            }
            if (!empty($youtube)) { ?>
                <li><a href="<?php echo esc_url($youtube); ?>" class="youtube" data-title="Youtube" target="_blank">
                        <span class="font-icon-social-youtube"><i class="fa fa-youtube"></i></span>
                    </a></li>
                <?php
            }
            if (!empty($pinterest)) {
                ?>
                <li><a href="<?php echo esc_url($pinterest); ?>" class="pinterest" data-title="Pinterest"
                       target="_blank">
                        <span class="font-icon-social-pinterest"><i class="fa fa-pinterest"></i></span>
                    </a></li>
                <?php
            }
            if (!empty($flickr)) {
                ?>
                <li><a href="<?php echo esc_url($flickr); ?>" class="flickr" data-title="Flickr" target="_blank">
                        <span class="font-icon-social-flickr"><i class="fa fa-flickr"></i></span>
                    </a></li>
                <?php
            }
            if (!empty($tumblr)) {
                ?>
                <li><a href="<?php echo esc_url($tumblr); ?>" class="tumblr" data-title="Tumblr" target="_blank">
                        <span class="font-icon-social-tumblr"><i class="fa fa-tumblr"></i></span>
                    </a></li>
                <?php
            }
            if (!empty($vk)) {
                ?>
                <li><a href="<?php echo esc_url($vk); ?>" class="vk" data-title="VK" target="_blank">
                        <span class="font-icon-social-vk"><i class="fa fa-vk"></i></span>
                    </a></li>
                <?php
            }
            if (!empty($wordpress)) {
                ?>
                <li><a href="<?php echo esc_url($wordpress); ?>" class="wordpress" data-title="WordPress"
                       target="_blank">
                        <span class="font-icon-social-wordpress"><i class="fa fa-wordpress"></i></span>
                    </a></li>
                <?php
            }
            ?>
        </ul>
        <?php
    }
endif;
add_action('fairy_author_links', 'fairy_show_author_links', 10);