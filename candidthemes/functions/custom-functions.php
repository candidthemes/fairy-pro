<?php
if (!function_exists('fairy_social_menu')) {
    /**
     * Add social icons menu
     *
     * @since 1.0.0
     *
     */
    function fairy_social_menu()
    {
        if (has_nav_menu('social-menu')) :
            wp_nav_menu(array(
                'theme_location' => 'social-menu',
                'container' => 'ul',
                'menu_class' => 'social-menu'
            ));
        endif;
    }
}


if (!function_exists('fairy_custom_body_class')) {
    /**
     * Add sidebar class in body
     *
     * @since 1.0.0
     *
     */
    function fairy_custom_body_class($classes)
    {
        global $fairy_theme_options;

        // Get box/full width layout option from customizer
        $fairy_layout = !empty($fairy_theme_options['fairy-site-layout-options']) ? $fairy_theme_options['fairy-site-layout-options'] : '';
        if ($fairy_layout == 'boxed') {
            $classes[] = 'ct-boxed';
        }

        // Get dark/layout option from customizer
        $fairy_layout = !empty($fairy_theme_options['fairy-site-dark-light-layout-options']) ? $fairy_theme_options['fairy-site-dark-light-layout-options'] : '';
        if ($fairy_layout == 'dark-layout') {
            $classes[] = 'ct-dark-mode';
        }
        if (!empty($fairy_theme_options['fairy-enable-sticky-sidebar']) && $fairy_theme_options['fairy-enable-sticky-sidebar'] == 1) {
            $classes[] = 'ct-sticky-sidebar';
        }

        if (!empty($fairy_theme_options['fairy-sidebar-title-designs']) && $fairy_theme_options['fairy-sidebar-title-designs'] != 'default-title') {
            $classes[] = 'fairy-widget-' . $fairy_theme_options['fairy-sidebar-title-designs'];
        }

        if (!empty($fairy_theme_options['fairy-font-awesome-version-loading'])) {
            $classes[] = 'fairy-fontawesome-' . $fairy_theme_options['fairy-font-awesome-version-loading'];
        }

        return $classes;
    }
}

add_filter('body_class', 'fairy_custom_body_class');



if (!function_exists('fairy_excerpt_more')) :
    /**
     * Remove ... From Excerpt
     *
     * @since 1.0.0
     */
    function fairy_excerpt_more($more)
    {
        if (!is_admin()) {
            return '';
        }
    }
endif;
add_filter('excerpt_more', 'fairy_excerpt_more');


if (!function_exists('fairy_alter_excerpt')) :
    /**
     * Filter to change excerpt length size
     *
     * @since 1.0.0
     */
    function fairy_alter_excerpt($length)
    {
        if (is_admin()) {
            return $length;
        }
        global $fairy_theme_options;
        $excerpt_length = $fairy_theme_options['fairy-excerpt-length'];
        if (!empty($excerpt_length)) {

            return absint($excerpt_length);
        }
        return 25;
    }
endif;
add_filter('excerpt_length', 'fairy_alter_excerpt');


if (!function_exists('fairy_tag_cloud_widget')) :
    /**
     * Function to modify tag clouds font size
     *
     * @param none
     * @return array $args
     *
     * @since 1.0.0
     *
     */
    function fairy_tag_cloud_widget($args)
    {
        $args['largest'] = 0.9; //largest tag
        $args['smallest'] = 0.9; //smallest tag
        $args['unit'] = 'rem'; //tag font unit
        return $args;
    }
endif;
add_filter('widget_tag_cloud_args', 'fairy_tag_cloud_widget');


/**
 * Google Fonts
 *
 * @param null
 * @return array
 *
 * @since Fairy 1.0.0
 *
 */
if (!function_exists('fairy_google_fonts')) :
    function fairy_google_fonts()
    {
        $fairy_google_fonts = array(
            'ABeeZee:400,400italic' => 'ABeeZee',
            'Abel' => 'Abel',
            'Abril+Fatface' => 'Abril Fatface',
            'Aldrich' => 'Aldrich',
            'Alegreya:400,400italic,700,900' => 'Alegreya',
            'Alex+Brush' => 'Alex Brush',
            'Alfa+Slab+One' => 'Alfa Slab One',
            'Amaranth:400,400italic,700' => 'Amaranth',
            'Andada' => 'Andada',
            'Anton' => 'Anton',
            'Archivo+Black' => 'Archivo Black',
            'Archivo+Narrow:400,400italic,700' => 'Archivo Narrow',
            'Arimo:400,400italic,700' => 'Arimo',
            'Arvo:400,400italic,700' => 'Arvo',
            'Asap:400,400italic,700' => 'Asap',
            'Bangers' => 'Bangers',
            'BenchNine:400,700' => 'BenchNine',
            'Bevan' => 'Bevan',
            'Bitter:400,400italic,700' => 'Bitter',
            'Bree+Serif' => 'Bree Serif',
            'Cabin:400,400italic,500,600,700' => 'Cabin',
            'Cabin+Condensed:400,500,600,700' => 'Cabin Condensed',
            'Cantarell:400,400italic,700' => 'Cantarell',
            'Carme' => 'Carme',
            'Cherry+Cream+Soda' => 'Cherry Cream Soda',
            'Cinzel:400,700,900' => 'Cinzel',
            'Comfortaa:400,300,700' => 'Comfortaa',
            'Cookie' => 'Cookie',
            'Covered+By+Your+Grace' => 'Covered By Your Grace',
            'Crete+Round:400,400italic' => 'Crete Round',
            'Crimson+Text:400,400italic,600,700' => 'Crimson Text',
            'Cuprum:400,400italic' => 'Cuprum',
            'Dancing+Script:400,700' => 'Dancing Script',
            'Didact+Gothic' => 'Didact Gothic',
            'Droid+Sans:400,700' => 'Droid Sans',
            'Domine' => 'Domine',
            'Dosis:400,300,600,800' => 'Dosis',
            'Droid+Serif:400,400italic,700' => 'Droid Serif',
            'Economica:400,700,400italic' => 'Economica',
            'EB+Garamond' => 'EB Garamond',
            'Exo:400,300,400italic,600,800' => 'Exo',
            'Exo +2:400,300,400italic,600,700,900' => 'Exo 2',
            'Fira+Sans:400,500' => 'Fira Sans',
            'Fjalla+One' => 'Fjalla One',
            'Francois+One' => 'Francois One',
            'Fredericka+the+Great' => 'Fredericka the Great',
            'Fredoka+One' => 'Fredoka One',
            'Fugaz+One' => 'Fugaz One',
            'Great+Vibes' => 'Great Vibes',
            'Handlee' => 'Handlee',
            'Hammersmith+One' => 'Hammersmith One',
            'Hind:400,300,600,700' => 'Hind',
            'Inconsolata:400,700' => 'Inconsolata',
            'Indie+Flower' => 'Indie Flower',
            'Istok+Web:400,400italic,700' => 'Istok Web',
            'Josefin+Sans:400,600,700,400italic' => 'Josefin Sans',
            'Josefin+Slab:400,400italic,700,600' => 'Josefin Slab',
            'Jura:400,300,500,600' => 'Jura',
            'Karla:400,400italic,700' => 'Karla',
            'Kaushan+Script' => 'Kaushan Script',
            'Kreon:400,300,700' => 'Kreon',
            'Lateef' => 'Lateef',
            'Lato:400,300,400italic,900,700' => 'Lato',
            'Libre+Baskerville:400,400italic,700' => 'Libre Baskerville',
            'Limelight' => 'Limelight',
            'Lobster' => 'Lobster',
            'Lobster+Two:400,700,700italic' => 'Lobster Two',
            'Lora:400,400i' => 'Lora',
            'Maven+Pro:400,500,700,900' => 'Maven Pro',
            'Merriweather:400,400italic,300,900,700' => 'Merriweather',
            'Merriweather+Sans:400,400italic,700,800' => 'Merriweather Sans',
            'Monda:400,700' => 'Monda',
            'Montserrat:400,700' => 'Montserrat',
            'Muli:400,300italic,300' => 'Muli',
            'News+Cycle:400,700' => 'News Cycle',
            'Noticia+Text:400,400italic,700' => 'Noticia Text',
            'Noto +Sans:400,400italic,700' => 'Noto Sans',
            'Noto +Serif:400,400italic,700' => 'Noto Serif',
            'Nunito:400,300,700' => 'Nunito',
            'Old+Standard +TT:400,400italic,700' => 'Old Standard TT',
            'Open+Sans:400,400italic,600,700' => 'Open Sans',
            'Open+Sans+Condensed:300,300italic,700' => 'Open Sans Condensed',
            'Oswald:400,300,700' => 'Oswald',
            'Oxygen:400,300,700' => 'Oxygen',
            'Pacifico' => 'Pacifico',
            'Passion+One:400,700,900' => 'Passion One',
            'Pathway+Gothic+One' => 'Pathway Gothic One',
            'Patua+One' => 'Patua One',
            'Poiret+One' => 'Poiret One',
            'Pontano+Sans' => 'Pontano Sans',
            'Poppins:400,500,600,700' => 'Poppins',
            'Play:400,700' => 'Play',
            'Playball' => 'Playball',
            'Playfair+Display:400,400italic,700,900' => 'Playfair Display',
            'PT+Sans:400,400italic,700' => 'PT Sans',
            'PT+Sans+Caption:400,700' => 'PT Sans Caption',
            'PT+Sans+Narrow:400,700' => 'PT Sans Narrow',
            'PT+Serif:400,400italic,700' => 'PT Serif',
            'Quattrocento+Sans:400,700,400italic' => 'Quattrocento Sans',
            'Questrial' => 'Questrial',
            'Quicksand:400,700' => 'Quicksand',
            'Raleway:400,300,500,600,700,900' => 'Raleway',
            'Righteous' => 'Righteous',
            'Roboto:400,500,300,700,400italic' => 'Roboto',
            'Roboto+Condensed:400,300,400italic,700' => 'Roboto Condensed',
            'Roboto+Slab:400,300,700' => 'Roboto Slab',
            'Rokkitt:400,700' => 'Rokkitt',
            'Ropa+Sans:400,400italic' => 'Ropa Sans',
            'Russo+One' => 'Russo One',
            'Sanchez:400,400italic' => 'Sanchez',
            'Satisfy' => 'Satisfy',
            'Shadows+Into+Light' => 'Shadows Into Light',
            'Sigmar+One' => 'Sigmar One',
            'Signika:400,300,700' => 'Signika',
            'Six+Caps' => 'Six Caps',
            'Slabo+27px' => 'Slabo 27px',
            'Source+Sans+Pro:400,400italic,600,900,300' => 'Source Sans Pro',
            'Squada+One' => 'Squada One',
            'Tangerine:400,700' => 'Tangerine',
            'Tinos:400,400italic,700' => 'Tinos',
            'Titillium+Web:400,300,400italic,700,900' => 'Titillium Web',
            'Ubuntu:400,400italic,500,700' => 'Ubuntu',
            'Ubuntu+Condensed' => 'Ubuntu Condensed',
            'Varela+Round' => 'Varela Round',
            'Vollkorn:400,400italic,700' => 'Vollkorn',
            'Voltaire' => 'Voltaire',
            'Yanone+Kaffeesatz:400,300,700' => 'Yanone Kaffeesatz'
        );
        return apply_filters('fairy_google_fonts', $fairy_google_fonts);
    }
endif;


/**
 * Enqueue the list of fonts.
 */
function fairy_customizer_fonts()
{
    wp_enqueue_style('fairy_customizer_fonts', 'https://fonts.googleapis.com/css?family=ABeeZee|Abel|Abril+Fatface|Aldrich|Alegreya|Alex+Brush|Alfa+Slab+One|Amaranth|Andada|Anton|Archivo+Black|Archivo+Narrow|Arimo|Arimo|Arvo|Asap|Bangers|BenchNine|Bevan|Bitter|Bree+Serif|Cabin|Cabin+Condensed|Cantarell|Carme|Cherry+Cream+Soda|Cinzel|Comfortaa|Cookie|Covered+By+Your+Grace|Crete+Round|Crimson+Text|Cuprum|Dancing+Script|Didact+Gothic|Droid+Sans|Domine|Dosis|Droid+Serif|Economica|EB+Garamond|Exo|Exo|Fira+Sans|Fjalla+One|Francois+One|Fredericka+the+Great|Fredoka+One|Fugaz+One|Great+Vibes|Handlee|Hammersmith+One|Hind|Inconsolata|Indie+Flower|Istok+Web|Josefin+Sans|Josefin+Slab|Jura|Karla|Kaushan+Script|Kreon|Lateef|Lato|Lato|Libre+Baskerville|Limelight|Lobster|Lobster+Two|Lora|Maven+Pro|Merriweather|Merriweather+Sans|Monda|Montserrat|Muli|News+Cycle|Noticia+Text|Noto+Sans|Noto+Serif|Nunito|Old+Standard +TT|Open+Sans|Open+Sans+Condensed|Oswald|Oxygen|Pacifico|Passion+One|Passion One|Pathway+Gothic+One|Patua+One|Poiret+One|Pontano+Sans|Poppins|Play|Playball|Playfair+Display|PT+Sans|PT+Sans+Caption|PT+Sans+Narrow|PT+Serif|Quattrocento+Sans|Questrial|Quicksand|Raleway|Righteous|Roboto|Roboto+Condensed|Roboto+Slab|Rokkitt|Ropa+Sans|Russo+One|Sanchez|Satisfy|Shadows+Into+Light|Sigmar+One|Signika|Six+Caps|Slabo+27px|Source+Sans+Pro|Squada+One|Tangerine|Tinos|Titillium+Web|Ubuntu|Ubuntu+Condensed|Varela+Round|Vollkorn|Voltaire|Yanone+Kaffeesatz', array(), null);
}

add_action('customize_controls_print_styles', 'fairy_customizer_fonts');
add_action('customize_preview_init', 'fairy_customizer_fonts');

add_action(
    'customize_controls_print_styles',
    function () {
?>
    <style>
        <?php
        $arr = array('ABeeZee', 'Abel', 'Abril+Fatface', 'Aldrich', 'Alegreya', 'Alex+Brush', 'Alfa+Slab+One', 'Amaranth', 'Andada', 'Anton', 'Archivo+Black', 'Archivo+Narrow', 'Arimo', 'Arimo', 'Arvo', 'Asap', 'Bangers', 'BenchNine', 'Bevan', 'Bitter', 'Bree+Serif', 'Cabin', 'Cabin+Condensed', 'Cantarell', 'Carme', 'Cherry+Cream+Soda', 'Cinzel', 'Comfortaa', 'Cookie', 'Covered+By+Your+Grace', 'Crete+Round', 'Crimson+Text', 'Cuprum', 'Dancing+Script', 'Didact+Gothic', 'Droid+Sans', 'Domine', 'Dosis', 'Droid+Serif', 'Economica', 'EB+Garamond', 'Exo', 'Exo', 'Fira+Sans', 'Fjalla+One', 'Francois+One', 'Fredericka+the+Great', 'Fredoka+One', 'Fugaz+One', 'Great+Vibes', 'Handlee', 'Hammersmith+One', 'Hind', 'Inconsolata', 'Indie+Flower', 'Istok+Web', 'Josefin+Sans', 'Josefin+Slab', 'Jura', 'Karla', 'Kaushan+Script', 'Kreon', 'Lateef', 'Lato', 'Lato', 'Libre+Baskerville', 'Limelight', 'Lobster', 'Lobster+Two', 'Lora', 'Maven+Pro', 'Merriweather', 'Merriweather+Sans', 'Monda', 'Montserrat', 'Muli', 'News+Cycle', 'Noticia+Text', 'Noto+Sans', 'Noto+Serif', 'Nunito', 'Old+Standard +TT', 'Open+Sans', 'Open+Sans+Condensed', 'Oswald', 'Oxygen', 'Pacifico', 'Passion+One', 'Passion One', 'Pathway+Gothic+One', 'Patua+One', 'Poiret+One', 'Pontano+Sans', 'Poppins', 'Play', 'Playball', 'Playfair+Display', 'PT+Sans', 'PT+Sans+Caption', 'PT+Sans+Narrow', 'PT+Serif', 'Quattrocento+Sans', 'Questrial', 'Quicksand', 'Raleway', 'Righteous', 'Roboto', 'Roboto+Condensed', 'Roboto+Slab', 'Rokkitt', 'Ropa+Sans', 'Russo+One', 'Sanchez', 'Satisfy', 'Shadows+Into+Light', 'Sigmar+One', 'Signika', 'Six+Caps', 'Slabo+27px', 'Source+Sans+Pro', 'Squada+One', 'Tangerine', 'Tinos', 'Titillium+Web', 'Ubuntu', 'Ubuntu+Condensed', 'Varela+Round', 'Vollkorn', 'Voltaire', 'Yanone+Kaffeesatz');

        foreach ($arr as $font) {
            $font_family = str_replace("+", " ", $font);
            echo '.customize-control select option[value*="' . $font . '"] {font-family: ' . $font_family . '; font-size: 22px;}';
        }
        ?>
    </style>
<?php
    }
);


if (!function_exists('fairy_editor_assets')) {
    /**
     * Add styles and fonts for the editor.
     */
    function fairy_editor_assets()
    {
        wp_enqueue_style('fairy-fonts', fairy_fonts_url(), array(), null);
        wp_enqueue_style('fairy-blocks', get_theme_file_uri('/candidthemes/assets/css/block-editor.css'), false);
    }

    add_action('enqueue_block_editor_assets', 'fairy_editor_assets');
}




/**
 * Custom theme hooks for Users Info
 *
 * This file contains hook functions attached to theme hooks.
 *
 * @package
 */
if (!function_exists('fairy_user_contact_methods')) :

    /**
     * Added For Author Page
     *
     * @since 1.0.0
     */

    // Register User Contact Methods
    function fairy_user_contact_methods($user_contact_method)
    {

        $user_contact_method['facebook'] = __('Enter Facebook Url', 'fairy');
        $user_contact_method['twitter'] = __('Enter Twitter Url', 'fairy');
        $user_contact_method['linkedin'] = __('Enter Linkedin Url', 'fairy');
        $user_contact_method['youtube'] = __('Enter Youtube Url', 'fairy');
        $user_contact_method['instagram'] = __('Enter Instagram Url', 'fairy');
        $user_contact_method['pinterest'] = __('Enter Pinterest Url', 'fairy');
        $user_contact_method['flickr'] = __('Enter Flickr Url', 'fairy');
        $user_contact_method['tumblr'] = __('Enter Tumblr Url', 'fairy');
        $user_contact_method['vk'] = __('Enter VK Url', 'fairy');
        $user_contact_method['wordpress'] = __('Enter WordPress Url', 'fairy');

        return $user_contact_method;
    }

    add_filter('user_contactmethods', 'fairy_user_contact_methods');
endif;


/**
 * Save user visits
 *
 * @param int $post_id
 * @return null
 *
 * @since 1.0.0
 *
 */
if (!function_exists('fairy_popular_meta')) :

    function fairy_popular_meta($post_id)
    {
        $count_post = get_post_meta($post_id, 'post_views_count', true);

        if ((int)$count_post >= 0) {
            $count_post = (int)$count_post + 1;
            update_post_meta($post_id, 'post_views_count', $count_post);
        } else {
            $count_post = 0;
            delete_post_meta($post_id, 'post_views_count');
            add_post_meta($post_id, 'post_views_count', $count_post);
        }
    }
endif;
add_action('fairy_popular_meta', 'fairy_popular_meta', 10, 1);
