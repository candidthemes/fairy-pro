<?php
if (!function_exists('fairy_do_skip_to_content_link')) {
    /**
     * Add skip to content link before the header.
     *
     * @since 1.0.0
     */
    function fairy_do_skip_to_content_link()
    {
        ?>
        <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'fairy'); ?></a>
        <?php
    }
}
add_action('fairy_before_header', 'fairy_do_skip_to_content_link', 10);


if (!function_exists('fairy_header_search_modal')) {
    /**
     * Add search modal before header
     *
     * @since 1.0.0
     */
    function fairy_header_search_modal()
    {
        global $fairy_theme_options;
        if (($fairy_theme_options['fairy-enable-top-header-search'] != 1) || ($fairy_theme_options['fairy-enable-top-header'] != 1))
            return false;
        ?>
        <section class="search-section">
            <div class="container">
                <button class="close-btn"><i class="fa fa-times"></i></button>
                <?php get_search_form(); ?>
            </div>
        </section>
        <?php

    }
}
add_action('fairy_header', 'fairy_header_search_modal', 10);


if (!function_exists('fairy_construct_header')) {
    /**
     * Add header
     *
     * @since 1.0.0
     */
    function fairy_construct_header()
    {
        global $fairy_theme_options;
        $fairy_header_type = $fairy_theme_options['fairy-header-types'];
        $fairy_enable_top_header = $fairy_theme_options['fairy-enable-top-header'];
        $fairy_enable_top_social = $fairy_theme_options['fairy-enable-top-header-social'];
        $fairy_enable_top_menu = $fairy_theme_options['fairy-enable-top-header-menu'];
        $fairy_enable_top_search = $fairy_theme_options['fairy-enable-top-header-search'];
        if ($fairy_header_type == 'header-one') {
            $header_class = 'site-header-left-logo';
            $topbar_class = 'site-header-top';
        } elseif ($fairy_header_type == 'header-two') {
            $header_class = 'site-header-v2 site-header-menu-top ';
            $topbar_class = 'site-header-topbar';
            if($fairy_theme_options['fairy-header-logo-position'] == 'left-logo'){
                $header_class .= 'left-logo-ad';
            }
        } else {
            $header_class = 'site-header-v2 ';
            $topbar_class = 'site-header-topbar';
            if($fairy_theme_options['fairy-header-logo-position'] == 'left-logo'){
                $header_class .= 'left-logo-ad';
            }
        }
        ?>
        <!-- for sticky header add [.is-sticky-header] class on header tag -->
        <header id="masthead" class="site-header text-center <?php echo $header_class ?>">
            <?php
            if (($fairy_enable_top_header == 1) && (($fairy_enable_top_menu == 1) || ($fairy_enable_top_search == 1) || ($fairy_enable_top_social == 1))) {
                ?>
                <section class="<?php echo $topbar_class; ?>">
                    <div class="container">
                        <div class="row">
                            <div class="col col-sm-2-3 col-md-2-3 col-lg-2-4">
                                <?php
                                /**
                                 * fairy_top_left hook.
                                 *
                                 * @since 1.0.0
                                 *
                                 * @hooked fairy_top_menu - 10
                                 *
                                 */
                                do_action('fairy_top_left');
                                ?>
                            </div>
                            <div class="col col-sm-1-3 col-md-1-3 col-lg-1-4">
                                <div class="fairy-menu-social topbar-flex-grid">
                                    <?php
                                    /**
                                     * fairy_top_right hook.
                                     *
                                     * @since 1.0.0
                                     *
                                     * @hooked fairy_top_search - 10
                                     * @hooked fairy_top_social - 20
                                     *
                                     */
                                    do_action('fairy_top_right');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php
            }

            /**
             * fairy_main_header hook.
             *
             * @since 1.0.0
             *
             * @hooked fairy_construct_main_header - 10
             */
            do_action('fairy_main_header');
            ?>
        </header><!-- #masthead -->
        <?php

    }
}
add_action('fairy_header', 'fairy_construct_header', 20);

if (!function_exists('fairy_top_menu')) {
    /**
     * Add menu on top header.
     *
     * @since 1.0.0
     */
    function fairy_top_menu()
    {
        global $fairy_theme_options;
        if ($fairy_theme_options['fairy-enable-top-header-menu'] != 1)
            return false;
        ?>
        <nav class="site-header-top-nav">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'top-menu',
                'container' => 'ul',
                'menu_class' => 'site-header-top-menu',
                'depth' => 1
            ));
            ?>
        </nav>
        <?php
    }
}
add_action('fairy_top_left', 'fairy_top_menu', 10);


if (!function_exists('fairy_top_search')) {
    /**
     * Add search icon on top header.
     *
     * @since 1.0.0
     */
    function fairy_top_search()
    {
        global $fairy_theme_options;
        if ($fairy_theme_options['fairy-enable-top-header-search'] != 1)
            return false;
        ?>
        <button class="search-toggle"><i class="fa fa-search"></i></button>
        <?php
    }
}
add_action('fairy_top_right', 'fairy_top_search', 10);

if (!function_exists('fairy_top_social')) {
    /**
     * Add social icon menu on top header.
     *
     * @since 1.0.0
     */
    function fairy_top_social()
    {
        global $fairy_theme_options;
        if ($fairy_theme_options['fairy-enable-top-header-social'] != 1)
            return false;
        fairy_social_menu();
    }
}
add_action('fairy_top_right', 'fairy_top_social', 20);

if (!function_exists('fairy_construct_main_header')) {
    /**
     * Add Main Header
     *
     * @since 1.0.0
     */
    function fairy_construct_main_header()
    {
        global $fairy_theme_options;
        if ($fairy_theme_options['fairy-header-types'] == 'header-one') {
            /**
             * fairy_header_one hook.
             *
             * @since 1.0.0
             *
             * @hooked fairy_header_left_logo - 10
             */
            do_action('fairy_header_one');
        } elseif ($fairy_theme_options['fairy-header-types'] == 'header-two') {
            /**
             * fairy_header_two hook.
             *
             * @since 1.0.0
             *
             * @hooked fairy_header_main_menu_top - 10
             */
            do_action('fairy_header_two');
        } else {
            /**
             * fairy_header_default hook.
             *
             * @since 1.0.0
             *
             * @hooked fairy_default_header - 10
             */
            do_action('fairy_header_default');
        }

    }
}
add_action('fairy_main_header', 'fairy_construct_main_header', 10);


if (!function_exists('fairy_header_left_logo')) {
    /**
     * Add left logo header
     *
     * @since 1.0.0
     */
    function fairy_header_left_logo()
    {
        //has header image
        $has_header_image = has_header_image();
        global $fairy_theme_options;
        $fairy_enable_header_sticky = $fairy_theme_options['fairy-enable-sticky-primary-menu'];
        $header_class = '';
        if($fairy_enable_header_sticky == 1){
            $header_class = 'is-sticky-header';
        }
        ?>
        <!-- for sticky header add [.is-sticky-header] class on header tag -->
        <section class="site-header-bottom header-main-bar <?php echo $header_class; ?>" <?php if (!empty($has_header_image)) { ?> style="background-image: url(<?php echo header_image(); ?>);" <?php } ?>>
            <div class="container">
                <div class="row">
                    <div class="col col-md-1-4 col-lg-1-4">

                        <?php
                        /**
                         * fairy_branding hook.
                         *
                         * @since 1.0.0
                         *
                         * @hooked fairy_construct_branding - 10
                         */
                        do_action('fairy_branding');
                        ?>
                    </div>

                    <div class="col col-md-2-4 col-lg-2-4">
                        <?php
                        /**
                         * fairy_header_default hook.
                         *
                         * @since 1.0.0
                         *
                         * @hooked fairy_construct_main_menu - 10
                         */
                        do_action('fairy_main_menu');
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <?php
    }
}
add_action('fairy_header_one', 'fairy_header_left_logo', 10);


if (!function_exists('fairy_header_main_menu_top')) {
    /**
     * Add left logo header
     *
     * @since 1.0.0
     */
    function fairy_header_main_menu_top()
    {
        //has header image
        $has_header_image = has_header_image();
        global $fairy_theme_options;
        $fairy_enable_header_sticky = $fairy_theme_options['fairy-enable-sticky-primary-menu'];
        $header_class = '';
        if($fairy_enable_header_sticky == 1){
            $header_class = 'is-sticky-header ';
        }
        if($fairy_theme_options['fairy-header-menu-position'] == 'left-menu'){
            $header_class .= 'left-menu';
        }
        ?>

        <section class="site-header-bottom <?php echo $header_class; ?>">
            <div class="container">
                <?php
                /**
                 * fairy_header_default hook.
                 *
                 * @since 1.0.0
                 *
                 * @hooked fairy_construct_main_menu - 10
                 */
                do_action('fairy_main_menu');
                ?>
            </div>
        </section>

        <section class="site-header-top header-main-bar" <?php if (!empty($has_header_image)) { ?> style="background-image: url(<?php echo header_image(); ?>);" <?php } ?>>
            <div class="container">
                <div class="row">
                    <div class="col-1-1">
                        <?php
                        /**
                         * fairy_branding hook.
                         *
                         * @since 1.0.0
                         *
                         * @hooked fairy_construct_branding - 10
                         */
                        do_action('fairy_branding');
                        ?>
                    </div>
                    <?php
                    /**
                     * fairy_header_ad hook.
                     *
                     * @since 1.0.0
                     *
                     * @hooked fairy_construct_header_ad - 10
                     */
                    do_action('fairy_header_ad');
                    ?>
                </div>
            </div>
        </section>
        <?php
    }
}
add_action('fairy_header_two', 'fairy_header_main_menu_top', 10);


if (!function_exists('fairy_default_header')) {
    /**
     * Add Default header
     *
     * @since 1.0.0
     */
    function fairy_default_header()
    {

        //has header image
        $has_header_image = has_header_image();

        global $fairy_theme_options;
        $fairy_enable_header_sticky = $fairy_theme_options['fairy-enable-sticky-primary-menu'];
        $header_class = '';
        if($fairy_enable_header_sticky == 1){
            $header_class = 'is-sticky-header ';
        }

        if($fairy_theme_options['fairy-header-menu-position'] == 'left-menu'){
            $header_class .= 'left-menu';
        }
        ?>

        <section class="site-header-top header-main-bar" <?php if (!empty($has_header_image)) { ?> style="background-image: url(<?php echo header_image(); ?>);" <?php } ?>>
            <div class="container">
                <div class="row">
                    <div class="col-1-1">
                        <?php
                        /**
                         * fairy_branding hook.
                         *
                         * @since 1.0.0
                         *
                         * @hooked fairy_construct_branding - 10
                         */
                        do_action('fairy_branding');
                        ?>
                    </div>
                    <?php
                    /**
                     * fairy_header_ad hook.
                     *
                     * @since 1.0.0
                     *
                     * @hooked fairy_construct_header_ad - 10
                     */
                    do_action('fairy_header_ad');
                    ?>

                </div>
            </div>
        </section>

        <!-- add [.lef-menu] class for left align navigation menu -->
        <section class="site-header-bottom <?php echo $header_class; ?>">
            <div class="container">
                <?php
                /**
                 * fairy_main_menu hook.
                 *
                 * @since 1.0.0
                 *
                 * @hooked fairy_construct_main_menu - 10
                 */
                do_action('fairy_main_menu');
                ?>

            </div>
        </section>
        <?php
    }
}
add_action('fairy_header_default', 'fairy_default_header', 10);





if (!function_exists('fairy_construct_branding')) {
    /**
     * Add Branding on Header
     *
     * @since 1.0.0
     */
    function fairy_construct_branding()
    {
        ?>
        <div class="site-branding">
            <?php
            the_custom_logo();
            if (is_front_page() && is_home()) :
                ?>
                <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>"
                                          rel="home"><?php bloginfo('name'); ?></a></h1>
            <?php
            else :
                ?>
                <p class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>"
                                         rel="home"><?php bloginfo('name'); ?></a></p>
            <?php
            endif;
            $fairy_description = get_bloginfo('description', 'display');
            if ($fairy_description || is_customize_preview()) :
                ?>
                <p class="site-description"><?php echo $fairy_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                    ?></p>
            <?php endif; ?>
        </div><!-- .site-branding -->
        <?php
    }
}
add_action('fairy_branding', 'fairy_construct_branding', 10);



if (!function_exists('fairy_construct_main_menu')) {
    /**
     * Add Main Menu on Header
     *
     * @since 1.0.0
     */
    function fairy_construct_main_menu()
    {
        ?>
        <nav id="site-navigation" class="main-navigation">
            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </button>
            <ul id="primary-menu" class="nav navbar-nav nav-menu justify-content-center">
                <?php
                if (has_nav_menu('menu-1')) :
                    wp_nav_menu(array(
                        'theme_location' => 'menu-1',
                        'items_wrap' => '%3$s',
                        'container' => false
                    ));
                else:
                    wp_list_pages(array('depth' => 0, 'title_li' => ''));
                endif; // has_nav_menu
                ?>
            </ul>
        </nav><!-- #site-navigation -->
        <?php
    }
}
add_action('fairy_main_menu', 'fairy_construct_main_menu', 10);




if (!function_exists('fairy_construct_header_ad')) {
    /**
     * Add Advertisement image on Header
     *
     * @since 1.0.0
     */
    function fairy_construct_header_ad()
    {
        global $fairy_theme_options;

        $header_ad_image_url = $fairy_theme_options['fairy-header-ads-image'];
        if (empty($header_ad_image_url))
            return false;
        $header_ad_image_link = $fairy_theme_options['fairy-header-ads-image-link'];
        ?>
        <div class="col-1-1">
            <div class="site-header-ad-wrapper">
                <?php
                if(!empty($header_ad_image_link)){
                    ?>
                    <a href="<?php echo $header_ad_image_link; ?>" target="_blank"><img src="<?php echo $header_ad_image_url; ?>" alt="header image"> </a>
                    <?php
                }else {
                    ?>
                    <img src="<?php echo $header_ad_image_url; ?>" alt="header image">
                    <?php
                }
                    ?>
            </div>
        </div>
        <?php
    }
}
add_action('fairy_header_ad', 'fairy_construct_header_ad', 10);