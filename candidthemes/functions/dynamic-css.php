<?php

/**
 * Dynamic CSS elements.
 *
 * @package Fairy
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}


if (!function_exists('fairy_dynamic_css')) :
    /**
     * Dynamic CSS
     *
     * @param null
     * @return null
     *
     * @since 1.0.0
     *
     */
    function fairy_dynamic_css()
    {

        global $fairy_theme_options;

        /*Color Options */
        $fairy_header_color = get_header_textcolor();
        $fairy_custom_css = '';
        $primary_color = !empty($fairy_theme_options['fairy-primary-color']) ? esc_html($fairy_theme_options['fairy-primary-color']) : '';
        $site_description_color = !empty($fairy_theme_options['fairy-header-description-color']) ? esc_html($fairy_theme_options['fairy-header-description-color']) : '';

        if (!empty($primary_color)) {
            $fairy_custom_css .= ".primary-color, p a, h1 a, h2 a, h3 a, h4 a, h5 a, .author-title a, figure a, table a, span a, strong a, li a, h1 a, .btn-primary-border, .main-navigation #primary-menu > li > a:hover, .widget-area .widget a:hover, .widget-area .widget a:focus, .card-bg-image .card_body a:hover, .main-navigation #primary-menu > li.focus > a, .category-label-group a:hover, .card_title a:hover, .card-blog-post .entry-meta a:hover, .site-title a:visited, .post-navigation .nav-links > * a:hover, .post-navigation .nav-links > * a:focus, .wp-block-button.is-style-outline .wp-block-button__link, .error-404-title, .ct-dark-mode a:hover, .ct-dark-mode .widget ul li a:hover, .ct-dark-mode .widget-area .widget ul li a:hover, .ct-dark-mode .post-navigation .nav-links > * a, .ct-dark-mode .author-wrapper .author-title a, .ct-dark-mode .wp-calendar-nav a, .site-footer a:hover, .top-header-toggle-btn, .woocommerce ul.products li.product .woocommerce-loop-category__title:hover, .woocommerce ul.products li.product .woocommerce-loop-product__title:hover, .woocommerce ul.products li.product h3:hover, .entry-content dl a, .entry-content table a, .entry-content ul a, .breadcrumb-trail .trial-items a:hover, .breadcrumbs ul a:hover, .breadcrumb-trail .trial-items .trial-end a, .breadcrumbs ul li:last-child a, .comment-list .comment .comment-body a, .comment-list .comment .comment-body .comment-metadata a.comment-edit-link { color: {$primary_color}; }";

            $fairy_custom_css .= ".primary-bg, .btn-primary, .main-navigation ul ul a, #secondary .widget .widget-title:after, #primary .widget .widget-title:after,.fairy-before-footer-widgets .widget .widget-title:after,.fairy-after-slider-widgets .widget .widget-title:after, .search-form .search-submit, .category-label-group a:after, .posts-navigation .nav-links a, .category-label-group.bg-label a, .wp-block-button__link, .wp-block-button.is-style-outline .wp-block-button__link:hover, button, input[type=\"button\"], input[type=\"reset\"], input[type=\"submit\"], .pagination .page-numbers.current, .pagination .page-numbers:hover, table #today, .tagcloud .tag-cloud-link:hover, .footer-go-to-top,  .fairy-menu-social a.website, #masthead #primary-menu.off_canva_nav > .close-nav, .woocommerce ul.products li.product .button,
.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,
.woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover, .woocommerce #respond input#submit.alt:disabled, .woocommerce #respond input#submit.alt:disabled:hover, .woocommerce #respond input#submit.alt:disabled[disabled], .woocommerce #respond input#submit.alt:disabled[disabled]:hover, .woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover, .woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover, .woocommerce a.button.alt:disabled[disabled], .woocommerce a.button.alt:disabled[disabled]:hover, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:disabled, .woocommerce button.button.alt:disabled:hover, .woocommerce button.button.alt:disabled[disabled], .woocommerce button.button.alt:disabled[disabled]:hover, .woocommerce input.button.alt.disabled, .woocommerce input.button.alt.disabled:hover, .woocommerce input.button.alt:disabled, .woocommerce input.button.alt:disabled:hover, .woocommerce input.button.alt:disabled[disabled], .woocommerce input.button.alt:disabled[disabled]:hover, .woocommerce .widget_price_filter .ui-slider .ui-slider-range, .woocommerce .widget_price_filter .ui-slider .ui-slider-handle, .fairy-menu-social li a[href*=\"mailto:\"]  { background-color: {$primary_color}; }";

            $fairy_custom_css .= ".primary-border, .btn-primary-border, .site-header-v2,.wp-block-button.is-style-outline .wp-block-button__link, .site-header-bottom { border-color: {$primary_color}; }";

            $fairy_custom_css .= "a:focus, button.slick-arrow:focus, input[type=text]:focus, input[type=email]:focus, input[type=password]:focus, input[type=search]:focus, input[type=file]:focus, input[type=number]:focus, input[type=datetime]:focus, input[type=url]:focus, select:focus, textarea:focus { outline-color: {$primary_color}; }";

            $fairy_custom_css .= ".entry-content a { text-decoration-color: {$primary_color}; }";
            $fairy_custom_css .= ".post-thumbnail a:focus img, .card_media a:focus img, article.sticky .card-blog-post { border-color: {$primary_color}; }";
        }
        if (!empty($fairy_header_color)) {
            $fairy_custom_css .= ".site-title, .site-title a, h1.site-title a, p.site-title a,  .site-title a:visited { color: #{$fairy_header_color}; }";
        }

        if (!empty($site_description_color)) {
            $fairy_custom_css .= ".site-description { color: {$site_description_color}; }";
        }


        /* Paragraph Font Options */
        $fairy_google_fonts = fairy_google_fonts();
        $fairy_body_fonts = !empty($fairy_theme_options['fairy-font-family-url']) ? $fairy_theme_options['fairy-font-family-url'] : '';
        if (!empty($fairy_body_fonts)) {
            $fairy_font_family = esc_attr($fairy_google_fonts[$fairy_body_fonts]);
        }

        $fairy_font_size = !empty($fairy_theme_options['fairy-font-paragraph-font-size']) ? absint($fairy_theme_options['fairy-font-paragraph-font-size']) : 16;
        $fairy_line_height = !empty($fairy_theme_options['fairy-font-line-height']) ? esc_attr($fairy_theme_options['fairy-font-line-height']) : 1.5;
        $fairy_letter_spacing = !empty($fairy_theme_options['fairy-letter-spacing']) ? esc_attr($fairy_theme_options['fairy-letter-spacing']) : 0;


        /* Menu Font Options */
        $fairy_menu_fonts = !empty($fairy_theme_options['fairy-menu-font-family-url']) ? esc_attr($fairy_theme_options['fairy-menu-font-family-url']) : '';
        if (!empty($fairy_menu_fonts)) {
            $fairy_menu_font_family = $fairy_google_fonts[$fairy_menu_fonts];
        }
        $fairy_menu_font_size = !empty($fairy_theme_options['fairy-menu-font-size']) ? absint($fairy_theme_options['fairy-menu-font-size']) : 16;

        $fairy_menu_line_height = !empty($fairy_theme_options['fairy-menu-font-line-height']) ? esc_attr($fairy_theme_options['fairy-menu-font-line-height']) : 1.5;
        $fairy_menu_letter_spacing = !empty($fairy_theme_options['fairy-menu-letter-spacing']) ? esc_attr($fairy_theme_options['fairy-menu-letter-spacing']) : 0;


        //widget
        $fairy_widget_fonts = !empty($fairy_theme_options['fairy-widget-font-family-url']) ? esc_attr($fairy_theme_options['fairy-widget-font-family-url']) : '';

        if (!empty($fairy_widget_fonts)) {
            $fairy_widget_font_family = $fairy_google_fonts[$fairy_widget_fonts];
        }


        $fairy_widget_font_size = !empty($fairy_theme_options['fairy-widget-font-size']) ? absint($fairy_theme_options['fairy-widget-font-size']) : 20;
        $fairy_widget_title_line_height = !empty($fairy_theme_options['fairy-widget-font-line-height']) ? esc_attr($fairy_theme_options['fairy-widget-font-line-height']) : 1.125;
        $fairy_widget_title_letter_spacing = !empty($fairy_theme_options['fairy-widget-letter-spacing']) ? esc_attr($fairy_theme_options['fairy-widget-letter-spacing']) : 0;


        //Post Title
        $fairy_post_fonts = !empty($fairy_theme_options['fairy-post-font-family-url']) ? esc_attr($fairy_theme_options['fairy-post-font-family-url']) : '';
        if (!empty($fairy_post_fonts)) {
            $fairy_post_font_family = $fairy_google_fonts[$fairy_post_fonts];
        }


        $fairy_post_font_size = !empty($fairy_theme_options['fairy-post-font-size']) ? absint($fairy_theme_options['fairy-post-font-size']) : 20;
        $fairy_post_title_line_height = !empty($fairy_theme_options['fairy-post-font-line-height']) ? esc_attr($fairy_theme_options['fairy-post-font-line-height']) : 1.125;
        $fairy_post_title_letter_spacing = !empty($fairy_theme_options['fairy-post-letter-spacing']) ? esc_attr($fairy_theme_options['fairy-post-letter-spacing']) : 0;

        /* Heading H1 Font Option */
        $fairy_h1_fonts = !empty($fairy_theme_options['fairy-font-heading-family-url']) ? esc_attr($fairy_theme_options['fairy-font-heading-family-url']) : '';
        if (!empty($fairy_h1_fonts)) {
            $fairy_h1_font_family = $fairy_google_fonts[$fairy_h1_fonts];
        }

        /* Heading H2 Font Option */
        $fairy_h2_fonts = !empty($fairy_theme_options['fairy-font-h2-family-url']) ? esc_attr($fairy_theme_options['fairy-font-h2-family-url']) : '';
        if (!empty($fairy_h2_fonts)) {
            $fairy_h2_font_family = $fairy_google_fonts[$fairy_h2_fonts];
        }

        /* Heading H3 Font Option */
        $fairy_h3_fonts = !empty($fairy_theme_options['fairy-font-h3-family-url']) ? esc_attr($fairy_theme_options['fairy-font-h3-family-url']) : '';
        if (!empty($fairy_h3_fonts)) {
            $fairy_h3_font_family = $fairy_google_fonts[$fairy_h3_fonts];
        }

        /* Heading H4 Font Option */
        $fairy_h4_fonts = !empty($fairy_theme_options['fairy-font-h4-family-url']) ? esc_attr($fairy_theme_options['fairy-font-h4-family-url']) : '';
        if (!empty($fairy_h4_fonts)) {
            $fairy_h4_font_family = $fairy_google_fonts[$fairy_h4_fonts];
        }

        /* Heading H5 Font Option */
        $fairy_h5_fonts = !empty($fairy_theme_options['fairy-font-h5-family-url']) ? esc_attr($fairy_theme_options['fairy-font-h5-family-url']) : '';
        if (!empty($fairy_h5_fonts)) {
            $fairy_h5_font_family = $fairy_google_fonts[$fairy_h5_fonts];
        }

        /* Heading H6 Font Option */
        $fairy_h6_fonts = !empty($fairy_theme_options['fairy-font-h6-family-url']) ? esc_attr($fairy_theme_options['fairy-font-h6-family-url']) : '';
        if (!empty($fairy_h6_fonts)) {
            $fairy_h6_font_family = $fairy_google_fonts[$fairy_h6_fonts];
        }

        /* Heading H6 Font Option */
        $fairy_site_title_fonts = !empty($fairy_theme_options['fairy-font-site-title-family-url']) ? esc_attr($fairy_theme_options['fairy-font-site-title-family-url']) : '';
        if (!empty($fairy_site_title_fonts)) {
            $fairy_site_title_font_family = $fairy_google_fonts[$fairy_site_title_fonts];
        }

        $fairy_h1_font_size = !empty($fairy_theme_options['fairy-h1-font-size']) ? absint($fairy_theme_options['fairy-h1-font-size']) : 32;
        $fairy_h2_font_size = !empty($fairy_theme_options['fairy-h2-font-size']) ? absint($fairy_theme_options['fairy-h2-font-size']) : 24;
        $fairy_h3_font_size = !empty($fairy_theme_options['fairy-h3-font-size']) ? absint($fairy_theme_options['fairy-h3-font-size']) : 21;
        $fairy_h4_font_size = !empty($fairy_theme_options['fairy-h4-font-size']) ? absint($fairy_theme_options['fairy-h4-font-size']) : 20;
        $fairy_h5_font_size = !empty($fairy_theme_options['fairy-h5-font-size']) ? absint($fairy_theme_options['fairy-h5-font-size']) : 18;
        $fairy_h6_font_size = !empty($fairy_theme_options['fairy-h6-font-size']) ? absint($fairy_theme_options['fairy-h6-font-size']) : 16;

        $fairy_site_title_font_size = !empty($fairy_theme_options['fairy-site-title-font-size']) ? absint($fairy_theme_options['fairy-site-title-font-size']) : 36;

        $fairy_h1_line_height = !empty($fairy_theme_options['fairy-h1-font-line-height']) ? esc_attr($fairy_theme_options['fairy-h1-font-line-height']) : 1.5;
        $fairy_h2_line_height = !empty($fairy_theme_options['fairy-h2-font-line-height']) ? esc_attr($fairy_theme_options['fairy-h2-font-line-height']) : 1.5;
        $fairy_h3_line_height = !empty($fairy_theme_options['fairy-h3-font-line-height']) ? esc_attr($fairy_theme_options['fairy-h3-font-line-height']) : 1.5;
        $fairy_h4_line_height = !empty($fairy_theme_options['fairy-h4-font-line-height']) ? esc_attr($fairy_theme_options['fairy-h4-font-line-height']) : 1.5;
        $fairy_h5_line_height = !empty($fairy_theme_options['fairy-h5-font-line-height']) ? esc_attr($fairy_theme_options['fairy-h5-font-line-height']) : 1.5;
        $fairy_h6_line_height = !empty($fairy_theme_options['fairy-h6-font-line-height']) ? esc_attr($fairy_theme_options['fairy-h6-font-line-height']) : 1.5;
        $fairy_site_title_line_height = !empty($fairy_theme_options['fairy-site-title-font-line-height']) ? esc_attr($fairy_theme_options['fairy-site-title-font-line-height']) : 1.2;

        $fairy_h1_letter_spacing = !empty($fairy_theme_options['fairy-h1-letter-spacing']) ? absint($fairy_theme_options['fairy-h1-letter-spacing']) : 0;
        $fairy_h2_letter_spacing = !empty($fairy_theme_options['fairy-h2-letter-spacing']) ? absint($fairy_theme_options['fairy-h2-letter-spacing']) : 0;
        $fairy_h3_letter_spacing = !empty($fairy_theme_options['fairy-h3-letter-spacing']) ? absint($fairy_theme_options['fairy-h3-letter-spacing']) : 0;
        $fairy_h4_letter_spacing = !empty($fairy_theme_options['fairy-h4-letter-spacing']) ? absint($fairy_theme_options['fairy-h4-letter-spacing']) : 0;
        $fairy_h5_letter_spacing = !empty($fairy_theme_options['fairy-h5-letter-spacing']) ? absint($fairy_theme_options['fairy-h5-letter-spacing']) : 0;
        $fairy_h6_letter_spacing = !empty($fairy_theme_options['fairy-h6-letter-spacing']) ? absint($fairy_theme_options['fairy-h6-letter-spacing']) : 0;
        $fairy_site_title_letter_spacing = !empty($fairy_theme_options['fairy-site-title-letter-spacing']) ? absint($fairy_theme_options['fairy-site-title-letter-spacing']) : 0;

        /* Typography Section */
        //Paragraph
        if (!empty($fairy_font_family)) {
            $fairy_custom_css .= "body { font-family: '{$fairy_font_family}'; }";
        }

        //Widget
        if (!empty($fairy_widget_font_family)) {
            $fairy_custom_css .= ".widget-title, , .related-post .card_title  { font-family: '{$fairy_widget_font_family}'; }";
        }

        if (!empty($fairy_post_font_family)) {
            $fairy_custom_css .= ".card-blog-post .card_title, .search .card-blog-post .entry-title { font-family: '{$fairy_post_font_family}'; }";
        }

        //Menu
        if (!empty($fairy_menu_font_family)) {
            $fairy_custom_css .= ".main-navigation a { font-family: '{$fairy_menu_font_family}'; }";
        }

        //h1
        if (!empty($fairy_h1_font_family)) {
            $fairy_custom_css .= ".entry-content h1 { font-family: '{$fairy_h1_font_family}'; }";
        }
        //h2
        if (!empty($fairy_h2_font_family)) {
            $fairy_custom_css .= ".entry-content h2, .related-post .post-title { font-family: '{$fairy_h2_font_family}'; }";
        }
        //h3
        if (!empty($fairy_h3_font_family)) {
            $fairy_custom_css .= ".entry-content h3 { font-family: '{$fairy_h3_font_family}'; }";
        }
        //h4
        if (!empty($fairy_h4_font_family)) {
            $fairy_custom_css .= ".entry-content h4, .author-title { font-family: '{$fairy_h4_font_family}'; }";
        }
        //h5
        if (!empty($fairy_h5_font_family)) {
            $fairy_custom_css .= ".entry-content h5 { font-family: '{$fairy_h5_font_family}'; }";
        }
        //h6
        if (!empty($fairy_h6_font_family)) {
            $fairy_custom_css .= ".entry-content h6 { font-family: '{$fairy_h6_font_family}'; }";
        }

        if (!empty($fairy_site_title_font_family)) {
            $fairy_custom_css .= ".site-title, .site-title a, .site-title h1 { font-family: '{$fairy_site_title_font_family}'; }";
        }

        if (!empty($fairy_font_size)) {
            $fairy_custom_css .= "body { font-size: {$fairy_font_size}px; }";
        }

        /* Primary Color Section */
        if (!empty($fairy_primary_color)) {
            //font-color
            $fairy_custom_css .= ".entry-content a, .entry-title a:hover, .related-title a:hover, .posts-navigation .nav-previous a:hover, .post-navigation .nav-previous a:hover, .posts-navigation .nav-next a:hover, .post-navigation .nav-next a:hover, #comments .comment-content a:hover, #comments .comment-author a:hover, .offcanvas-menu nav ul.top-menu li a:hover, .offcanvas-menu nav ul.top-menu li.current-menu-item > a, .error-404-title, #fairy-breadcrumbs a:hover, .entry-content a.read-more-text:hover, a:hover, a:visited:hover, .widget_fairy_category_tabbed_widget.widget ul.ct-nav-tabs li a  { color : {$fairy_primary_color}; }";

            //background-color
            $fairy_custom_css .= ".main-navigation #primary-menu li a:hover, .main-navigation #primary-menu li.current-menu-item > a, .candid-refined-post-format, .fairy-featured-block .fairy-col-2 .candid-refined-post-format, .trending-title, .search-form input[type=submit], input[type=\"submit\"], ::selection, #toTop, .breadcrumbs span.breadcrumb, article.sticky .fairy-content-container, .candid-pagination .page-numbers.current, .candid-pagination .page-numbers:hover, .ct-title-head, .widget-title:before,
.about-author-box .container-title:before, .widget ul.ct-nav-tabs:before, .widget ul.ct-nav-tabs li.ct-title-head:hover, .widget ul.ct-nav-tabs li.ct-title-head.ui-tabs-active { background-color : {$fairy_primary_color}; }";

            //border-color
            $fairy_custom_css .= ".candid-refined-post-format, .fairy-featured-block .fairy-col-2 .candid-refined-post-format, blockquote, .search-form input[type=\"submit\"], input[type=\"submit\"], .candid-pagination .page-numbers { border-color : {$fairy_primary_color}; }";
        }

        if (!empty($fairy_widget_font_family)) {
            $fairy_custom_css .= ".widget .widget-title, .related-post .card_title { font-family : '{$fairy_widget_font_family}'; }";
        }

        if (!empty($fairy_widget_font_size)) {
            $fairy_custom_css .= ".widget .widget-title, .related-post .card_title   { font-size : {$fairy_widget_font_size}px; }";
        }

        if (!empty($fairy_post_font_family)) {
            $fairy_custom_css .= ".card-blog-post .card_title, .search .card-blog-post .entry-title { font-family : '{$fairy_post_font_family}'; }";
        }

        if (!empty($fairy_post_font_size)) {
            $fairy_custom_css .= ".widget .hero_slick-slider .card .card_title, .card-blog-post .card_title, .search .card-blog-post .entry-title  { font-size : {$fairy_post_font_size}px; }";
        }

        if (!empty($fairy_h1_font_size)) {
            $fairy_custom_css .= ".entry-content h1 { font-size : {$fairy_h1_font_size}px; }";
        }

        if (!empty($fairy_h2_font_size)) {
            $fairy_custom_css .= ".entry-content h2, .related-post .post-title  { font-size : {$fairy_h2_font_size}px; }";
        }

        if (!empty($fairy_h3_font_size)) {
            $fairy_custom_css .= ".entry-content h3  { font-size : {$fairy_h3_font_size}px; }";
        }

        if (!empty($fairy_h4_font_size)) {
            $fairy_custom_css .= ".entry-content h4, .author-title  { font-size : {$fairy_h4_font_size}px; }";
        }

        if (!empty($fairy_h5_font_size)) {
            $fairy_custom_css .= ".entry-content h5  { font-size : {$fairy_h5_font_size}px; }";
        }

        if (!empty($fairy_h6_font_size)) {
            $fairy_custom_css .= ".entry-content h6  { font-size : {$fairy_h6_font_size}px; }";
        }

        if (!empty($fairy_site_title_font_size)) {
            $fairy_custom_css .= ".site-title, .site-title a, .site-title h1  { font-size : {$fairy_site_title_font_size}px; }";
        }

        if (!empty($fairy_menu_font_size)) {
            $fairy_custom_css .= "#primary-menu a  { font-size : {$fairy_menu_font_size}px; }";
        }

        if (!empty($fairy_h1_line_height)) {
            $fairy_custom_css .= ".entry-content h1  { line-height : {$fairy_h1_line_height}; }";
        }

        if (!empty($fairy_h2_line_height)) {
            $fairy_custom_css .= ".entry-content h2, .related-post .post-title  { line-height : {$fairy_h2_line_height}; }";
        }

        if (!empty($fairy_h3_line_height)) {
            $fairy_custom_css .= ".entry-content h3  { line-height : {$fairy_h3_line_height}; }";
        }

        if (!empty($fairy_h4_line_height)) {
            $fairy_custom_css .= ".entry-content h4, .author-title  { line-height : {$fairy_h4_line_height}; }";
        }

        if (!empty($fairy_h5_line_height)) {
            $fairy_custom_css .= ".entry-content h5  { line-height : {$fairy_h5_line_height}; }";
        }

        if (!empty($fairy_h6_line_height)) {
            $fairy_custom_css .= ".entry-content h6  { line-height : {$fairy_h6_line_height}; }";
        }

        if (!empty($fairy_site_title_line_height)) {
            $fairy_custom_css .= ".site-title a, .site-title h1 { line-height : {$fairy_site_title_line_height}; }";
        }

        if (!empty($fairy_h1_letter_spacing)) {
            $fairy_custom_css .= ".entry-content h1  { letter-spacing : {$fairy_h1_letter_spacing}px; }";
        }

        if (!empty($fairy_h2_letter_spacing)) {
            $fairy_custom_css .= ".entry-content h2, .related-post .post-title  { letter-spacing : {$fairy_h2_letter_spacing}px; }";
        }

        if (!empty($fairy_h3_letter_spacing)) {
            $fairy_custom_css .= ".entry-content h3  { letter-spacing : {$fairy_h3_letter_spacing}px; }";
        }

        if (!empty($fairy_h4_letter_spacing)) {
            $fairy_custom_css .= ".entry-content h4, .author-title  { letter-spacing : {$fairy_h4_letter_spacing}px; }";
        }

        if (!empty($fairy_h5_letter_spacing)) {
            $fairy_custom_css .= ".entry-content h5  { letter-spacing : {$fairy_h5_letter_spacing}px; }";
        }

        if (!empty($fairy_h6_letter_spacing)) {
            $fairy_custom_css .= ".entry-content h6  { letter-spacing : {$fairy_h6_letter_spacing}px; }";
        }
        if (!empty($fairy_site_title_letter_spacing)) {
            $fairy_custom_css .= ".site-title, .site-title h1, .site-title a  { letter-spacing : {$fairy_site_title_letter_spacing}px; }";
        }

        if (!empty($fairy_line_height)) {
            $fairy_custom_css .= "body, button, input, select, optgroup, textarea, p { line-height : {$fairy_line_height}; }";
        }

        if (!empty($fairy_menu_line_height)) {
            $fairy_custom_css .= "#primary-menu li a { line-height : {$fairy_menu_line_height}; }";
        }

        if (!empty($fairy_widget_title_line_height)) {
            $fairy_custom_css .= ".widget .widget-title, .related-post .card_title { line-height : {$fairy_widget_title_line_height}; }";
        }

        if (!empty($fairy_post_title_line_height)) {
            $fairy_custom_css .= ".card-blog-post .card_title, .search .card-blog-post .entry-title { line-height : {$fairy_post_title_line_height}; }";
        }

        if (!empty($fairy_letter_spacing)) {
            $fairy_custom_css .= "body, button, input, select, optgroup, textarea { letter-spacing : {$fairy_letter_spacing}px; }";
        }

        if (!empty($fairy_menu_letter_spacing)) {
            $fairy_custom_css .= "#primary-menu li a  { letter-spacing : {$fairy_menu_letter_spacing}px; }";
        }

        if (!empty($fairy_post_title_letter_spacing)) {
            $fairy_custom_css .= ".card-blog-post .card_title, .search .card-blog-post .entry-title  { letter-spacing : {$fairy_post_title_letter_spacing}px; }";
        }

        $fairy_top_header_bg_color = !empty($fairy_theme_options['fairy-top-header-background-color']) ? esc_attr($fairy_theme_options['fairy-top-header-background-color']) : '';
        if (!empty($fairy_top_header_bg_color)) {
            $fairy_custom_css .= ".light-grayish-white-bg, .search-form .search-field:focus, .site-header.site-header-left-logo .site-header-top, .site-header-topbar, .newsletter-content  { background-color : {$fairy_top_header_bg_color}; }";
        }

        $fairy_top_header_text_color = !empty($fairy_theme_options['fairy-top-header-text-color']) ? esc_attr($fairy_theme_options['fairy-top-header-text-color']) : '';
        if (!empty($fairy_top_header_text_color)) {
            $fairy_custom_css .= ".site-header.site-header-left-logo .site-header-top .site-header-top-menu li a, .site-header-topbar .site-header-top-menu li a { color : {$fairy_top_header_text_color}; }";
        }

        $fairy_top_header_text_hover_color = !empty($fairy_theme_options['fairy-top-header-hover-color']) ? esc_attr($fairy_theme_options['fairy-top-header-hover-color']) : '';
        if (!empty($fairy_top_header_text_hover_color)) {
            $fairy_custom_css .= ".site-header.site-header-left-logo .site-header-top .site-header-top-menu li a:hover, .site-header-topbar .site-header-top-menu li a:hover  { color : {$fairy_top_header_text_hover_color}; }";
        }

        /*
         * Menu Color Options
         */
        $fairy_menu_bg_color = !empty($fairy_theme_options['fairy-menu-background-color']) ? esc_attr($fairy_theme_options['fairy-menu-background-color']) : '';
        if (!empty($fairy_menu_bg_color)) {
            $fairy_custom_css .= ".site-header-v2 .site-header-bottom { background-color : {$fairy_menu_bg_color}; }
            @media screen and (max-width: 991px){
            #masthead #primary-menu > li, #masthead #primary-menu > li > a  { background-color : {$fairy_menu_bg_color}; }
            }";
        }

        $fairy_menu_border_bottom_color = !empty($fairy_theme_options['fairy-border-color']) ? esc_attr($fairy_theme_options['fairy-border-color']) : '';
        if (!empty($fairy_menu_border_bottom_color)) {
            $fairy_custom_css .= ".site-header-v2, .site-header-bottom { border-color : {$fairy_menu_border_bottom_color}; }";
        }

        $fairy_menu_border_bottom_color = !empty($fairy_theme_options['fairy-top-border-color']) ? esc_attr($fairy_theme_options['fairy-top-border-color']) : '';
        if (!empty($fairy_menu_border_bottom_color)) {
            $fairy_custom_css .= "@media screen and (min-width: 768px) { .site-header-top { border-bottom-color : {$fairy_menu_border_bottom_color}; } }";
        }

        $fairy_menu_text_color = !empty($fairy_theme_options['fairy-text-color']) ? esc_attr($fairy_theme_options['fairy-text-color']) : '';
        if (!empty($fairy_menu_text_color)) {
            $fairy_custom_css .= ".main-navigation a { color : {$fairy_menu_text_color}; }";
        }

        $fairy_menu_text_hover_color = !empty($fairy_theme_options['fairy-text-hover-color']) ? esc_attr($fairy_theme_options['fairy-text-hover-color']) : '';
        if (!empty($fairy_menu_text_hover_color)) {
            $fairy_custom_css .= ".main-navigation #primary-menu > li > a:hover { color : {$fairy_menu_text_hover_color}; }";
        }

        $fairy_dropdown_menu_text_color = !empty($fairy_theme_options['fairy-dropdown-text-color']) ? esc_attr($fairy_theme_options['fairy-dropdown-text-color']) : '';
        if (!empty($fairy_dropdown_menu_text_color)) {
            $fairy_custom_css .= " .main-navigation ul ul a, .main-navigation #primary-menu li ul li.focus a { color : {$fairy_dropdown_menu_text_color}; }";
        }

        $fairy_dropdown_menu_text_hover_color = !empty($fairy_theme_options['fairy-dropdown-text-hover-color']) ? esc_attr($fairy_theme_options['fairy-dropdown-text-hover-color']) : '';
        if (!empty($fairy_dropdown_menu_text_hover_color)) {
            $fairy_custom_css .= " .main-navigation ul ul a:hover, .main-navigation #primary-menu li ul li.focus a:hover { color : {$fairy_dropdown_menu_text_hover_color}; }";
        }

        $fairy_dropdown_menu_bg_color = !empty($fairy_theme_options['fairy-dropdown-background-color']) ? esc_attr($fairy_theme_options['fairy-dropdown-background-color']) : '';
        if (!empty($fairy_dropdown_menu_bg_color)) {
            $fairy_custom_css .= " .main-navigation ul ul a, .main-navigation #primary-menu li ul li.focus a { background-color : {$fairy_dropdown_menu_bg_color}; }";
        }

        $fairy_dropdown_menu_hover_bg_color = !empty($fairy_theme_options['fairy-dropdown-single-background-color']) ? esc_attr($fairy_theme_options['fairy-dropdown-single-background-color']) : '';
        if (!empty($fairy_dropdown_menu_hover_bg_color)) {
            $fairy_custom_css .= ".main-navigation ul ul li:hover > a, .main-navigation ul ul a:hover, .main-navigation #primary-menu li ul li.focus a:hover { background-color : {$fairy_dropdown_menu_hover_bg_color}; }";
        }

        /*
         * Blog Section Color Options
         */
        $fairy_blog_bg_color = !empty($fairy_theme_options['fairy-blog-background-color']) ? esc_attr($fairy_theme_options['fairy-blog-background-color']) : '';
        if (!empty($fairy_blog_bg_color)) {
            $fairy_custom_css .= ".card-blog-post  { background-color : {$fairy_blog_bg_color}; }";
        }

        $fairy_blog_title_color = !empty($fairy_theme_options['fairy-blog-heading-color']) ? esc_attr($fairy_theme_options['fairy-blog-heading-color']) : '';
        if (!empty($fairy_blog_title_color)) {
            $fairy_custom_css .= ".card_title a,  .card-blog-post .card_title{ color : {$fairy_blog_title_color}; }";
        }

        $fairy_blog_title_hover_color =  !empty($fairy_theme_options['fairy-blog-heading-hover-color']) ? esc_attr($fairy_theme_options['fairy-blog-heading-hover-color']) : '';
        if (!empty($fairy_blog_title_hover_color)) {
            $fairy_custom_css .= ".card_title a:hover{ color : {$fairy_blog_title_hover_color}; }";
        }

        $fairy_blog_text_color = !empty($fairy_theme_options['fairy-blog-paragraph-color']) ? esc_attr($fairy_theme_options['fairy-blog-paragraph-color']) : '';
        if (!empty($fairy_blog_text_color)) {
            $fairy_custom_css .= ".entry-content *{ color : {$fairy_blog_text_color}; }";
        }

        $fairy_blog_btn_bg_color = !empty($fairy_theme_options['fairy-blog-read-more-background-color']) ? esc_attr($fairy_theme_options['fairy-blog-read-more-background-color']) : '';
        if (!empty($fairy_blog_btn_bg_color)) {
            $fairy_custom_css .= " body .btn-primary, .btn.btn-primary  { background-color : {$fairy_blog_btn_bg_color}; }";
        }

        $fairy_blog_btn_hover_bg_color = !empty($fairy_theme_options['fairy-blog-read-more-hover-color']) ? esc_attr($fairy_theme_options['fairy-blog-read-more-hover-color']) : '';
        if (!empty($fairy_blog_btn_hover_bg_color)) {
            $fairy_custom_css .= " body .btn-primary:hover, .btn.btn-primary:hover  { background-color : {$fairy_blog_btn_hover_bg_color}; border-color : {$fairy_blog_btn_hover_bg_color};  }";
        }

        $fairy_blog_btn_text_color = !empty($fairy_theme_options['fairy-blog-read-more-text-color']) ? esc_attr($fairy_theme_options['fairy-blog-read-more-text-color']) : '';
        if (!empty($fairy_blog_btn_text_color)) {
            $fairy_custom_css .= " body .btn-primary, .btn.btn-primary  { color : {$fairy_blog_btn_text_color}; }";
        }

        $fairy_blog_pagination_bg_color = !empty($fairy_theme_options['fairy-blog-pagination-color']) ? esc_attr($fairy_theme_options['fairy-blog-pagination-color']) : '';
        if (!empty($fairy_blog_pagination_bg_color)) {
            $fairy_custom_css .= ".ajax-pagination .show-more, .posts-navigation .nav-links a, .pagination .page-numbers { background-color : {$fairy_blog_pagination_bg_color}; }";
        }

        $fairy_blog_pagination_active_bg_color = !empty($fairy_theme_options['fairy-blog-pagination-active-color']) ? esc_attr($fairy_theme_options['fairy-blog-pagination-active-color']) : '';
        if (!empty($fairy_blog_pagination_active_bg_color)) {
            $fairy_custom_css .= ".ajax-pagination .show-more:hover, .posts-navigation .nav-links a:hover, .pagination .page-numbers.current, .pagination .page-numbers:hover { background-color : {$fairy_blog_pagination_active_bg_color}; }";
        }

        /*
         * Sidebar Color Options
         */
        $fairy_sidebar_bg_color = !empty($fairy_theme_options['fairy-sidebar-background-color']) ? esc_attr($fairy_theme_options['fairy-sidebar-background-color']) : '';
        if (!empty($fairy_sidebar_bg_color)) {
            $fairy_custom_css .= ".widget-area .widget  { background-color : {$fairy_sidebar_bg_color}; }";
        }

        $fairy_sidebar_title_color = !empty($fairy_theme_options['fairy-sidebar-title-color']) ? esc_attr($fairy_theme_options['fairy-sidebar-title-color']) : '';
        if (!empty($fairy_sidebar_title_color)) {
            $fairy_custom_css .= ".widget .widget-title { color : {$fairy_sidebar_title_color}; }";
        }

        $fairy_sidebar_text_color = !empty($fairy_theme_options['fairy-sidebar-content-color']) ? esc_attr($fairy_theme_options['fairy-sidebar-content-color']) : '';
        if (!empty($fairy_sidebar_text_color)) {
            $fairy_custom_css .= ".widget-area .widget { color : {$fairy_sidebar_text_color}; }";
        }

        $fairy_sidebar_link_color = !empty($fairy_theme_options['fairy-sidebar-link-color']) ? esc_attr($fairy_theme_options['fairy-sidebar-link-color']) : '';
        if (!empty($fairy_sidebar_link_color)) {
            $fairy_custom_css .= ".widget-area .widget a{ color : {$fairy_sidebar_link_color}; }";
        }

        $fairy_sidebar_link_hover_color = !empty($fairy_theme_options['fairy-sidebar-link-hover-color']) ? esc_attr($fairy_theme_options['fairy-sidebar-link-hover-color']) : '';
        if (!empty($fairy_sidebar_link_hover_color)) {
            $fairy_custom_css .= ".widget-area .widget a:hover { color : {$fairy_sidebar_link_hover_color}; }";
        }

        /*
         * Top Footer Color Options
         */
        $fairy_top_footer_bg_color = !empty($fairy_theme_options['fairy-footer-background-color']) ? esc_attr($fairy_theme_options['fairy-footer-background-color']) : '';
        if (!empty($fairy_top_footer_bg_color)) {
            $fairy_custom_css .= ".site-footer-top  { background-color : {$fairy_top_footer_bg_color}; }";
        }

        $fairy_top_footer_text_color = !empty($fairy_theme_options['fairy-footer-text-color']) ? esc_attr($fairy_theme_options['fairy-footer-text-color']) : '';
        if (!empty($fairy_top_footer_text_color)) {
            $fairy_custom_css .= ".site-footer .site-footer-top *, .site-footer .site-footer-top .widget-title { color : {$fairy_top_footer_text_color}; }";
        }

        $fairy_top_footer_link_color = !empty($fairy_theme_options['fairy-footer-link-color']) ? esc_attr($fairy_theme_options['fairy-footer-link-color']) : '';
        if (!empty($fairy_top_footer_link_color)) {
            $fairy_custom_css .= ".site-footer .site-footer-top a { color : {$fairy_top_footer_link_color}; }";
        }

        $fairy_top_footer_link_hover_color = !empty($fairy_theme_options['fairy-footer-link-hover-color']) ? esc_attr($fairy_theme_options['fairy-footer-link-hover-color']) : '';
        if (!empty($fairy_top_footer_link_hover_color)) {
            $fairy_custom_css .= ".site-footer .site-footer-top a:hover { color : {$fairy_top_footer_link_hover_color}; }";
        }

        /*
         * Bottom Footer Color Options
         */

        $fairy_bottom_footer_bg_color = !empty($fairy_theme_options['fairy-lower-footer-background-color']) ? esc_attr($fairy_theme_options['fairy-lower-footer-background-color']) : '';
        if (!empty($fairy_bottom_footer_bg_color)) {
            $fairy_custom_css .= ".site-footer-bottom  { background-color : {$fairy_bottom_footer_bg_color}; }";
        }

        $fairy_bottom_footer_text_color = !empty($fairy_theme_options['fairy-lower-footer-text-color']) ? esc_attr($fairy_theme_options['fairy-lower-footer-text-color']) : '';
        if (!empty($fairy_bottom_footer_text_color)) {
            $fairy_custom_css .= ".site-footer .site-footer-bottom * { color : {$fairy_bottom_footer_text_color}; }";
        }

        $fairy_bottom_footer_link_color = !empty($fairy_theme_options['fairy-lower-footer-link-color']) ? esc_attr($fairy_theme_options['fairy-lower-footer-link-color']) : '';
        if (!empty($fairy_bottom_footer_link_color)) {
            $fairy_custom_css .= ".site-footer .site-footer-bottom a { color : {$fairy_bottom_footer_link_color}; }";
        }

        $fairy_bottom_footer_link_hover_color = !empty($fairy_theme_options['fairy-lower-footer-link-hover-color']) ? esc_attr($fairy_theme_options['fairy-lower-footer-link-hover-color']) : '';
        if (!empty($fairy_bottom_footer_link_hover_color)) {
            $fairy_custom_css .= ".site-footer .site-footer-bottom a:hover { color : {$fairy_bottom_footer_link_hover_color}; }";
        }

        $fairy_go_to_top_bg_color = !empty($fairy_theme_options['fairy-footer-to-top-back-color']) ? esc_attr($fairy_theme_options['fairy-footer-to-top-back-color']) : '';
        if (!empty($fairy_go_to_top_bg_color)) {
            $fairy_custom_css .= ".footer-go-to-top  { background-color : {$fairy_go_to_top_bg_color}; }";
        }

        $enable_category_color = !empty($fairy_theme_options['fairy-enable-category-color']) ? $fairy_theme_options['fairy-enable-category-color'] : '';
        if ($enable_category_color == 1) {
            $args = array(
                'orderby' => 'id',
                'hide_empty' => 0
            );
            $categories = get_categories($args);
            $wp_category_list = array();
            $i = 1;
            foreach ($categories as $category_list) {
                $wp_category_list[$category_list->cat_ID] = $category_list->cat_name;

                $cat_color = 'cat-' . esc_attr(get_cat_id($wp_category_list[$category_list->cat_ID]));


                if (array_key_exists($cat_color, $fairy_theme_options)) {
                    $cat_color_code = $fairy_theme_options[$cat_color];
                    $fairy_custom_css .= "
                    .category-label-group .ct-cat-item-{$category_list->cat_ID}:after,
                    .category-label-group.bg-label .ct-cat-item-{$category_list->cat_ID}{
                    background-color: {$cat_color_code};
                    }
                    ";
                }


                $i++;
            }
        }

        $fairy_overlay_color = !empty($fairy_theme_options['fairy-overlay-color']) ? esc_attr($fairy_theme_options['fairy-overlay-color']) : '';
        $fairy_overlay_second_color =  !empty($fairy_theme_options['fairy-overlay-second-color']) ? esc_attr($fairy_theme_options['fairy-overlay-second-color']) : '';
        if (!empty($fairy_overlay_color) && !empty($fairy_overlay_second_color)) {
            $fairy_custom_css .= "
                    .card-bg-image:after{
                    background-image: linear-gradient(45deg, {$fairy_overlay_color}, {$fairy_overlay_second_color});
                    }
                    ";
        } else {
            if (!empty($fairy_overlay_color)) {
                $fairy_custom_css .= "
                    .card-bg-image:after{
                    background-image: none;
                    background-color: $fairy_overlay_color;
                    }
                    ";
            }
            if (!empty($fairy_overlay_second_color)) {
                $fairy_custom_css .= "
                    .card-bg-image:after{
                    background-image: none;
                    background-color: $fairy_overlay_second_color;
                    }
                    ";
            }
        }

        $fairy_promo_overlay_color =  !empty($fairy_theme_options['fairy-boxes-overlay-first-color']) ? esc_attr($fairy_theme_options['fairy-boxes-overlay-first-color']) : '';
        $fairy_promo_overlay_second_color =  !empty($fairy_theme_options['fairy-boxes-overlay-second-color']) ? esc_attr($fairy_theme_options['fairy-boxes-overlay-second-color']) : '';
        if (!empty($fairy_promo_overlay_color) && !empty($fairy_promo_overlay_second_color)) {
            $fairy_custom_css .= "
                    .card-bg-image.card-promo .card_media a:after{
                    background-image: linear-gradient(45deg, {$fairy_promo_overlay_color}, {$fairy_promo_overlay_second_color});
                    }
                    ";
        } else {
            if (!empty($fairy_promo_overlay_color)) {
                $fairy_custom_css .= "
                   .card-bg-image.card-promo .card_media a:after{
                    background-image: none;
                    background-color: {$fairy_promo_overlay_color};
                    }
                    ";
            }
            if (!empty($fairy_promo_overlay_second_color)) {
                $fairy_custom_css .= "
                    .card-bg-image.card-promo .card_media a:after{
                    background-image: none;
                    background-color: {$fairy_promo_overlay_second_color};
                    }
                    ";
            }
        }

        $fairy_container_width =  !empty($fairy_theme_options['fairy-boxed-width-options']) ? absint($fairy_theme_options['fairy-boxed-width-options']) : '';
        if (!empty($fairy_container_width)) {
            $fairy_custom_css .= "
                    .ct-boxed #page{ max-width : {$fairy_container_width}px;
                    }
                    ";
        }


        /*
         * boxed content area bg color
         */
        $boxed_bg_color = !empty($fairy_theme_options['fairy-boxed-layout-background-color']) ? esc_attr($fairy_theme_options['fairy-boxed-layout-background-color']) : '';
        if (!empty($boxed_bg_color)) {
            $fairy_custom_css .= "body.ct-boxed #page{ background-color : {$boxed_bg_color}; }";
        }

        /*
         * Logo Section background color
         */
        $logo_background = !empty($fairy_theme_options['fairy-logo-section-background-color']) ? esc_attr($fairy_theme_options['fairy-logo-section-background-color']) : '';
        if (!empty($logo_background)) {
            $fairy_custom_css .= ".site-header .header-main-bar{ background-color : {$logo_background}; }";
        }

        $fairy_box_shadow = !empty($fairy_theme_options['fairy-site-layout-blog-overlay']) ? esc_attr($fairy_theme_options['fairy-site-layout-blog-overlay']) : '';
        if (!empty($fairy_theme_options) && ($fairy_box_shadow == 0)) {
            $fairy_custom_css .= "
                    .card-blog-post, .widget-area .widget{
                    box-shadow: none;
                    }
                    ";
        }

        $fairy_show_date = !empty($fairy_theme_options['fairy-post-published-updated-date']) ? esc_attr($fairy_theme_options['fairy-post-published-updated-date']) : '';
        if ($fairy_show_date == 'post-updated') {
            $fairy_custom_css .= "
                        .posted-on time.published:not(.updated){
                            display: none;
                        }
                        .posted-on time.updated:not(.published){
                            display: inline-block;
                        }
                    ";
        }

        $fairy_show_underline = !empty($fairy_theme_options['fairy-enable-underline-link']) ? esc_attr($fairy_theme_options['fairy-enable-underline-link']) : '';
        if ($fairy_show_underline == 1) {
            $fairy_custom_css .= "
                       .entry-content a{
                            text-decoration: underline;
                        }
                    ";
        }

        wp_add_inline_style('fairy-style', $fairy_custom_css);
    }
endif;
add_action('wp_enqueue_scripts', 'fairy_dynamic_css', 99);
